<?php

/* This file is created to store the database changes done */

/* * ***********************************************************************************
 * Version       : 1.0
 * Changed On    : 5th August 2018
 * Changed By   : Rahul V. Kedari 
 * *********************************************************************************** */
"ALTER TABLE `products` 
ADD COLUMN `product_definition` ENUM('Veg', 'NonVeg') NULL AFTER `deleteFlag`,
ADD COLUMN `product_cat_definition` ENUM('Single', 'Combo', 'Personalized') NULL AFTER `product_definition`,
ADD COLUMN `product_stock` INT NULL AFTER `product_cat_definition`,
ADD COLUMN `stock_limit` INT NULL AFTER `product_stock`,
ADD COLUMN `video_link` VARCHAR(200) NULL AFTER `stock_limit`,
ADD COLUMN `health_benifits` TEXT NULL AFTER `video_link`,
ADD COLUMN `product_review` TEXT NULL AFTER `health_benifits`,
ADD COLUMN `product_rate` INT NULL AFTER `product_review`,
ADD COLUMN `comments` TEXT NULL AFTER `product_rate`,
ADD COLUMN `long_description` TEXT NULL AFTER `comments`,
ADD COLUMN `product_price` FLOAT NULL AFTER `long_description`,
ADD COLUMN `discounted_price` FLOAT NULL AFTER `product_price`,
ADD COLUMN `city_price` FLOAT NULL AFTER `discounted_price`,
ADD COLUMN `tax_category` INT NULL AFTER `city_price`,
ADD COLUMN `tax_slab` INT NULL AFTER `tax_category`";

"CREATE TABLE `tax_category` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `tax_name` VARCHAR(100) NULL,
  `tax_percentage` INT NULL,
  `status` TINYINT(1) NULL,
  PRIMARY KEY (`id`))";

"ALTER TABLE `category` 
ADD COLUMN `category_image` VARCHAR(255) NULL AFTER `deleteFlag`";

"ALTER TABLE `subcategory` 
ADD COLUMN `image` VARCHAR(255) NULL AFTER `deleteFlag`;";

"CREATE TABLE `customer_class_config` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `class_name` VARCHAR(150) NOT NULL,
  `min_value` FLOAT NULL,
  `max_value` FLOAT NULL,
  `status` TINYINT(1) NULL,
  PRIMARY KEY (`id`))";

"CREATE TABLE `deal_of_day` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `sub_category_id` INT NOT NULL,
  `title` VARCHAR(150) NULL,
  `title_marathi` VARCHAR(150) NULL,
  `sub_title` VARCHAR(150) NULL,
  `sub_title_marathi` VARCHAR(150) NULL,
  `start_date` DATE NULL,
  `end_date` DATE NULL,
  `image` TEXT NULL,
  `status` TINYINT(1) NULL,
  `created_on` DATETIME NULL,
  PRIMARY KEY (`id`))";

?>

