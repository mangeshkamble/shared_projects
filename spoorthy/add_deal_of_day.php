<?php
/* * ---------------------------------------------------------------------------------
 * @MAHESHFULSUNDAR
 * @author           : Mahesh Fulsundar
 * @owner            : Mahesh Fulsundar
 * @version          : MAHESHFULSUNDAR 1.0 12-06-2018
 * @Notes            : All copyrights are reserved!
 * @rights           : All rights are reserved to the owner of the file creator.
  No one can do change to this file without permission of the owner.
 * @fileName         :
 * @dependancies     :
  ------------------------------------------------------------------------------------- */
include('cconfig.php');

//=========if form is posted ===========================// 
// code to update into database
if (isset($_POST['submit'])) {
    $sub_category_id = $_POST['sub_category_id'];
    $deal_title_eng = $_POST['deal_title_eng'];
    $deal_title_mar = $_POST['deal_title_mar'];
    $deal_sub_title_eng = $_POST['deal_sub_title_eng'];
    $deal_sub_title_mar = $_POST['deal_sub_title_mar'];
    $deal_start = date('Y-m-d', strtotime($_POST['deal_start']));
    $deal_end = date('Y-m-d', strtotime($_POST['deal_end']));
    $status = $_POST['status'];
    $current_date = date('Y-m-d H:i:s');
    $cat_image = '';

    if ($_FILES['deal_image1'] && $_FILES["deal_image1"]["name"] !== '') {
        $name = $_FILES["deal_image1"]["name"];
        $extension = end((explode(".", $name)));
        $filetype = $_FILES['deal_image1']['type'];
        $size = $_FILES['deal_image1']['size'];
        $destinationPath = '../upload/deal_of_day/';

        $idProofFileName = time();
        $idProofFileName = '1-' . $idProofFileName . '.' . $extension;
        $image1 = '../upload/deal_of_day/' . $idProofFileName;

        move_uploaded_file($_FILES["deal_image1"]["tmp_name"], $image1);
        $cat_image = 'upload/deal_of_day/' . $idProofFileName;
    }

    $query = mysqli_query($con, "INSERT INTO deal_of_day(sub_category_id,title,title_marathi,sub_title,sub_title_marathi,start_date,end_date,image,status,created_on) VALUES  ('" . $sub_category_id . "','" . $deal_title_eng . "','" . $deal_title_mar . "','" . $deal_sub_title_eng . "','" . $deal_sub_title_mar . "','" . $deal_start . "','" . $deal_end . "','" . $cat_image . "','" . $status . "','" . $current_date . "')");
    $sucmsg = base64_encode(serialize("Deal added sucessfully!"));
    header("Location:deal_of_day_list.php?sucmsg=$sucmsg");
    exit;
}

include('header.php');
include('nav.php');
?>
<title>Add Deal of Day |<?php echo SITENAME; ?></title>
<div class="page-inner">
    <div class="page-breadcrumb">
        <ol class="breadcrumb container">
            <li><a href="dashboard.php">Home</a></li>
            <li class="active">Add Deal of Day</li>
        </ol>
    </div>
    <div class="page-title">
        <div class="container">
            <h3>Deal of Day</h3>
        </div>
    </div>
    <div id="main-wrapper" class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-white">
                    <div class="panel-body">
                        <form class="form-horizontal" id="deal_of_day_form" name="deal_of_day_form" method="post" enctype="multipart/form-data">
                            <div class="form-group">
                                <label for="input-Default" class="col-sm-2 control-label">Category : </label>
                                <div class="col-sm-5">
                                    <select id="sub_category_id" name="sub_category_id" class="form-control">
                                        <option value="" selected="selected">Category...</option>
<?php
$query = mysqli_query($con, "SELECT * FROM subcategory sc INNER JOIN category cc ON cc.category_id=sc.category_id WHERE subcategory_status='Active'");
while ($category = mysqli_fetch_assoc($query)) {
    ?>
                                            <option value="<?php echo $category['subcat_id']; ?>"><?php echo $category['category_eng'] . ' - ' . $category['subcategory_eng']; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="input-Default" class="col-sm-2 control-label">Title (English) : </label>
                                <div class="col-sm-5">
                                    <input type="text" name="deal_title_eng" id="deal_title_eng" class="form-control" placeholder="English Title">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="input-Default" class="col-sm-2 control-label">Title (Marathi): </label>
                                <div class="col-sm-5">
                                    <input type="text" name="deal_title_mar" id="deal_title_mar" class="form-control" placeholder="Marathi Title">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="input-Default" class="col-sm-2 control-label">Sub Title (English) : </label>
                                <div class="col-sm-5">
                                    <input type="text" name="deal_sub_title_eng" id="deal_sub_title_eng" class="form-control" placeholder="English Sub Title">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="input-Default" class="col-sm-2 control-label">Sub Title (Marathi): </label>
                                <div class="col-sm-5">
                                    <input type="text" name="deal_sub_title_mar" id="deal_sub_title_mar" class="form-control" placeholder="Marathi Sub Title">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="input-Default" class="col-sm-2 control-label">Start Date: </label>
                                <div class="col-sm-5">
                                    <input type="text" name="deal_start" id="deal_start" class="form-control" placeholder="Start Date">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="input-Default" class="col-sm-2 control-label">End Date: </label>
                                <div class="col-sm-5">
                                    <input type="text" name="deal_end" id="deal_end" class="form-control" placeholder="End Date">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="input-Default" class="col-sm-2 control-label">Upload Image : </label>
                                <div class="col-sm-4">
                                    <input type="file" name="deal_image1" id="deal_image1" class="text-center event-name gui-input br-light bg-light" placeholder="Upload Image">
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <label for="input-Default" class="col-sm-2 control-label">Status : </label>
                                <div class="col-sm-5">
                                    <select name="status" id="status" class="form-control">
                                        <option value="1" selected>Active</option>
                                        <option value="0">Inactive</option>
                                    </select> 
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-sm-offset-10 col-sm-10">
                                    <input  class="btn btn-sm btn-success" type="submit" name="submit" value="Submit">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php include('footer.php'); ?>
    <script>
        $(document).ready(function (e) {

            $("#deal_start").datepicker({
                //minDate: ,
                maxDate: 0,
            });
            $("#deal_end").datepicker({
                //minDate: ,
                maxDate: 0,
            });
            //validation for topuplimit check
            $('#category').validate({
                rules: {
                    category:
                            {
                                required: true,
                            },
                    subcategory:
                            {
                                required: true,
                            },
                },
            });
        });
    </script>