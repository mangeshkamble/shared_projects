<?php
/* * ---------------------------------------------------------------------------------
 * @MAHESHFULSUNDAR
 * @author           : Mahesh Fulsundar
 * @owner            : Mahesh Fulsundar
 * @version          : MAHESHFULSUNDAR 1.0 12-06-2018
 * @Notes            : All copyrights are reserved!
 * @rights           : All rights are reserved to the owner of the file creator.
  No one can do change to this file without permission of the owner.
 * @fileName         :
 * @dependancies     :
  ------------------------------------------------------------------------------------- */
//include db configuration file
include_once("cconfig.php"); 
$myProducts	=	json_decode(base64_decode($_POST['product_id']));

//------------get the expiry date extention setting from the settings--//
//$settingval	=	intval(getSettingsValues('Expiry Days',$con));
//$checkDate	=	date('Y-m-d',strtotime("+".$settingval." days"));
$checkDate	=	date('Y-m-d',strtotime("1 days"));

function deductQuantity($con,$batchId,$quantity,$userID,$productID,$checkDate)
{
	$checkProducts	= mysqli_fetch_assoc(mysqli_query($con, "SELECT * FROM products WHERE 
												  
												  product_id = '" . $productID . "'  AND quantity!=0 "));
												  
	$batchQuantity		= $checkProducts['quantity'];
	//$batchCode			= $checkProducts['batch_no'];
	//$expiry_date		= $checkProducts['expiry_date'];
	if($batchQuantity >= $quantity)
	{	
		//echo "minus from quantity.<br>";
		$remainQuantity	= $batchQuantity - $quantity;
		//echo "remain : " . $remainQuantity . "<br>";
		//mysqli_query($con, "UPDATE assign_products SET quantity= quantity - '$quantity' WHERE product_id='".$checkProducts['product_id']."' AND batch_no='".$checkProducts['batch_no']."' AND assign_to='".$checkProducts['assign_to']."'");		
		mysqli_query($con,"UPDATE products SET quantity=quantity - '$quantity' WHERE product_id='".$checkProducts['product_id']."'");
	} 
	else 
	{
		//echo "batch quantity : ".$batchQuantity."<br>";
		$remainQuantity	= $quantity - $batchQuantity;
		//echo "remain : " . $remainQuantity . "<br>";
		$minusQuantity	= $batchQuantity;
		$batchId++;
		
		mysqli_query($con, "UPDATE products SET quantity= quantity - '$minusQuantity' 
							WHERE product_id='".$checkProducts['product_id']."'");
							
		//mysqli_query($con,"UPDATE rd_product SET quantity=quantity - '$minusQuantity' 
			//			   WHERE product_id='".$checkProducts['product_id']."'");
		
		if($remainQuantity >= 0)
		deductQuantity($con,$batchId,$remainQuantity,$userID,$productID,$checkDate);
	}
}
	 
	 	
if(isset($_POST["comments"])) 
{	
 	//check $_POST["content_txt"] is not empty
 	//sanitize post value, PHP filter FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH Strip tags, encode special characters.
	$comments	 	= filter_var($_POST["comments"],FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH); 
	$inputStatus 	= filter_var($_POST["inputStatus"],FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH); 
	$inputNotify	= filter_var($_POST["inputNotify"],FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH); 
	$orderNo		= filter_var($_POST["orderNo"],FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH); 
	$deductQuantity	= false;
	$directPurchaseIncome = false;
	 
	//get order status name
	//echo "<br> SELECT * FROM rd_order_status WHERE status_id = '" . $inputStatus . "'";
	
	$getOrderStatus		= mysqli_query($con,"SELECT * FROM order_status WHERE status_id = '" . $inputStatus . "'");
	$orderStatusDet		= mysqli_fetch_assoc($getOrderStatus);
	$orderStatusName	= $orderStatusDet['status_name'];
	if(strlen($comments ) < 0)
		$comments = $orderStatusName;

	//get order details from rd_order
	
	//echo "<br> SELECT * FROM rd_order WHERE order_id = '" . $orderNo . "'";
	
	 $orderDetails		= mysqli_fetch_assoc(mysqli_query($con,"SELECT * FROM orders WHERE order_id = '" . $orderNo . "'"));
	 $ref_id			= $orderDetails['referral_id'];
	 $customerId		= $orderDetails['cust_id'];
	 
	 
   
	if(!empty($myProducts))
	{
		foreach($myProducts as $index=>$value)
		{
			$order_details	=	mysqli_fetch_assoc(mysqli_query($con,"SELECT * FROM order_details WHERE order_id='$orderNo' AND product_id='$value'"));
			$quantity		=	$order_details['quantity'];
			$dealer_id		=	$order_details['dealer_id'];
			$distributor_id	=	$order_details['distributor_id'];
			
			if($dealer_id != 0)
			{
				 $user_id = $dealer_id;
			}
			elseif($distributor_id!=0)
			{
				 $user_id = $distributor_id;
			}
			else
			{ 
				 $user_id	=	1;
			}
			 	
			//get total quantity available at product
			$getTotalQuantity	= mysqli_query($con,"SELECT ifnull(sum(quantity),0) as totalQuantity 
													FROM products  WHERE product_id = '" . $value . "'");
			$quantityDetails	= mysqli_fetch_assoc($getTotalQuantity);
			$totalQuantity		= $quantityDetails['totalQuantity'];
			
			if($totalQuantity >= $quantity) 
			{
				//get previous order status
				$getOrderDetails	= mysqli_query($con,"SELECT * FROM order_tracking 
														  WHERE order_id = '" . $orderNo . "' 
														    order by status_id DESC LIMIT 1");
				$orderDetails		= mysqli_fetch_assoc($getOrderDetails);
				$prevOrderStatus	= $orderDetails['status_id'];
				$nextOrderStatus	= $prevOrderStatus + 1;

				
				//check for previous entries for status is there or not
				//if not then make previous entries also
				if($nextOrderStatus < $inputStatus)
				{
					//get the difference
					for($i = $nextOrderStatus; $i < $inputStatus; $i++)
					{
						//get order status name
						$getOrderStatus		= mysqli_query($con, "SELECT * FROM order_status WHERE status_id = '" . $i . "'");
						$orderStatusDet		= mysqli_fetch_assoc($getOrderStatus);
						$orderStatusName	= $orderStatusDet['status_name'];
						$timeStamp			= date('Y-m-d H:i:s');
						
						//make entry in rd_order_tracking
						mysqli_query($con, "INSERT INTO order_tracking 
											(order_id,date_added, status_id, comments, customer_notify) 
											VALUES 
											('" . $orderNo . "', '" . $timeStamp . "', '" . $i . "', 
											 '" . $orderStatusName . "', 'No')
										   ");
										   
						if($i == 4)
						{
							$deductQuantity = true;
						}
						if($i == 6)
						{
							$directPurchaseIncome = true;
						}
					}
				}
				
				$insert_row = mysqli_query($con,"INSERT INTO order_tracking
												  (order_id, comments, status_id, customer_notify, date_added) 
										  		   VALUES 
										 		  ('" . $orderNo . "', '" . $comments . "', 
												   '" . $inputStatus . "', '" . $inputNotify . "', 
												   '" . date('Y-m-d H:i:s') . "')
												  ");
				
				//update order_details table 
				mysqli_query($con,"UPDATE order_details SET product_status = '" . $inputStatus . "' WHERE order_id = '" . $orderNo . "' AND product_id = '" . $value . "'");
				if($inputStatus == 4)
				{
					$deductQuantity = true;
				}
				if($inputStatus == 6)
				{
					$directPurchaseIncome = true;
				}
				
				if($deductQuantity == true)
				{
					$order_details	=	mysqli_fetch_assoc(mysqli_query($con,"SELECT * FROM order_details 
																			  WHERE order_id='$orderNo' AND 
																			  product_id='$value'"));
					$quantity		=	$order_details['quantity'];
					$dealer_id		=	$order_details['dealer_id'];
					$distributor_id	=	$order_details['distributor_id'];
					$batchId		=	0;
					
					//if dealer is not present then 
					if($dealer_id != 0)
					{
						deductQuantity($con,$batchId,$quantity,$dealer_id,$value,$checkDate);
					}
					elseif($distributor_id!=0)
					{
						deductQuantity($con,$batchId,$quantity,$distributor_id,$value,$checkDate);
					}
					else
					{
						// if the admin approves the order then userid- resellers id
						/*$productResllerQuery	=	mysqli_query($con,"Select * from  rd_product where product_id=$value");
						$prodDetails			=   mysqli_fetch_assoc($productResllerQuery);
						//$user_id	=	$prodDetails['added_by'];
						deductQuantity($con,$batchId,$quantity,$prodDetails['added_by'],$value,$checkDate);*/
						deductQuantity($con,$batchId,$quantity,1,$value,$checkDate);
					}
				}
				
				/*if($directPurchaseIncome == true)
				{
					   $productID = $value;
					   $order_quantity = $quantity; 
					/*============================================================================================
					Inert into the Direct Purchse Income table if RefID is not null
					============================================================================================= 
					if($ref_id != '')
					{
						//level income
						  $product_id	=	$value;
  						 // $userDetails = getUserDetails($ref_id,$con);
						 // $user_id	=	$userDetails['referal_id'];   
						  $user_id	=	$ref_id;   
						  //include('level_income_distribut.php');
					}
				}*/
			} 
			else {
				echo "no_quantity";
				exit;
			}
		}
	}
	else
	{
		$insert_row = mysqli_query($con, "INSERT INTO order_tracking
   										 (order_id, comments, status_id, customer_notify, date_added) 
									  VALUES 
									 ('" . $orderNo . "','" . $comments . "', '" . $inputStatus . "', '" . $inputNotify . "', 
									  '" . date('Y-m-d H:i:s') . "')
									");
	}
	
	//update order status after all products are delivered
	//get all product count ordered in selected order
	$getProductCount	= mysqli_query($con, "SELECT * FROM order_details WHERE order_id = '" . $orderNo . "'");
	$productCount		= mysqli_num_rows($getProductCount);
	
	//check all product status of selected order
	$getAllProductDetails		= mysqli_query($con, "SELECT * FROM orders WHERE order_id = '" . $orderNo . "'");
	$productStatusCount			= 0;
	$productDStatusCount		= 0;
	while($allProductDetails	= mysqli_fetch_assoc($getAllProductDetails))
	{
		$productStatus = $allProductDetails['order_status_id'];
		if($productStatus == 4)		// status_id = 4 is product dispatched
		{
			$orderStatus	= 4;
			$productStatusCount++;
		}
		
		if($productStatus == 6)		// status_id = 6 is product delivered
		{
			$orderStatus	= 6;
			$productDStatusCount++;
		}
	}
	
	//if all products are dispatched then change order status this is for product dispatched
	if($productCount == $productStatusCount)
	{
		//update product Status
		mysqli_query($con, "UPDATE  orders SET order_status_id = '" . $orderStatus . "' WHERE order_id = '" . $orderNo . "'");
	}
	
	//if all products are dispatched then change order status this is for product delivered
	if($productCount == $productDStatusCount)
	{
		//update product Status
		mysqli_query($con, "UPDATE  orders SET order_status_id = '" . $orderStatus . "' WHERE order_id = '" . $orderNo . "'");
	}
	
	//$update_row = mysqli_query($con, "UPDATE rd_order SET order_status_id = " . $inputStatus . " WHERE order_id = " . $orderNo);
	 
	// Insert sanitize string in record	
	if($insert_row)
	{
		$status	=	 getStatus($con,$inputStatus);
		//Record was successfully inserted, respond result back to index page
		$my_id = mysqli_insert_id($con); //Get ID of last inserted row from MySQL
		$Details = '<tr>';
		$Details.= '<td class="text-left">'.date('d/m/Y H:i:s').'</td>';
		$Details.= '<td class="text-left">'.$comments.'</td>';
		$Details.= '<td class="text-left">'.$status.'</td>';
		$Details.= '<td class="text-left">'.$inputNotify.'</td>';
		$Details.= '</tr>';
		
		$arrOrderTrack	=	array('track' =>$Details);
		
		//if inputStatus == 'Delivered' or 'Dispatched' then send email and message to notify customer
		if($status == 'Dispatched' || $status == 'Delivered')
		{
			if($status == 'Delivered')
			{
				//update delivery date for order
				mysqli_query($con, "UPDATE rd_order SET delivery_date = '" . date("Y-m-d") . "' WHERE order_id = '" . $orderNo . "'");
			}
			
			//get order details
			$orderDetails		= getOrderDetails($con,$orderNo);
			$shippingCode		= $orderDetails['shipping_code'];
			$mobile				= $orderDetails['mobile'];
			$emailId			= $orderDetails['email_id'];
			$custName			= $orderDetails['cust_name'];
			$shippingAddr1		= $orderDetails['shipping_address1'];
			$shippingAddr2		= $orderDetails['shipping_address2'];
			
			$shippingCityId		= $orderDetails['shipping_city_id'];
			$cityDetails		= getCityDetails($con,$shippingCityId);
			$shippingCityName	= $cityDetails['city_name'];
			
			$shippingStateId 	= $orderDetails['shipping_state_id'];
			$stateDetails		= getStateDetails($con,$shippingStateId);
			$shippingStateName	= $stateDetails['state'];
			
			$shippingCountryId	= $orderDetails['shipping_country_id'];
			$countryDetails		= getCountry($con,$shippingCountryId);
			$shippingCountryName = $countryDetails['name'];
			
			$shippingPincode	= $orderDetails['shipping_pincode'];
			
			$trackingLink		= SITEURL . "/order_track.php?orderNo=" . base64_encode($orderNo);
			//send message
			$message	= "Your order " . $shippingCode . " was " . $status . " at " . date("D d H:i a") . ". You can track your shipment at " . $trackingLink;
			//send_sms($mobile, $message, $con);
			
			/********************************
			send email
			********************************/
			ob_start();
			?>
            <html>
              <body>
                <div>
                <h3>Hi, <?php echo $custName ?>, </h3>
                <br><br>
                Greeting from Spoorthy Bazaar!
                <br><br>
                Your shipment was <?php echo $status ?>. <br>
                Your Shipment Id : <strong><?php echo $shippingCode ?></strong>
                <br>
                <a href="<?php echo $trackingLink ?>" style="padding: 10px; color: white; background:rgba(0,153,255,1)">Track Order</a>
                <br><br>
                The shipment was sent to :
                <?php echo $custName . " " . $mobile ?><br>
                <?php echo $shippingAddr1 . " " . $shippingAddr2; ?><br>
                <?php echo $shippingCityName . "-" . $shippingPincode; ?><br>
                <?php echo $shippingStateName; ?>
                </div>
              </body>
            </html>
            <?php
			$messageToSend	= ob_get_clean();
			$to 			= $emailId;
			$subject 		= "Tracking of your order: Spoorthy Bazaar";
			//send_email($to, $subject, $messageToSend, NO_REPLY_EMAIL);
		}
		
		echo json_encode($arrOrderTrack);

	}else{
		
		//header('HTTP/1.1 500 '.mysql_error()); //display sql errors.. must not output sql errors in live mode.
		header('HTTP/1.1 500 Looks like mysql error, could not insert record!');
		exit();
	}
}
else
{
	//Output error
	header('HTTP/1.1 500 Error occurred, Could not process request!');
    exit();
}
?>