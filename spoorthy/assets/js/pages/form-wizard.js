$(document).ready(function() {
	
	
	//-------------Validation for the Van Details form------------//
     $("#vanForm").validate({
        rules: {
            vanCode: {
                required: true
            },
            vanNumber: {
                required: true
		    },
        },
		submitHandler:function(form)
		{
			form.submit();
		}
    });
	
	//-------------Validation for the Franchisee Details form------------//
     $("#franForm").validate({
        rules: {
            franchisee_code: {
                required: true
            },
            franchisee_name: {
                required: true
		    },
			owner_name: {
                required: true
            },
            franMobile: {
                required: true,
				number:true
		    },
			franEmail: {
                required: true,
				email:true
            },
            franAddress: {
                required: true
		    },
        },
		submitHandler:function(form)
		{
			form.submit();
		}
    });
	
	//-------------Validation for the Item Details form------------//
     $("#itemForm").validate({
        rules: {
            itemCode: {
                required: true
            },
            itemName: {
                required: true
		    },
			category: {
                required: true
            },
            purchsePrise: {
                required: true
		    },
			mrp: {
                required: true
            },
            salePrise: {
                required: true
		    },
			discount: {
				required: true
			},
        },
		submitHandler:function(form)
		{
			form.submit();
		}
    });
	
	//------------Validations for the suppliers form---------------//
	$("#suppliersForm").validate({
        rules: {
            suppName: {
                required: true
            },
            suppCompany: {
                required: true
		    },
			suppMobile:{
				required: true,
				number:	true
			},
			suppEmail:{
				required: true,
				email:	true
			},
			suppAddress:{
				required: true
			},
			suppCode:{
				required: true
			},	
        },
		submitHandler:function(form)
		{
			form.submit();
		}
    });
	
	
	 var $validator = $("#vanForm2").validate({
        rules: {
            exampleInputName: {
                required: true
            },
            exampleInputName2: {
                required: true
		    },
		    exampleInputEmail: {
                required: true,
                email: true
		    },
		    exampleInputPassword1: {
                required: true
		    },
		    exampleInputPassword2: {
                required: true,
                equalTo: '#exampleInputPassword1'
		    },
		    exampleInputProductName: {
                required: true
		    },
		    exampleInputProductId: {
                required: true
		    },
		    exampleInputQuantity: {
                required: true
            },
		    exampleInputCard: {
                required: true,
                number: true
		    },
		    exampleInputSecurity: {
                required: true,
                number: true
		    },
		    exampleInputHolder: {
                required: true
            },
		    exampleInputExpiration: {
                required: true,
                date: true
            },
		    exampleInputCsv: {
                required: true,
                number: true
            }
        }
    });
 
    $('#rootwizard').bootstrapWizard({
        'tabClass': 'nav nav-tabs',
        onTabShow: function(tab, navigation, index) {
            var $total = navigation.find('li').length;
            var $current = index+1;
            var $percent = ($current/$total) * 100;
            $('#rootwizard').find('.progress-bar').css({width:$percent+'%'});
        },
        'onNext': function(tab, navigation, index) {
            var $valid = $("#wizardForm").valid();
            if(!$valid) {
                $validator.focusInvalid();
                return false;
            }
        },
        'onTabClick': function(tab, navigation, index) {
            var $valid = $("#wizardForm").valid();
            if(!$valid) {
                $validator.focusInvalid();
                return false;
            }
        },
    });
    
    $('.date-picker').datepicker({
        orientation: "top auto",
        autoclose: true
    });
});