<?php 
/* * ---------------------------------------------------------------------------------
 * @MAHESHFULSUNDAR
 * @author           : Mahesh Fulsundar
 * @owner            : Mahesh Fulsundar
 * @version          : MAHESHFULSUNDAR 1.0 12-06-2018
 * @Notes            : All copyrights are reserved!
 * @rights           : All rights are reserved to the owner of the file creator.
  No one can do change to this file without permission of the owner.
 * @fileName         :
 * @dependancies     :
  ------------------------------------------------------------------------------------- */
include('cconfig.php');
include('header.php');
include('nav.php');

if(isset($_GET['action']))
{
	$bannerId		=	unserialize(base64_decode($_GET['bannerId']));
	$getBanner		=	mysql_query("SELECT * FROM banner WHERE banner_id=".$bannerId);
	$bannerList		=	mysql_fetch_assoc($getBanner);
	
	$banner_id		=	$bannerList['banner_id'];
	$Title			=	$bannerList['banner_title'];
	$bannerUrl		=	$bannerList['banner_url'];
	$userDetails	=	getUserDetailsByLoginID($bannerList['banner_added_by']);
	$refDetails		=	getUserDetails($bannerList['banner_referral_id']);
	$positionId		=	$bannerList['banner_position_id'];
	$position		=	getBannerPosition($bannerList['banner_position_id']);
	$image_path		=	displayImage($bannerUrl, '../', 0);
	$start_date		=	date('d/m/Y',strtotime($bannerList['banner_start_date']));
	$end_date		=	date('d/m/Y',strtotime($bannerList['banner_end_date']));
	$status			=	$bannerList['banner_status'];
	$amount			=	round($bannerList['banner_price'],2);
	
	$diff			=	date_diff(date_create($bannerList['banner_start_date']),date_create($bannerList['banner_end_date']));
	$date_diff		=	$diff->format('%a');
	$action			=	'Edit';
	?>
    <script>
	jQuery(document).ready(function()
	{
		$('#bannertitle').attr('disabled','true');
		$('#position').attr('disabled','true');
		$('#days').attr('disabled','true');
		$('#amount').attr('disabled','true');
		$('#fromDate').attr('disabled','true');
		$('#referralCode').attr('disabled','true');
	});
	</script>
    <?php
 }
 else
 {
	$bannerId		= '';
	$banner_id		= '';
	$Title			= '';
	$bannerUrl		= '';
	$positionId		= '';
	$position		= '';
	$image_path		= '';
	$start_date		= '';
	$end_date		= '';
	$status			= '';
	$amount			= '';
	$date_diff		= 1;
	$action			= 'Add';
 } ?>
<script>
 
jQuery(document).ready(function()
{
  $('#check').hide();
  $('#uncheck').hide();
	  
	jQuery("#fromDate").datepicker({
		minDate:0,
		onSelect: function(date) {
		  var fromDate1 = jQuery('#fromDate').datepicker('getDate');
		  fromDate1.setDate(fromDate1.getDate() + 1);
		}
	});	
});

function verify_referral_code(code)
{
 	$.ajax({
	 type: "POST",
	 url: "../verify_refcode.php",
	// data: dataString,
	 data : { refcode : code},
	 cache: false,
	 success: function(result){
		 myresult	=	JSON.parse(result);
		 
		if(myresult.count ==1)
		{ 
			$('#check').show();
			$('#uncheck').hide();
			$('#referral_id').val(myresult.userID);
			$('#ref_name').html(myresult.name);
		}
		else
		{
			$('#uncheck').show();
			$('#check').hide();
			$('#referral_id').val('0');
			$('#ref_name').html(myresult.msg);
		}
	 }
   });
}


function calculateAmount(position)
{
	$.ajax({
	 type: "POST",
	 url: "getbanner_amount.php",
	 data : { position_id : position},
	 cache: false,
	 success: function(result){
		 myresult	=	JSON.parse(result);
		
		days 	=	$('#days').val();
		total	=	(days * myresult.amount);
		
		$('#amount').val(total);
		$('#perDay').val(myresult.amount);
	 }
   });
}

function addDays(theDate, days) {
    return new Date(theDate.getTime() + days*24*60*60*1000);
}

function convertDate(inputFormat) {
  function pad(s) { return (s < 10) ? '0' + s : s; }
  var d = new Date(inputFormat);
  return [ pad(d.getMonth()+1),pad(d.getDate()), d.getFullYear()].join('/');
}


function checkTotal()
{
	frmdate	=	new Date($('#fromDate').val());
	
	days	=	$('#days').val();
	perday	=	$('#perDay').val();
	
	total	=	days * perday;
		
	$('#amount').val(total);
	
	var newDate = addDays(frmdate, days);
	$('#toDate').val(convertDate(newDate));
	
}
</script>
<style>
.input-xxlarge {
	width: 33%
}
select {
	width: 34%;
}
</style>
<title>Banners |<?php echo SITENAME;?></title>
<div class="page-inner">
<div class="page-breadcrumb">
  <ol class="breadcrumb container">
    <li><a href="dashboard.php">Home</a></li>
    <li class="active">Banners</li>
  </ol>
</div>
<div class="page-title">
  <div class="container">
    <h3>Banners</h3>
  </div>
</div>
<div id="main-wrapper" class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="panel panel-white">
          <div class="panel-heading clearfix">
            <h4 class="panel-title"><?php echo $action; ?> Banners</h4>
          </div>
          <div class="panel-body">
            <form class="form-horizontal" enctype="multipart/form-data" name="addbannersForm" id="addbannersForm" method="post" action="banners_insert.php">
              <div class="form-group">
              <label for="input-help-block" class="col-sm-2 control-label">Banner Title : </label>
                <div class="col-sm-5">
                  <input type="text" name="bannertitle" id="bannertitle" class="form-control" value="<?php  echo $Title; ?>" />
                <input type="hidden" name="bannerid" id="bannerid" class="form-control"  value="<?php echo $banner_id;?>"/>
                <input type="hidden" name="action" id="action" class="form-control"  value="<?php echo $action;?>"/>
                </div>
              </div>
              <div class="form-group">
              <label for="input-help-block" class="col-sm-2 control-label">Position : </label>
                <div class="col-sm-5">
                 <select class="form-control" name="position" id="position" onChange="calculateAmount(this.value)">
                <option value="">-- Select --</option>
                <option value="0">Top</option>
                <?php 
				$queryBannerSize	=	mysqli_query($con,"SELECT * FROM subcategory sc INNER JOIN category cc ON cc.category_id=sc.category_id WHERE subcategory_status='Active'");
				while($bannerSize	=	mysqli_fetch_assoc($queryBannerSize))
				{
					$bannerPositionId	= $bannerSize['subcat_id'];
					$bannerPosition 	= $bannerSize['category_eng'] . ' - ' . $bannerSize['subcategory_eng'];
					?>
                	<option value="<?php echo $bannerPositionId;?>"><?php echo $bannerPosition;?></option>
                	<?php 
				}
				?>
              </select>
                </div>
              </div>
              <div class="form-group">
              <label for="input-help-block" class="col-sm-2 control-label">Banner Image : </label>
                <div class="col-sm-5">
                 <input type="file" name="bannerImage" id="bannerImage" />
                  <?php if($bannerUrl != '') { ?>
                <div  style="margin-top:20px;">
                	<img  style="border:1px solid #0C3; margin-right:200px;" src="<?php echo $image_path;?>" width="220" 
                    height="150">
                </div>
				<?php } ?>
                 
                </div>
              </div>
               <div class="form-group">
              <label for="input-help-block" class="col-sm-2 control-label">From Date : </label>
                <div class="col-sm-5">
                <input type="text" name="fromDate" id="fromDate" onChange="checkTotal()" onBlur="checkTotal()" value="<?php echo $start_date;?>" class="form-control" placeholder="mm/dd/yyyy"/>
                </div>
              </div>
               <div class="form-group">
              <label for="input-help-block" class="col-sm-2 control-label">Days : </label>
                <div class="col-sm-5">
                 <input type="text" name="days" id="days" class="form-control" value="<?php echo $date_diff;?>" onChange="checkTotal()" onBlur="checkTotal()"/>
                </div>
              </div>
               <div class="form-group">
              <label for="input-help-block" class="col-sm-2 control-label">Amount : </label>
                <div class="col-sm-5">
                 <input type="text" readonly name="amount" id="amount" value="<?php echo $amount;?>" class="form-control"/>
                <input type="hidden" name="perDay" id="perDay" class="input-xxlarge"/>
                </div>
              </div>
              <div class="form-group">
              <label for="input-help-block" class="col-sm-2 control-label">To Date : </label>
                <div class="col-sm-5">
                <input type="text" readonly name="toDate" id="toDate" value="<?php echo $end_date;?>" class="form-control"  placeholder="mm/dd/yyyy"/>
                </div>
              </div>
              <!--<div class="form-group">
              <label for="input-help-block" class="col-sm-2 control-label">Referral Code : </label>
                <div class="col-sm-5">
                 <input type="text" name="referralCode" id="referralCode" class="form-control" onchange="verify_referral_code(this.value)" onBlur="verify_referral_code(this.value)" onKeyDown="verify_referral_code(this.value)" onKeyUp="verify_referral_code(this.value)" />
                  <span id="check"><img src="images/check.png" height="30" width="30"></span> <span id="uncheck"><img src="images/uncheck.gif" width="10" height="10"></span>
                <input type="hidden" name="referral_id" id="referral_id">
                <span id="ref_name" style="color:#069"></span>
                </div>
              </div>-->
<hr>
              <div class="form-group col-md-12" align="right"> 
                 <input type="submit" name="submit" id="submit" class="btn btn-primary btn-addon m-b-sm" value="Save" />
              </div> 
          </form>
       </div>
        </div>
      </div>
    </div>
  </div>
 <?php include("footer.php");?>