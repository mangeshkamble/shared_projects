<?php  
/* * ---------------------------------------------------------------------------------
 * @MAHESHFULSUNDAR
 * @author           : Mahesh Fulsundar
 * @owner            : Mahesh Fulsundar
 * @version          : MAHESHFULSUNDAR 1.0 12-06-2018
 * @Notes            : All copyrights are reserved!
 * @rights           : All rights are reserved to the owner of the file creator.
  No one can do change to this file without permission of the owner.
 * @fileName         :
 * @dependancies     :
  ------------------------------------------------------------------------------------- */
include('cconfig.php');

//------------------------------------------------------------------
$msg 	= '';
$type 	= '';

if($_POST['action']=='Add')
{
	//conditions to upload image
	$txtName			 = "bannerImage";
	$tableName			 = "banner";
	$uploadFolderName	 = "../upload";
	$subFolderName		 = "banner";
	$conditionColumnName = "banner_id";
	
	extract($_POST);
	
	$login_id		=	$userId;
	$bannertitle	=	$_POST['bannertitle'];
	$bannerid		=	$_POST['bannerid'];
	$position		=	$_POST['position'];
	$fromDate		=	date('Y-m-d',strtotime($_POST['fromDate']));
	$toDate			=	date('Y-m-d',strtotime($_POST['toDate']));
	
	$amount			=	$_POST['amount'];
	//$referralCode	=	$_POST['referralCode'];
	//$referral_id	=	$_POST['referral_id'];
	
	$imagePath		=	$subFolderName.'/'.$_FILES['bannerImage']['name'];
	
		if($login_id!=1)
		{
			//------------------Insert into the Bannser table--------------------------//
			mysqli_query($con,"INSERT INTO banner 
							   (banner_title, banner_url, banner_position_id, banner_date_added, banner_start_date,  
							    banner_end_date, banner_price, banner_added_by) 
							    VALUES 
							   ('" . $bannertitle . "', '" . $imagePath . "', '" . $position . "', 
							    '" . date('Y-m-d H:i:s') . "', '" . $fromDate . "', '" . $toDate . "', '" . $amount . "',
								'" . $login_id . "')
							  ");  
		}
		else
		{
			//------------------Insert into the Bannser table--------------------------//
			mysqli_query($con,"INSERT INTO banner 
							   (banner_title, banner_url, banner_position_id, banner_date_added, banner_start_date, 
							    banner_end_date, banner_price, banner_added_by,  banner_status) 
								VALUES 
							   ('" . $bannertitle . "', '" . $imagePath . "', '" . $position . "', 
							    '" . date('Y-m-d H;i:s') . "', '" . $fromDate . "', '" . $toDate . "',
								'" . $amount . "', '" . $login_id . "', 'Active')
							   ");  
		}
		
		//-----------------------------------------------------------------------------//
		
		// get last inserted id of product
		$lastBannerId = mysqli_insert_id($con);
		
		//upload image - this is single image upload
		if($_FILES['bannerImage']['name'] != '')
		{
			$condition	= $lastBannerId;
			$columnName	= "banner_url";
			$action		= "update";
					
			//then upload original image
			$columnName	= "banner_url";
			
			upload_image($con,$txtName, $columnName, $tableName, $uploadFolderName, $subFolderName, $conditionColumnName, $condition);
		}
		
		//----------referals(Vendors) Income----------------------------------//
		/*if($referral_id!=0 && $referral_id!='')
		{
			$advertise_income	= getSettingsValues("Advertise Income",$con);
			$income			= ($amount * $advertise_income)/100;
			
			$tdspercentage	= getSettingsValues("TDS",$con);
			$serviceTax		= getSettingsValues("Service Tax",$con);
			
			$tds			= ($income * $tdspercentage)/100;
			$stax			= ($income * $serviceTax)/100;
			$netAmount		= $income - ($tds + $stax);
			
			mysqli_query($con,"INSERT INTO rd_vendor_income
							   (to_user_id, from_user_id ,banner_id, date_time, income, tds, service_tax, net_amount) 
							   VALUES
							   ('" . $referral_id . "', '" . $login_id . "', '" . $lastBannerId . "', 
							    '" . date('Y-m-d H:i:s') . "', '" . $income . "', '" . $tds . "', '" . $stax . "', 
								'" . $netAmount . "')
							  ");
			
		}*/
		
		// check last inserted id is availble or not
		if($lastBannerId > 0)
		{
			$sucmsg = base64_encode(serialize("Banner is added successfully..."));
		}
	header("Location:banners_report.php?sucmsg=$sucmsg");
	exit;
}
else
{
	$lastBannerId		=	$_POST['bannerid'];
	//conditions to upload image
	$txtName			 = "bannerImage";
	$tableName			 = "rd_banner";
	$uploadFolderName	 = "../upload";
	$subFolderName		 = "banner";
	$conditionColumnName = "banner_id";
	
	$imagePath		=	$subFolderName.'/'.$_FILES['bannerImage']['name'];
	//upload image - this is single image upload
	if($_FILES['bannerImage']['name'] != '')
	{
		$condition	= $lastBannerId;
		$columnName	= "banner_url";
		$action		= "update";
				
		//then upload original image
		$columnName	= "banner_url";
		
		upload_image($con,$txtName, $columnName, $tableName, $uploadFolderName, $subFolderName, $conditionColumnName, $condition);
	}
	
	$sucmsg = base64_encode(serialize("Banner is updated successfully..."));
	header("Location:banners_report.php?sucmsg=$sucmsg");
	exit;
}
?>