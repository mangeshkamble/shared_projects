<?php 
/* * ---------------------------------------------------------------------------------
 * @MAHESHFULSUNDAR
 * @author           : Mahesh Fulsundar
 * @owner            : Mahesh Fulsundar
 * @version          : MAHESHFULSUNDAR 1.0 12-06-2018
 * @Notes            : All copyrights are reserved!
 * @rights           : All rights are reserved to the owner of the file creator.
  No one can do change to this file without permission of the owner.
 * @fileName         :
 * @dependancies     :
  ------------------------------------------------------------------------------------- */
include('cconfig.php');
include('header.php');
include('nav.php');
?>
<title>Banners |<?php echo SITENAME;?></title>
<div class="page-inner">
  <div class="page-breadcrumb">
    <ol class="breadcrumb container">
      <li><a href="dashboard.php">Home</a></li>
      <li class="active">Banners</li>
    </ol>
  </div>
  <div class="page-title">
    <div class="container">
      <h3>Banners</h3>
    </div>
  </div>
  <?php if(isset($_GET['errmsg'])) { ?>
  <div class="alert alert-danger" role="alert"> <strong>Sorry !</strong> <a href="#" class="alert-link"><?php echo unserialize(base64_decode($_GET['errmsg']));?></a> </div>
  <?php } 
  if(isset($_GET['infomsg'])) { ?>
  <div class="alert alert-info" role="alert"> <strong>Note : </strong> <a href="#" class="alert-link"><?php echo unserialize(base64_decode($_GET['infomsg']));?></a> </div>
  <?php } 
  if(isset($_GET['sucmsg'])) {?>
  <div class="alert alert-success" role="alert"> <strong>Success !</strong> <a href="#" class="alert-link"><?php echo unserialize(base64_decode($_GET['sucmsg']));?></a> </div>
  <?php } ?>
  <div class="panel panel-white">
    <div class="panel-heading clearfix" align="right"> <a href="banners_add.php">
      <input  class="btn btn-sm btn-success" type="submit" name="submit" value="Add Banner">
      </a></div>
    <div class="panel-body">
      <div class="row m-b-lg table-responsive">
        <?php $counter	= 1;
                
                $ads_details= mysqli_query($con,"SELECT * FROM banner order by banner_id DESC");
                
                $adsCount	= mysqli_num_rows($ads_details);
                if($adsCount>0)
                {
		    ?>
        <table id="example" class="display table" style="width: 100%;" role="grid" aria-describedby="example_info">
          <thead>
            <tr>
              <th>Sr No.</th>
              <th>Image</th>
              <th>Title</th>
              <th>Position</th>
              <th>Start Date</th>
              <th>End Date</th>
              <th>Amount</th>
              <th>Visits</th>
              <th>Status</th>
              <!--<th>Action</th>--> 
            </tr>
          </thead>
          <tfoot>
            <tr>
              <th>Sr No.</th>
              <th>Image</th>
              <th>Title</th>
              <th>Position</th>
              <th>Start Date</th>
              <th>End Date</th>
              <th>Amount</th>
              <th>Visits</th>
              <th>Status</th>
              <!--<th>Action</th>--> 
            </tr>
          </tfoot>
          <tbody>
            <?php
		  while($adsList = mysqli_fetch_assoc($ads_details))
		  { 
			  $banner_id		= $adsList['banner_id'];
			  $Title			= $adsList['banner_title'];
			  $addedBy			= $adsList['banner_added_by'];
			  $referalId		= $adsList['banner_referral_id'];
			  $positionId		= $adsList['banner_position_id'];
			  $bannerImage		= $adsList['banner_url'];
			  $startDate 		= $adsList['banner_start_date'];
			  $endDate 			= $adsList['banner_end_date'];
			  $status			= $adsList['banner_status'];
			  $amount			= round($adsList['banner_price'],2);
			  $visits			=	$adsList['banner_hits_counter'];
			  //get added by details
			  $userDetails	= getUserDetailsByLoginID($con,$addedBy);
			  $addedByCode	= $userDetails['user_id'];
			  
			  //get ref id details
			  $refDetails		= getUserDetails($con,$referalId);
			  $legalName		= $refDetails['legal_name'];
			  
			  //get banner position details
			  $position			= getBannerPosition($con,$positionId);
			  $bannerPosition	= $position['banner_position'];
			  
			  //get image path to display
			  $image_path		= displayImage($bannerImage, '../', 0);
			  
			  $start_date		= date('d/m/Y',strtotime($startDate));
			  $end_date		= date('d/m/Y',strtotime($endDate));
			  
			  if($status == 'Active')
			  {
				  $redirectLink	= "banner_activate.php?bannerId=" . base64_encode($banner_id) . "&stat=" . base64_encode('Inactive') . "";
				  $icon			= "iconfa-remove";
				  $linkTitle		= "Deactivate";
			  } else {
				  $redirectLink	= "banner_activate.php?bannerId=" . base64_encode($banner_id) . "&stat=" . base64_encode('Active') . "";
				  $icon			= "iconfa-ok";
				  $linkTitle		= "Activate";
			  }
			  ?>
            <tr >
              <td><?php echo $counter++;?></td>
              <td><img src="<?php echo $image_path;?>" width="50" height="50" alt="<?php echo $Title;?>"></td>
              <td><?php echo $Title;?></td>
              <td><?php echo $bannerPosition; ?></td>
              <td><?php echo $start_date; ?></td>
              <td><?php echo $end_date; ?></td>
              <td><?php echo $amount; ?></td>
              <td><?php echo $visits;?></td>
              <td><?php  if($status=='Active')
					{ ?>
                <a onclick="banner_activate.php?bannerId=<?php echo base64_encode(serialize($banner_id)).'&stat='. base64_encode(serialize('Active'));?>" title="Active">
                <button type="submit" class="btn btn-success" ><?php echo $status;?></button>
                </a>
                <?php
					}
					else
					{?>
                <a onclick="active(<?php echo $banner_id;?>,'Inactive')" title="Inactive">
                <button type="button" class="btn btn-danger"><?php echo $status;?></button>
                </a>
                <?php
					} ?></td>
              <!--<td><a href="banners_add.php?action=edit&bannerId=<?php //echo base64_encode(serialize($banner_id));?>" title="Edit"><span class="fa fa-pencil"></span></a>&nbsp;  </td>--> 
            </tr>
            <?php
                        }
                        ?>
          </tbody>
        </table>
        <?php
                } else {
					echo "<br>";
                    $error = "No banners yet";
                    
                }
                ?>
        </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
<?php include("footer.php");?>
