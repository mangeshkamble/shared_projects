<?php
/* * ---------------------------------------------------------------------------------
 * @MAHESHFULSUNDAR
 * @author           : Mahesh Fulsundar
 * @owner            : Mahesh Fulsundar
 * @version          : MAHESHFULSUNDAR 1.0 12-06-2018
 * @Notes            : All copyrights are reserved!
 * @rights           : All rights are reserved to the owner of the file creator.
  No one can do change to this file without permission of the owner.
 * @fileName         :
 * @dependancies     :
  ------------------------------------------------------------------------------------- */ 
include('cconfig.php');
include('header.php');
include('nav.php');
?>
<title>Category |<?php echo SITENAME;?></title>
<div class="page-inner">
  <div class="page-breadcrumb">
    <ol class="breadcrumb container">
      <li><a href="dashboard.php">Home</a></li>
      <li class="active">Category</li>
    </ol>
  </div>
  <div class="page-title">
    <div class="container">
      <h3>Category</h3>
    </div>
  </div>
  <?php if(isset($_GET['errmsg'])) { ?>
  <div class="alert alert-danger" role="alert"> <strong>Sorry !</strong> <a href="#" class="alert-link"><?php echo unserialize(base64_decode($_GET['errmsg']));?></a> </div>
  <?php } 
  if(isset($_GET['infomsg'])) { ?>
  <div class="alert alert-info" role="alert"> <strong>Note : </strong> <a href="#" class="alert-link"><?php echo unserialize(base64_decode($_GET['infomsg']));?></a> </div>
  <?php } 
  if(isset($_GET['sucmsg'])) {?>
  <div class="alert alert-success" role="alert"> <strong>Success !</strong> <a href="#" class="alert-link"><?php echo unserialize(base64_decode($_GET['sucmsg']));?></a> </div>
  <?php } ?>
  <div class="panel panel-white">
  <div class="panel-heading clearfix" align="right"> <a href="category_add.php">
      <input  class="btn btn-sm btn-success" type="submit" name="submit" value="Add Category">
      </a></div>
    <div class="panel-body">
    
      <div class="row m-b-lg table-responsive">
        <?php $getproducts	= "SELECT * FROM category WHERE deleteFlag!='Yes' order by category_id desc";								  
		
			$getTransaction	= mysqli_query($con,$getproducts);
		    ?>
        <table id="example" class="display table" style="width: 100%;" role="grid" aria-describedby="example_info">
          <thead>
            <tr>
              <th>Sr</th>
              <th>Category Name (ENG)</th>
              <th>Category Name (MAR)</th>
              <th>Status</th>
              <th>Action</th>
            </tr>
          </thead>
          <tfoot>
            <tr>
               <th>Sr</th>
              <th>Category Name (ENG)</th>
              <th>Category Name (MAR)</th>
              <th>Status</th>
              <th>Action</th>
            </tr>
          </tfoot>
          <tbody>
            <?php
		  $i = 0;
          while($productList	= mysqli_fetch_assoc($getTransaction))
          {
			  
			 $i++;
			$category_id   			= $productList['category_id']; 
			$category_eng		    = $productList['category_eng']; 
			$category_mar		    = $productList['category_mar']; 
			$status		    		= $productList['category_status'];					   			 
              ?>
            <tr>
              <td><?php echo $i;?></td> 
              <td><?php echo $category_eng;?></td> 
              <td><?php echo $category_mar;?></td> 
              <td><?php  if($status=='Active')
					{ ?>
                <a onclick="active(<?php echo $category_id;?>,'Active')" title="Active">
                <button type="button" class="btn btn-success" ><?php echo $status;?></button>
                </a>
                <?php
					}
					else
					{?>
                <a onclick="active(<?php echo $category_id;?>,'Inactive')" title="Inactive">
                <button type="button" class="btn btn-danger"><?php echo $status;?></button>
                </a>
                <?php
					} ?></td>
              <td><a href="category_edit.php?id=<?php echo base64_encode(serialize($category_id));?>" title="Edit"><span class="fa fa-pencil"></span></a>&nbsp; | &nbsp; <a title="Delete" onclick="if(confirm('Do you really want to delete this category?'))
			{ deleteid(<?php echo $category_id;?>); } else { return  false;}"><span class="fa fa-trash"></span></a></td>
            </tr>
            <?php
          }
          ?>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
<script>
function active(id,status)
{			
	$.ajax({
	url:'user_actions.php',
	data: 'status=' + status + '&id=' + id +'&action=active',
	type:"post",
	success:function(data){
		if('success' == data)
		{
			window.location.href = 'category.php?sucmsg=<?php echo base64_encode(serialize("Status updated successfully!"));?>';
		}
		else
		{
			window.location.href = 'category.php?errmsg=<?php echo base64_encode(serialize("Error updating the status!"));?>';
		}
	}
});

}
function deleteid(id)
{
	  
  $.ajax({
  url:'user_actions.php',
  data: 'id=' + id +'&action=delete',
  type:"post",
  success:function(data){
	  if('success' == data)
	  {
		  window.location.href = 'category.php?sucmsg=<?php echo base64_encode(serialize("Category deleted successfully!"));?>';
	  }
	  else
	  {
		  window.location.href = 'category.php?errmsg=<?php echo base64_encode(serialize("Error deleting the Category!"));?>';
	  }
  }
});
	
	
}
</script>
<?php include('footer.php');?>
