<?php
/* * ---------------------------------------------------------------------------------
 * @MAHESHFULSUNDAR
 * @author           : Mahesh Fulsundar
 * @owner            : Mahesh Fulsundar
 * @version          : MAHESHFULSUNDAR 1.0 12-06-2018
 * @Notes            : All copyrights are reserved!
 * @rights           : All rights are reserved to the owner of the file creator.
  No one can do change to this file without permission of the owner.
 * @fileName         :
 * @dependancies     :
  ------------------------------------------------------------------------------------- */
include('cconfig.php');

//=========if form is posted ===========================// 
// code to update into database
if (isset($_POST['submit'])) {

    $category_eng = $_POST['category1'];
    $category_mar = $_POST['category2'];
    $status = $_POST['status'];
    $cat_image = '';
    
    if($_FILES['product_category_image1'] && $_FILES["product_category_image1"]["name"]!=='')
    {	
        $name 				= 	$_FILES["product_category_image1"]["name"];
        $extension	 		=	 end((explode(".", $name)));
        $filetype	 		= 	$_FILES['product_category_image1']['type'];
        $size 				= 	$_FILES['product_category_image1']['size'];
        $destinationPath 	= 	'../upload/products/';

        $idProofFileName	=	time();
        $idProofFileName	=	'1-'.$idProofFileName.'.'.$extension;
        $image1				= 	'../upload/products/'.$idProofFileName;

        move_uploaded_file($_FILES["product_category_image1"]["tmp_name"],$image1);  
        $cat_image				= 	'upload/products/'.$idProofFileName;
    }

    $myquery = mysqli_query($con, "SELECT * FROM category WHERE category_eng='" . $category_eng . "'");
    $mycount = mysqli_num_rows($myquery);
    if ($mycount > 0) {
        $errmsg = base64_encode(serialize("Category name already Exist!"));
        header("Location:category.php?errmsg=$errmsg");
        exit;
    } else {
        $query = mysqli_query($con, "INSERT INTO category(category_eng,category_mar,category_status,category_image) VALUES  ('" . $category_eng . "','" . $category_mar . "','" . $status . "','".$cat_image."')");
        $sucmsg = base64_encode(serialize("Category added sucessfully!"));
        header("Location:category.php?sucmsg=$sucmsg");
        exit;
    }
}

include('header.php');
include('nav.php');
?>
<title>Category Add |<?php echo SITENAME; ?></title>
<div class="page-inner">
    <div class="page-breadcrumb">
        <ol class="breadcrumb container">
            <li><a href="dashboard.php">Home</a></li>
            <li class="active">Category Add</li>
        </ol>
    </div>
    <div class="page-title">
        <div class="container">
            <h3>Category Add</h3>
        </div>
    </div>
    <div id="main-wrapper" class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-white">
                    <div class="panel-body">
                        <form class="form-horizontal" id="category" name="category" method="post" enctype="multipart/form-data">
                            <?php foreach ($multiLang as $lan) {
                                ?>
                                <div class="form-group">
                                    <label for="input-Default" class="col-sm-2 control-label">Category Name <?php echo ucfirst($lan['lang']); ?>: </label>
                                    <div class="col-sm-10">
                                        <input type="text" name="category<?php echo $lan['lang_id']; ?>" id="category<?php echo $lan['lang_id']; ?>" class="form-control">
                                    </div>
                                </div>
                            <?php } ?>
                            <div class="form-group">
                                <label for="input-Default" class="col-sm-2 control-label">Upload Image : </label>
                                <div class="col-sm-4">
                                    <input type="file" name="product_category_image1" id="product_category_image1" class="text-center event-name gui-input br-light bg-light" placeholder="Upload Image">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="input-Default" class="col-sm-2 control-label">Status : </label>
                                <div class="col-sm-4">
                                    <select name="status" id="status" class="form-control">
                                        <option value="Active">Active</option>
                                        <option value="Inactive">Inactive</option>
                                    </select> 
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-sm-offset-10 col-sm-10">
                                    <input  class="btn btn-sm btn-success" type="submit" name="submit" value="Submit">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php include('footer.php'); ?>
    <script>
        $(document).ready(function (e) {

            $("#date").datepicker({
                //minDate: ,
                maxDate: 0,
            });
            //validation for topuplimit check
            $('#category').validate({
                rules: {
                    category:
                            {
                                required: true,
                            },

                },
            });
        });
    </script>