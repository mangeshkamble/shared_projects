<?php
/* * ---------------------------------------------------------------------------------
 * @MAHESHFULSUNDAR
 * @author           : Mahesh Fulsundar
 * @owner            : Mahesh Fulsundar
 * @version          : MAHESHFULSUNDAR 1.0 12-06-2018
 * @Notes            : All copyrights are reserved!
 * @rights           : All rights are reserved to the owner of the file creator.
  No one can do change to this file without permission of the owner.
 * @fileName         :
 * @dependancies     :
  ------------------------------------------------------------------------------------- */
include('cconfig.php');

//=========if form is posted ===========================// 
// code to update into database
if (isset($_POST['submit'])) {
    $category_id = $_POST['category_id'];
    $category1 = $_POST['category1'];
    $category2 = $_POST['category2'];
    $status = $_POST['status'];
    $cat_image = '';
    $update_image = '';
    
    if($_FILES['edit_product_category_image1'] && $_FILES["edit_product_category_image1"]["name"]!=='')
    {	
        $name 				= 	$_FILES["edit_product_category_image1"]["name"];
        $extension	 		=	 end((explode(".", $name)));
        $filetype	 		= 	$_FILES['edit_product_category_image1']['type'];
        $size 				= 	$_FILES['edit_product_category_image1']['size'];
        $destinationPath 	= 	'../upload/products/';

        $idProofFileName	=	time();
        $idProofFileName	=	'1-'.$idProofFileName.'.'.$extension;
        $image1				= 	'../upload/products/'.$idProofFileName;

        move_uploaded_file($_FILES["edit_product_category_image1"]["tmp_name"],$image1);  
        $cat_image				= 	'upload/products/'.$idProofFileName;
    }
    
    if($cat_image!=''){
        $update_image = " ,category_image='".$cat_image."'";
    }

    $query = mysqli_query($con, "UPDATE category SET category_eng='" . $category1 . "',category_mar='" . $category2 . "',category_status ='" . $status . "' $update_image WHERE category_id=" . $category_id);
    $sucmsg = base64_encode(serialize("Category updated sucessfully!"));
    header("Location:category.php?sucmsg=$sucmsg");
    exit;
}

$myQ = mysqli_query($con, "Select * FROM category WHERE category_id=" . unserialize(base64_decode($_GET['id'])));
$result = mysqli_fetch_assoc($myQ);
include('header.php');
include('nav.php');
?>
<title>Category Edit |<?php echo SITENAME; ?></title>
<div class="page-inner">
    <div class="page-breadcrumb">
        <ol class="breadcrumb container">
            <li><a href="dashboard.php">Home</a></li>
            <li class="active">Category Edit</li>
        </ol>
    </div>
    <div class="page-title">
        <div class="container">
            <h3>Category Edit</h3>
        </div>
    </div>
    <div id="main-wrapper" class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-white">
                    <div class="panel-body">
                        <form class="form-horizontal" action="#" id="category" name="category" method="post" enctype="multipart/form-data">
                            <div class="form-group">
                                <label for="input-Default" class="col-sm-2 control-label">Category Name (English): </label>
                                <div class="col-sm-10">
                                    <input type="text" name="category1" id="category1" class="form-control" value="<?php echo $result['category_eng']; ?>">
                                    <input type="hidden" name="category_id" id="category_id" value="<?php echo unserialize(base64_decode($_GET['id'])); ?>" />
                                </div>
                            </div><div class="form-group">
                                <label for="input-Default" class="col-sm-2 control-label">Category Name (Marathi): </label>
                                <div class="col-sm-10">
                                    <input type="text" name="category2" id="category2" class="form-control" value="<?php echo $result['category_mar']; ?>"> 
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="input-Default" class="col-sm-2 control-label">Upload Image : </label>
                                <div class="col-sm-4">
                                    <input value="<?php echo $result['category_image']; ?>" type="file" name="edit_product_category_image1" id="edit_product_category_image1" class="text-center event-name gui-input br-light bg-light" placeholder="Upload Image">
                                    <span><?php echo end((explode("/", $result['category_image']))); ?></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="input-Default" class="col-sm-2 control-label">Status : </label>
                                <div class="col-sm-4">
                                    <select name="status" id="status" class="form-control">
                                        <option value="Active" <?php if ($result['category_status'] == 'Active') echo 'Selected'; ?>>Active</option>
                                        <option value="Inactive" <?php if ($result['category_status'] == 'Inactive') echo 'Selected'; ?>>Inactive</option>
                                    </select> 
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-sm-offset-10 col-sm-10">
                                    <input  class="btn btn-sm btn-success" type="submit" name="submit" value="Submit">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php include('footer.php'); ?>
    <script>
        $(document).ready(function (e) {

            //validation for topuplimit check
            $('#category1').validate({
                rules: {
                    category:
                            {
                                required: true,
                            },

                },
            });
        });
    </script>