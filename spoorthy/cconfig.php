<?php
session_start();
/* * ---------------------------------------------------------------------------------
 * @MAHESHFULSUNDAR
 * @author           : Mahesh Fulsundar
 * @owner            : Mahesh Fulsundar
 * @version          : MAHESHFULSUNDAR 1.0 12-06-2018
 * @Notes            : All copyrights are reserved!
 * @rights           : All rights are reserved to the owner of the file creator.
  No one can do change to this file without permission of the owner.
 * @fileName         :
 * @dependancies     :
session_start();
  ------------------------------------------------------------------------------------- */
include('config.php');
include('function.php');

date_default_timezone_set('Asia/Kolkata');

$con = connectDatabse();

if ((!isset($_SESSION['UserId']) || $_SESSION['UserId'] == "")) {
    header("location:index.php");
    exit;
}
$multiLang  =getLanguages($con);
//------------Create Global connection variable ---------------// 
$userId			=	$_SESSION['UserId'];
$balance		=	$_SESSION['balance'];
$userName		=	$_SESSION['userName'];
$email			=	$_SESSION['email'];
$compName		=	$_SESSION['compName'];
$login_ip		=	$_SESSION['login_ip'];

define('SITENAME',' Spoorthy Bazaar');
define('SITEURL','spoorthybazaar.com');
define('GLOBAL_URL','http://spoorthybazaar.com');
define('NO_REPLY_EMAIL','no-reply@spoorthybazaar.com');
define('SUPPORTEMAIL','support@spoorthybazaar.com');

define('CONTACTNO',9876543210);

//Constat for the TransactionID 
define('TXNCONSTANT', 'SPB');
?>