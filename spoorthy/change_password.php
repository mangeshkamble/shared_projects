<?php 
/* * ---------------------------------------------------------------------------------
 * @MAHESHFULSUNDAR
 * @author           : Mahesh Fulsundar
 * @owner            : Mahesh Fulsundar
 * @version          : MAHESHFULSUNDAR 1.0 12-06-2018
 * @Notes            : All copyrights are reserved!
 * @rights           : All rights are reserved to the owner of the file creator.
  No one can do change to this file without permission of the owner.
 * @fileName         :
 * @dependancies     :
  ------------------------------------------------------------------------------------- */
include('cconfig.php'); 
 
if(isset($_POST['submit']))
{
	extract($_POST);
    $password = $_POST['newPassword'];
    //--------CHECK FOR OLD PASSWORD-------------------------------------// 
	$getold			=	mysqli_query($con,"SELECT * FROM user WHERE id=".$userId." AND password='".hash('sha256',$_POST['oldPassword'])."'");
	$OldCount		=	mysqli_num_rows($getold); 
	if($OldCount>0)
	{ 	
	mysqli_query($con,"UPDATE user SET password='".hash('sha256',$password)."',plain_text='".$password."' WHERE id='".$userId."'");	
	//--------------Sending Email--------------------------------------    
	$arrstrRequest['userid']  		 = $userId; 	
	$arrstrRequest['password']		 = $password;
	$arrstrRequest['name']     		 = $compName;
	$arrstrRequest['emailid']  		 = $email;
	$arrstrRequest['SITEURL'] 		 = SITEURL;
	$arrstrRequest['GLOBAL_URL']	 = GLOBAL_URL;
	$arrstrRequest['SITENAME'] 		 = SITENAME;
	$arrstrRequest['NO_REPLY_EMAIL'] = NO_REPLY_EMAIL;
	//$arrstrRequest['logo']		=	'';	
	
	
	//Email::sendEmailRequest('changePassword',$arrstrRequest);
	//---------------------------------------------------------------------
		 
	 $msg = base64_encode(serialize('Your password is successfully changed and sent to your registered email Id.'));
	 header("Location:change_password.php?sucmsg=$msg");
	 exit; 
	 }
	else
	{
		$msg = 'Incorrect Old Password, Please try again!';
		$errmsg = base64_encode(serialize($msg));
		header('Location:change_password.php?errmsg='.$errmsg);
		exit;		
	}
}
include('header.php');
include('nav.php');

?>
<title>Change Password |<?php echo SITENAME;?></title>
<div class="page-inner">
<div class="page-breadcrumb">
  <ol class="breadcrumb container">
    <li><a href="dashboard.php">Home</a></li>
    <li class="active">Change Password</li>
  </ol>
</div>
<div class="page-title">
  <div class="container">
    <h3>Change Password</h3>
  </div>
</div>
<div id="main-wrapper" class="container">
<div class="row">
  <div class="col-md-12">
  <?php if(isset($_GET['errmsg'])) { ?>
  <div class="alert alert-danger" role="alert">
	  <strong>Sorry !</strong> <a href="#" class="alert-link"><?php echo unserialize(base64_decode($_GET['errmsg']));?></a>
  </div>
  <?php } 
  if(isset($_GET['infomsg'])) { ?>
  <div class="alert alert-info" role="alert">
	  <strong>Note : </strong> <a href="#" class="alert-link"><?php echo unserialize(base64_decode($_GET['infomsg']));?></a>
  </div>
  <?php } 
  if(isset($_GET['sucmsg'])) {?>
  <div class="alert alert-success" role="alert">
	  <strong>Success !</strong> <a href="#" class="alert-link"><?php echo unserialize(base64_decode($_GET['sucmsg']));?></a>
  </div>
  <?php } ?>
    <div class="panel panel-white">
      <div class="panel-heading clearfix"> </div>
      <div class="panel-body">
        <form class="form-horizontal" method="post" name="changePassword" id="changePassword">
          <div class="form-group">
            <label for="inputEmail3" class="col-sm-2 control-label">Old Password :  </label>
            <div class="col-sm-10">
              <input type="password" class="form-control" id="oldPassword" name="oldPassword" placeholder="old password">
            </div>
          </div>
          <div class="form-group">
            <label for="inputPassword3" class="col-sm-2 control-label"> New Password :  </label>
            <div class="col-sm-10">
              <input type="password" name="newPassword" class="form-control" id="newPassword" placeholder=" new Password">
            </div>
          </div>
          <div class="form-group">
            <label for="inputPassword3" class="col-sm-2 control-label"> Confirm Password :  </label>
            <div class="col-sm-10">
              <input type="password" class="form-control"  name="confirmPassword" id="confirmPassword" placeholder=" confirm Password">
            </div>
          </div>
         
          <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
              <input  class="btn btn-sm btn-success" type="submit" name="submit" value="Update Password">
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
  </div>
  </div>
  <?php include('footer.php');?>
  <script>
$(document).ready(function(e) {
 
		//validation for topuplimit check
			$('#changePassword').validate({
				rules:{
					oldPassword:{
						required:true,
						},
					newPassword:{
						required:true,
						minlength:5,
						},
						confirmPassword:
						{
							required:true,
							minlength:5,
							equalTo:newPassword,
						},
					},				
				});
});
</script>
