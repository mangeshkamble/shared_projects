<?php 
/* * ---------------------------------------------------------------------------------
 * @MAHESHFULSUNDAR
 * @author           : Mahesh Fulsundar
 * @owner            : Mahesh Fulsundar
 * @version          : MAHESHFULSUNDAR 1.0 12-06-2018
 * @Notes            : All copyrights are reserved!
 * @rights           : All rights are reserved to the owner of the file creator.
  No one can do change to this file without permission of the owner.
 * @fileName         :
 * @dependancies     :
  ------------------------------------------------------------------------------------- */
//-------Connection to the DB---------------------------------------//
function connectDatabse()
{
	$connection = mysqli_connect('127.0.0.1','root','root','spoorthy');
        return $connection;
}

//----------------disconnect from DB--------------------------------//	
function closeDatabase($con)
{
	mysqli_close($con);
}
 
?>