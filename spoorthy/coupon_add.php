<?php 
/* * ---------------------------------------------------------------------------------
 * @MAHESHFULSUNDAR
 * @author           : Mahesh Fulsundar
 * @owner            : Mahesh Fulsundar
 * @version          : MAHESHFULSUNDAR 1.0 12-06-2018
 * @Notes            : All copyrights are reserved!
 * @rights           : All rights are reserved to the owner of the file creator.
  No one can do change to this file without permission of the owner.
 * @fileName         :
 * @dependancies     :
  ------------------------------------------------------------------------------------- */
include('cconfig.php');

if(isset($_POST['submit']))
{
	$numberOfCupons = $_POST['noofcupons'];
	$coupon_type 	= $_POST['coupon_type'];
	$length 		= $_POST['position'];
	$fromDate 		= date('Y-m-d',strtotime($_POST['fromDate']));
	$toDate 		= date('Y-m-d',strtotime($_POST['toDate']));
	$amount 		= $_POST['amount'];
	for($i=1;$i<=$numberOfCupons;$i++)
	{
		$coupon_code 	= randstring($length);  
		$getBanner		= mysqli_query($con,"SELECT coupon_code FROM coupons WHERE coupon_code='".$coupon_code."'");  
		if(mysqli_num_rows($getBanner)==0)
		{
			mysqli_query($con,"INSERT INTO coupons (coupon_type,coupon_code,start_date,end_date,amount) VALUES ('".$coupon_type."','".$coupon_code."','".$fromDate."','".$toDate."','".$amount."')");
		}
	}
	$sucmsg	=	base64_encode(serialize("Cupons created sucessfully!"));
	header("Location:coupon_reports.php?sucmsg=$sucmsg");
	exit;
 }

include('header.php');
include('nav.php');
 ?>
<script>
 
jQuery(document).ready(function()
{
  $('#check').hide();
  $('#uncheck').hide();
	  
	jQuery("#fromDate").datepicker({
		minDate:0,
		onSelect: function(date) {
		  var fromDate1 = jQuery('#fromDate').datepicker('getDate');
		  fromDate1.setDate(fromDate1.getDate() + 1);
		}
	});	
	jQuery("#toDate").datepicker({
		minDate:0,
		onSelect: function(date) {
		  var fromDate1 = jQuery('#toDate').datepicker('getDate');
		  fromDate1.setDate(fromDate1.getDate() + 1);
		}
	});	
});

function verify_referral_code(code)
{
 	$.ajax({
	 type: "POST",
	 url: "../verify_refcode.php",
	// data: dataString,
	 data : { refcode : code},
	 cache: false,
	 success: function(result){
		 myresult	=	JSON.parse(result);
		 
		if(myresult.count ==1)
		{ 
			$('#check').show();
			$('#uncheck').hide();
			$('#referral_id').val(myresult.userID);
			$('#ref_name').html(myresult.name);
		}
		else
		{
			$('#uncheck').show();
			$('#check').hide();
			$('#referral_id').val('0');
			$('#ref_name').html(myresult.msg);
		}
	 }
   });
}


 
 

function convertDate(inputFormat) {
  function pad(s) { return (s < 10) ? '0' + s : s; }
  var d = new Date(inputFormat);
  return [ pad(d.getMonth()+1),pad(d.getDate()), d.getFullYear()].join('/');
} 
</script>
<style>
.input-xxlarge {
	width: 33%
}
select {
	width: 34%;
}
</style>
<title>Create Cupon |<?php echo SITENAME;?></title>
<div class="page-inner">
<div class="page-breadcrumb">
  <ol class="breadcrumb container">
    <li><a href="dashboard.php">Home</a></li>
    <li class="active">Create Cupon</li>
  </ol>
</div>
<div class="page-title">
  <div class="container">
    <h3>Create Cupon</h3>
  </div>
</div>
<div id="main-wrapper" class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="panel panel-white">
          <div class="panel-heading clearfix">
            <h4 class="panel-title"> Create Cupon</h4>
          </div>
          <div class="panel-body">
            <form class="form-horizontal" enctype="multipart/form-data" name="addbannersForm" id="addbannersForm" method="post">
              <div class="form-group">
              <label for="input-help-block" class="col-sm-2 control-label">Cupon Type : </label>
                <div class="col-sm-5">
                  <select name="coupon_type" id="coupon_type" class="form-control">
				  <option value='Diwali offers'>Diwali offers</option>
				  <option value='Mansoon offers'>Mansoon offers</option>
				  <option value='Summer offers'>Summer offers</option>
				  </select> 
                </div>
              </div>
              <div class="form-group">
              <label for="input-help-block" class="col-sm-2 control-label">Length of Code : </label>
                <div class="col-sm-5">
                 <select class="form-control" name="position" id="position">
                <option value="0">-- Select --</option>
                <option value="5">5</option>  
                <option value="6">6</option>  
                <option value="7">7</option>  
                <option value="8">8</option>  
                <option value="9">9</option>  
                <option value="10">10</option>  
              </select>
                </div>
              </div> 
               <div class="form-group">
              <label for="input-help-block" class="col-sm-2 control-label">From Date : </label>
                <div class="col-sm-5">
                <input type="text" name="fromDate" id="fromDate" class="form-control" placeholder="mm/dd/yyyy"/>
                </div>
              </div>
			   <div class="form-group">
              <label for="input-help-block" class="col-sm-2 control-label">To Date : </label>
                <div class="col-sm-5">
                <input type="text" name="toDate" id="toDate" class="form-control" placeholder="mm/dd/yyyy"/>
                </div>
              </div> 
               <div class="form-group">
              <label for="input-help-block" class="col-sm-2 control-label">Amount : </label>
                <div class="col-sm-5">
                 <input type="text" name="amount" id="amount" class="form-control"/> 
                </div>
              </div> 
			  <div class="form-group">
              <label for="input-help-block" class="col-sm-2 control-label">No of Cupons : </label>
                <div class="col-sm-5">
                 <input type="text" name="noofcupons" id="noofcupons" class="form-control"/> 
                </div>
              </div> 
<hr>
              <div class="form-group col-md-12" align="right"> 
                 <input type="submit" name="submit" id="submit" class="btn btn-primary btn-addon m-b-sm" value="Create" />
              </div> 
          </form>
       </div>
        </div>
      </div>
    </div>
  </div>
 <?php include("footer.php");?>