<?php 
/* * ---------------------------------------------------------------------------------
 * @MAHESHFULSUNDAR
 * @author           : Mahesh Fulsundar
 * @owner            : Mahesh Fulsundar
 * @version          : MAHESHFULSUNDAR 1.0 12-06-2018
 * @Notes            : All copyrights are reserved!
 * @rights           : All rights are reserved to the owner of the file creator.
  No one can do change to this file without permission of the owner.
 * @fileName         :
 * @dependancies     :
  ------------------------------------------------------------------------------------- */
include('cconfig.php');
include('header.php');
include('nav.php');
?>
<title>Discount Cupons |<?php echo SITENAME;?></title>
<div class="page-inner">
  <div class="page-breadcrumb">
    <ol class="breadcrumb container">
      <li><a href="dashboard.php">Home</a></li>
      <li class="active">Discount Cupons</li>
    </ol>
  </div>
  <div class="page-title">
    <div class="container">
      <h3>Discount Cupons</h3>
    </div>
  </div>
  <?php if(isset($_GET['errmsg'])) { ?>
  <div class="alert alert-danger" role="alert"> <strong>Sorry !</strong> <a href="#" class="alert-link"><?php echo unserialize(base64_decode($_GET['errmsg']));?></a> </div>
  <?php } 
  if(isset($_GET['infomsg'])) { ?>
  <div class="alert alert-info" role="alert"> <strong>Note : </strong> <a href="#" class="alert-link"><?php echo unserialize(base64_decode($_GET['infomsg']));?></a> </div>
  <?php } 
  if(isset($_GET['sucmsg'])) {?>
  <div class="alert alert-success" role="alert"> <strong>Success !</strong> <a href="#" class="alert-link"><?php echo unserialize(base64_decode($_GET['sucmsg']));?></a> </div>
  <?php } ?>
  <div class="panel panel-white">
    <div class="panel-heading clearfix" align="right"> <a href="coupon_add.php">
      <input  class="btn btn-sm btn-success" type="submit" name="submit" value="Create Cupons">
      </a></div>
    <div class="panel-body">
      <div class="row m-b-lg table-responsive">
        <?php $counter	= 1;
                
                $ads_details= mysqli_query($con,"SELECT count(id) as total,coupon_type,start_date,end_date,amount FROM coupons group by coupon_type");
                
                $adsCount	= mysqli_num_rows($ads_details);
                if($adsCount>0)
                {
		    ?>
        <table id="example" class="display table" style="width: 100%;" role="grid" aria-describedby="example_info">
          <thead>
            <tr>
              <th>Sr No.</th>
              <th>Cupons Type</th>
              <th>Total Cupons</th>
              <th>Amount</th>
              <th>Start Date</th>
              <th>End Date</th> 
            </tr>
          </thead>
          <tfoot>
            <tr>
              <th>Sr No.</th>
              <th>Cupons Type</th>
              <th>Total Cupons</th>
              <th>Amount</th>
              <th>Start Date</th>
              <th>End Date</th> 
            </tr>
          </tfoot>
          <tbody>
            <?php
		  while($adsList = mysqli_fetch_assoc($ads_details))
		  { 
			  $total		= $adsList['total'];
			  $coupon_type	= $adsList['coupon_type'];
			  $start_time	= date('d/m/Y',strtotime($adsList['start_date']));
			  $end_time		= date('d/m/Y',strtotime($adsList['end_date']));
			  $amount		= $adsList['amount']; 
			 
			  ?>
            <tr >
              <td><?php echo $counter++;?></td>
              <td><?php echo $coupon_type;?></td>
              <td><?php echo $total;?></td>
              <td><?php echo $amount; ?></td>
              <td><?php echo $start_time; ?></td>
              <td><?php echo $end_time; ?></td> 
               
            </tr>
            <?php
                        }
                        ?>
          </tbody>
        </table>
        <?php
                } else {
					echo "<br>";
                    $error = "No Cupons yet";
                    
                }
                ?>
        </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
<?php include("footer.php");?>
