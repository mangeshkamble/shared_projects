<?php 
if (isset($_POST['submit'])) {
    echo "--akjsdf--";
    print_r($_POST);exit;
}

include('cconfig.php');
include('header.php');
include('nav.php');
?>
<title>Sales Order |<?php echo SITENAME;?></title>
<div class="page-inner">
  <div class="page-breadcrumb">
    <ol class="breadcrumb container">
      <li><a href="dashboard.php">Home</a></li>
      <li class="active">Sales Order</li>
    </ol>
  </div>
  <div class="page-title">
    <div class="container">
      <h3>Sales Order</h3>
    </div>
  </div>
  <div class="panel panel-white">
    <div class="panel-body">
        <div class="col-lg-12">
            <div class="col-lg-8">
                <form class="form-horizontal" enctype="multipart/form-data" name="cart" id="cart" method="post" action="create_sales_order.php">
                <div class="container-fluid">
  <div class="row">
    <?php
        $sql = "SELECT * FROM products order by product_id DESC";
        $getProducts	= mysqli_query($con,$sql);
        while($productList	= mysqli_fetch_assoc($getProducts))
        {
    ?>
  <!-- cart section starts -->
    <div class="col-xs-12 col-sm-4 col-md-3 ">
      <div class="col-xs-12 ItemSection well TopBuffer text-center">
        <div class="row">
          <div class="container-fluid">
            <img src="<?php echo $productList['image1'];?>" class="img-responsive img-rounded" />
          </div>
        </div>
        <div class="row">
          <div class="container-fluid DividerSection">
            <h5 class="ItemHeading"><b><?php echo $productList['product_name'];?></b></h5>
            <input class="form-control" type="hidden" name="product_id" id="product_id" value="<?php echo $productList['product_id'];?>">
            <!--<p style="line-height:10px"><?php //echo $productList['product_brand'];?></p>-->
          </div>
        </div>
        <div class="row">
          <div class="container-fluid">
              <h4 class="pull-left"><b><span class="label label-default price-tag"><?php echo $productList['product_price'];?>/-</span></b></h4>
              <input type="hidden" name="t_product_price" id="t_product_price" value="<?php echo $productList['product_price'];?>">
              <h4 class="pull-right"><button type="submit" name="submit" class="btn btn-success btn-xs"><span class="glyphicon glyphicon-shopping-cart"></span></button></h4>
            </div>
            <div class="container-fluid">
              <div class="form-group">
                <input type="text" id="t_product_qty" name="t_product_qty" class="form-control" placeholder=""/>
              </div>
          </div>
        </div>
<!--        <div class="row">
          <div class="container-fluid">
            <div class="btn-group btn-group-justified">
              <a href="#" class="btn btn-sm btn-default"><span class="glyphicon glyphicon-ok"></span></a>
              <a href="#" class="btn btn-sm btn-default"><span class="glyphicon glyphicon-envelope"></span></a>
              <a href="#" class="btn btn-sm btn-default"><span class="glyphicon glyphicon-star"></span></a>
            </div>
          </div>
        </div>-->
      </div>
    </div>
    <div class="col-xs-12 hidden-sm hidden-md hidden-lg DividerSection"></div>
    <?php
        }
    ?>
<!-- cart section ends -->
  </div>
</div>
                </form>
            </div>
            <div class="col-lg-4">
                <?php include_once 'sales_order_cart.php'; ?>
            </div>
        </div>
        
    </div>
  </div>
</div>
<?php include('footer.php');?>

<style>
.ItemSection .img-responsive { margin: 0 auto; }
.TopBuffer { margin-top:16px; }
.DividerSection{ border-bottom: 1px solid #e0e0e0; }
.price-tag{background : #635f5f !important}
h5.ItemHeading {
    overflow:hidden;
    white-space:nowrap;
    -ms-text-overflow:ellipsis;
    text-overflow:ellipsis;
    width:80%;
    /* max-height:0px; */
    margin:0 auto;
    padding:9px 0px;
} 
/*h5:hover {
    overflow:visible;
}*/

.ItemSection .form-group{
  margin-bottom:7px;
}
</style>
