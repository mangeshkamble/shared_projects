<?php  
/* * ---------------------------------------------------------------------------------
 * @MAHESHFULSUNDAR
 * @author           : Mahesh Fulsundar
 * @owner            : Mahesh Fulsundar
 * @version          : MAHESHFULSUNDAR 1.0 12-06-2018
 * @Notes            : All copyrights are reserved!
 * @rights           : All rights are reserved to the owner of the file creator.
  No one can do change to this file without permission of the owner.
 * @fileName         :
 * @dependancies     :
  ------------------------------------------------------------------------------------- */
include('cconfig.php');
include('header.php');
include('nav.php'); 
?>
<title>View Customer |<?php echo SITENAME;?></title>
<div class="page-inner">
  <div class="page-breadcrumb">
    <ol class="breadcrumb container">
      <li><a href="dashboard.php">Home</a></li>
      <li class="active">View Customer</li>
    </ol>
  </div>
  <div class="page-title">
    <div class="container">
      <h3>View Customer</h3>
    </div>
  </div>
  <div id="main-wrapper" class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="panel panel-white">
          <div class="panel-body">
          	  <?php 
          $customerId	= unserialize(base64_decode($_GET['customerId']));
          $getcustomer	= mysqli_query($con,"SELECT * FROM customers WHERE customer_id='" . $customerId . "'");
          $customer		= mysqli_fetch_assoc($getcustomer);
          $customer_id		= $customer['customer_id'];
          //$user_id			= $customer['userId'];
          $full_name		= $customer['full_name'];
          $mobile_no		= $customer['mobile_no'];
          $email_id			= $customer['email_id'];
          $gender			= $customer['gender'];
          $street_address	= $customer['street_address'];
          $landmark			= $customer['landmark'];	
          $city_id			= $customer['city_id'];
          $cityDetails		= getCityDetails($con,$city_id);
          $cityName			= $cityDetails['city_name'];			
          $state_id			= $customer['state_id'];
          $stateDetails		= getstateDetails($con,$state_id);
          $stateName		= $stateDetails['state'];
          $country_id		= $customer['country_id'];
		  $countryDetails	= getCountry($con,$country_id);
		  $countryName		= $countryDetails['name'];
          $pincode			= $customer['pincode'];
          $contact_no		= $customer['contact_no'];
          $date_added		= date('d/m/Y',strtotime($customer['date_added']));
          $status			= $customer['status'];
          ?>
          <form class="form-horizontal" id="category" name="category" method="post" enctype="multipart/form-data">
              <div class="form-group">
                <label for="input-Default" class="col-sm-2 control-label">Customer Name : </label>
                <div class="col-sm-5">
                  <input type="text" name="title" id="title" value=" <?php echo $full_name;?>" disabled="disabled" class="form-control">
                </div>
              </div>
              <div class="form-group">
                <label for="input-Default" class="col-sm-2 control-label">Mobile No : </label>
                <div class="col-sm-5">
                  <input type="text" name="title" id="title" value=" <?php echo $mobile_no;?>" disabled="disabled" class="form-control">
                </div>
              </div>
              <div class="form-group">
                <label for="input-Default" class="col-sm-2 control-label">Email : </label>
                <div class="col-sm-5">
                  <input type="text" name="title" id="title" value=" <?php echo $email_id;?>" disabled="disabled" class="form-control">
                </div>
              </div>
              <div class="form-group">
                <label for="input-Default" class="col-sm-2 control-label">Gender : </label>
                <div class="col-sm-5">
                  <input type="text" name="title" id="title" value=" <?php echo $gender;?>" disabled="disabled" class="form-control">
                </div>
              </div>
              <div class="form-group">
                <label for="input-Default" class="col-sm-2 control-label">Address : </label>
                <div class="col-sm-5">
                  <textarea name="title" id="title" disabled="disabled" class="form-control"><?php echo $street_address.' '.$landmark.', '.$cityName.', '.$stateName.' ,'.$countryName.' - '.$pincode;?></textarea>
                </div>
              </div>
              <div class="form-group">
                <label for="input-Default" class="col-sm-2 control-label">Contact No : </label>
                <div class="col-sm-5">
                  <input type="text" name="title" id="title" value=" <?php echo $contact_no;?>" disabled="disabled" class="form-control">
                </div>
              </div>
              <div class="form-group">
                <label for="input-Default" class="col-sm-2 control-label">Entry Date : </label>
                <div class="col-sm-5">
                  <input type="text" name="title" id="title" value=" <?php echo $date_added;?>" disabled="disabled" class="form-control">
                </div>
              </div>
              <div class="form-group">
                <label for="input-Default" class="col-sm-2 control-label">Status : </label>
                <div class="col-sm-5">
                  <input type="text" name="title" id="title" value=" <?php echo $status;?>" disabled="disabled" class="form-control">
                </div>
              </div>
           </form>
          </div>
        </div>
      </div>
    </div>
  </div>
      <?php include("footer.php");?>