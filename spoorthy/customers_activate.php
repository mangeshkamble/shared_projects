<?php 
/* * ---------------------------------------------------------------------------------
 * @MAHESHFULSUNDAR
 * @author           : Mahesh Fulsundar
 * @owner            : Mahesh Fulsundar
 * @version          : MAHESHFULSUNDAR 1.0 12-06-2018
 * @Notes            : All copyrights are reserved!
 * @rights           : All rights are reserved to the owner of the file creator.
  No one can do change to this file without permission of the owner.
 * @fileName         :
 * @dependancies     :
  ------------------------------------------------------------------------------------- */
include('cconfig.php');
session_start(); 

if(isset($_GET['customerId']))
{
	$customerId	=	unserialize(base64_decode($_GET['customerId']));		
	$status		=	unserialize(base64_decode($_GET['stat']));
	
	if($status	=='Active')
	{
		$updateReview		=	mysqli_query($con,"UPDATE customers SET status='Inactive' WHERE customer_id='$customerId'"); 
	}
	else
	{
		$updateReview		=	mysqli_query($con,"UPDATE customers SET status='Active' WHERE customer_id='$customerId'"); 
	}

	$sucmsg	=	base64_encode(serialize("Customer status changed successfully !"));
	header("Location:customers_report.php?sucmsg=$sucmsg");
	exit;
}
else
{
	$errmsg	=	base64_encode(serialize("Oop's somthing went wrong please try again!"));
	header("Location:customers_report.php?errmsg=$errmsg");
	exit; 
} 
?>