<?php 
/* * ---------------------------------------------------------------------------------
 * @MAHESHFULSUNDAR
 * @author           : Mahesh Fulsundar
 * @owner            : Mahesh Fulsundar
 * @version          : MAHESHFULSUNDAR 1.0 12-06-2018
 * @Notes            : All copyrights are reserved!
 * @rights           : All rights are reserved to the owner of the file creator.
  No one can do change to this file without permission of the owner.
 * @fileName         :
 * @dependancies     :
  ------------------------------------------------------------------------------------- */
include('cconfig.php');
include('header.php');
include('nav.php');
?>
<title>Customers List |<?php echo SITENAME;?></title>
<div class="page-inner">
  <div class="page-breadcrumb">
    <ol class="breadcrumb container">
      <li><a href="dashboard.php">Home</a></li>
      <li class="active">Customers List</li>
    </ol>
  </div>
  <div class="page-title">
    <div class="container">
      <h3>Customers List</h3>
    </div>
  </div>
  <?php if(isset($_GET['errmsg'])) { ?>
  <div class="alert alert-danger" role="alert"> <strong>Sorry !</strong> <a href="#" class="alert-link"><?php echo unserialize(base64_decode($_GET['errmsg']));?></a> </div>
  <?php } 
  if(isset($_GET['infomsg'])) { ?>
  <div class="alert alert-info" role="alert"> <strong>Note : </strong> <a href="#" class="alert-link"><?php echo unserialize(base64_decode($_GET['infomsg']));?></a> </div>
  <?php } 
  if(isset($_GET['sucmsg'])) {?>
  <div class="alert alert-success" role="alert"> <strong>Success !</strong> <a href="#" class="alert-link"><?php echo unserialize(base64_decode($_GET['sucmsg']));?></a> </div>
  <?php } ?>
  <div class="panel panel-white">
    <div class="panel-body">
      <div class="row m-b-lg table-responsive">
        <table id="example" class="display table" style="width: 100%;" role="grid" aria-describedby="example_info">
          <thead>
            <tr>
              <th>Sr</th>
              <th>Customer Name</th>
              <th>Mobile</th>
              <th>Email</th>
              <th>Gender</th>
              <th>City</th>
              <th>State</th>
              <th>Status</th>
              <th>Action</th>
            </tr>
          </thead>
          <tfoot>
            <tr>
              <th>Sr</th>
              <th>Customer Name</th>
              <th>Mobile</th>
              <th>Email</th>
              <th>Gender</th>
              <th>City</th>
              <th>State</th>
              <th>Status</th>
              <th>Action</th>
            </tr>
          </tfoot>
          <tbody>
            <?php $counter	=	1;
				$cust_details	=	mysqli_query($con,"SELECT * FROM customers");
				
				$custCount	=	mysqli_num_rows($cust_details);
				if($custCount >0)
				{ 
					while($custList = mysqli_fetch_assoc($cust_details))
					{ 
						$customer_id	=	$custList['customer_id'];
						$full_name		=	$custList['full_name'];
						$mobile_no		=	$custList['mobile_no'];
						$email_id		=	$custList['email_id'];
						$gender			=	$custList['gender'];
						$street_address	=	$custList['street_address'];
						$landmark		=	$custList['landmark'];
						$city_id		=	$custList['city_id'];
						$cityDetails	=	getCityDetails($con,$city_id);
						$cityName		=	$cityDetails['city_name'];

						$state_id		=	$custList['state_id'];
						$stateDetails	=	getstateDetails($con,$state_id);
						$stateName		=	$stateDetails['state'];
						
						$country_id		=	$custList['country_id'];
						$countryDetails	=	getCountry($con,$country_id);
						$countryName	=	$countryDetails['name'];
						$status			=	$custList['status'];
						
					?>
            <tr>
              <td><?php echo $counter++;?></td>
              <td><?php echo $full_name;?></td>
              <td><?php echo $mobile_no;?></td>
              <td><?php echo $email_id;?></td>
              <td><?php echo $gender; ?></td>
              <td><?php echo $cityName; ?></td>
              <td><?php echo $stateName; ?></td>
              <td><?php  if($status=='Active')
					{ ?>
                <a href="customers_activate.php?customerId=<?php echo base64_encode(serialize($customer_id));?>&stat=<?php echo base64_encode(serialize($status));?>" title="Active" class="btn btn-success"> <?php echo $status;?> </a>
                <?php
					}
					else
					{?>
                <a href="customers_activate.php?customerId=<?php echo base64_encode(serialize($customer_id));?>&stat=<?php echo base64_encode(serialize($status));?>" title="Inactive" class="btn btn-danger"> <?php echo $status;?> </a>
                <?php
					} ?></td>
              <td><a href="customer_view.php?customerId=<?php echo base64_encode(serialize($customer_id));?>" title="View"><span class="fa fa-eye"></span></a>&nbsp;  </td>
            </tr>
            <?php } } ?>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
<?php include("footer.php");?>