<?php
/* * ---------------------------------------------------------------------------------
 * @MAHESHFULSUNDAR
 * @author           : Mahesh Fulsundar
 * @owner            : Mahesh Fulsundar
 * @version          : MAHESHFULSUNDAR 1.0 12-06-2018
 * @Notes            : All copyrights are reserved!
 * @rights           : All rights are reserved to the owner of the file creator.
  No one can do change to this file without permission of the owner.
 * @fileName         :
 * @dependancies     :
  ------------------------------------------------------------------------------------- */
include('cconfig.php');
include('header.php');
include('nav.php');
#print_r($_SESSION);exit;
$user_id    = $_SESSION["UserId"];
$user_type  = $_SESSION["user_type"];
$query  = mysqli_query($con,"SELECT ifnull(sum(bill_amount),0) as success,count(order_id) as numb FROM orders WHERE order_status_id='6'");
if($user_type!=1 && $user_type!=3){
$query	=	mysqli_query($con," SELECT 
                                  COUNT(DISTINCT(o.order_id)) AS numb,
                                  IFNULL(SUM(o.bill_amount), 0) AS success
                              FROM
                                  orders o
                                  left join order_details od ON od.order_id=o.order_id
                                  left join products p on p.product_id=od.product_id
                              WHERE
                                  o.order_status_id = '6'
                                  and p.created_by=$user_id");
}
$arrsuccess	=	mysqli_fetch_assoc($query);

$query	=	mysqli_query($con,"SELECT ifnull(sum(bill_amount),0) as pending,count(order_id) as numb FROM orders WHERE order_status_id='1'");
if($user_type!=1 && $user_type!=3){
$query  = mysqli_query($con," SELECT 
                                  COUNT(DISTINCT(o.order_id)) AS numb,
                                  IFNULL(SUM(o.bill_amount), 0) AS pending
                              FROM
                                  orders o
                                  left join order_details od ON od.order_id=o.order_id
                                  left join products p on p.product_id=od.product_id
                              WHERE
                                  o.order_status_id = '1'
                                  and p.created_by=$user_id");
}
$arrpending	=	mysqli_fetch_assoc($query);

$query	=	mysqli_query($con,"SELECT ifnull(sum(bill_amount),0) as failed,count(order_id) as numb FROM orders WHERE order_status_id='4' ");
if($user_type!=1 && $user_type!=3){
$query  = mysqli_query($con," SELECT 
                                  COUNT(DISTINCT(o.order_id)) AS numb,
                                  IFNULL(SUM(o.bill_amount), 0) AS failed
                              FROM
                                  orders o
                                  left join order_details od ON od.order_id=o.order_id
                                  left join products p on p.product_id=od.product_id
                              WHERE
                                  o.order_status_id = '4'
                                  and p.created_by=$user_id");
}
$arrDispatched	=	mysqli_fetch_assoc($query);

$query	=	mysqli_query($con,"SELECT ifnull(sum(bill_amount),0) as total,count(order_id) as numb FROM orders ");
if($user_type!=1 && $user_type!=3){
$query  = mysqli_query($con," SELECT 
                                  COUNT(DISTINCT(o.order_id)) AS numb,
                                  IFNULL(SUM(o.bill_amount), 0) AS total
                              FROM
                                  orders o
                                  left join order_details od ON od.order_id=o.order_id
                                  left join products p on p.product_id=od.product_id
                              WHERE
                                  p.created_by=$user_id");
}

$arrtotal	=	mysqli_fetch_assoc($query);


?>
<!-- Navbar -->
<!-- Page Sidebar -->
<title>Dashboard |<?php echo SITENAME;?></title>
<link href="assets/plugins/vertical-timeline/css/style.css" rel="stylesheet" type="text/css">
<div class="page-inner">
<div class="page-breadcrumb">
  <ol class="breadcrumb container">
    <li><a href="dashboard.php">Home</a></li>
    <li class="active">Dashboard</li>
  </ol>
</div>
<div class="page-title">
  <div class="container">
    <h3>Dashboard</h3>
  </div>
</div>
<div id="main-wrapper" class="container">
  <div class="row">
          <div class="col_3">
          <div class="col-md-3 widget widget1">
            <div class="r3_counter_box" style="background:#b2d8b2;">
                    <i class="pull-left fa fa-check icon-rounded"></i>
                    <div class="stats">
                      <h5><strong>&#8377; <?php echo $arrsuccess['success'];?></strong></h5>
                      <span style="color:#008000;"> <strong><?php echo $arrsuccess['numb'];?></strong>  Delivered</span>
                    </div>
                </div>
          </div>
          <div class="col-md-3 widget widget1">
            <div class="r3_counter_box" style="background:#ffcccc;">
                    <i class="pull-left fa fa-spinner user1 icon-rounded"></i>
                    <div class="stats">
                      <h5><strong>&#8377; <?php echo $arrpending['pending'];?></strong></h5>
                      <span style="color:#FF0000;"><strong><?php echo $arrpending['numb'];?></strong> Pending</span>
                    </div>
                </div>
          </div>
          <div class="col-md-3 widget widget1">
            <div class="r3_counter_box" style="background:#ffffcc;">
                    <i class="pull-left fa fa-plane user2 icon-rounded"></i>
                    <div class="stats">
                      <h5><strong>&#8377; <?php echo $arrDispatched['failed'];?></strong></h5>
                      <span style="color:#FFA500;"><strong><?php echo $arrDispatched['numb'];?></strong> Dispatched</span>
                    </div>
                </div>
          </div>
          <div class="col-md-3 widget">
            <div class="r3_counter_box" style="background:#e5e5ff;">
                    <i class="pull-left fa fa-plus-square dollar1 icon-rounded"></i>
                    <div class="stats">
                      <h5><strong>&#8377; <?php echo $arrtotal['total'];?></strong></h5>
                      <span style="color:#000080;"><strong><?php echo $arrtotal['numb'];?></strong> Total</span>
                    </div>
                </div>
           </div>
          <div class="clearfix"> </div>
      </div>
    <div class="row">
      <div class="col-lg-12 col-md-12">
         <div class="panel "><!-- panel-white -->
          <div class="row">
            <div class="col-sm-6 padding-5">
              <div class="grid_1">
              <h3>Recent Orders Placed</h3>
              <canvas id="line" height="300" width="400" style="width: 400px; height: 300px;"></canvas>
              </div>
            </div>
            <div class="col-sm-6 margin-5">
              <h3>Top Selling product</h3>
              <canvas id="pie" height="300" width="400" style="width: 400px; height: 300px;"></canvas>
            </div>
          </div>
        </div>

      </div>
      <div class="col-lg-12 col-md-12">
        <div class="panel "><!-- panel-white -->
          <div class="row">
            <div class="col-sm-6 margin-5">
              <div class="stats-info panel-white">
                <div class="panel-heading">
                  <h4 class="panel-title">Recent Orders Placed</h4>
                </div>
                <div class="panel-body">
                  <ul class="list-unstyled">
                    <li>Invoice No
                      <div class="text-success pull-right">Amount<i class="fa fa-level-up"></i></div>
                    </li>
                    <?php 
					  $counter	=	1;
            $qry ="SELECT DISTINCT(invoice_no),o.order_id,bill_amount FROM orders o
                                               left join order_details od on od.order_id=o.order_id
                                                left join products p on p.product_id = od.product_id 
                                                 order by o.order_id DESC LIMIT 5";
            if($user_type!=1 && $user_type!=3){
              $qry ="SELECT DISTINCT(invoice_no),o.order_id,bill_amount FROM orders o
                     left join order_details od on od.order_id=o.order_id
                      left join products p on p.product_id = od.product_id 
                      where p.created_by=$user_id
                       order by o.order_id DESC LIMIT 5";
            }
					  $queryOrders	=	mysqli_query($con,$qry);
            $invoices ="";
            $amounts ="";
					  while($reslutOrders	=	mysqli_fetch_assoc($queryOrders))
					  {
  						$bill_amount=	$reslutOrders['bill_amount'];
  						$invoiceNo	=	$reslutOrders['invoice_no'];
              $invoices.="$invoiceNo,";
              $amounts.=round($bill_amount).',';

						?>
                    <li> <?php echo $invoiceNo;?>
                      <div class="text-success pull-right">&#8377; <?php echo $bill_amount ;?>/-<!--<i class="fa fa-level-up"></i>--></div>
                    </li>
            <?php
			       }

             $amounts=substr($amounts, 0,-1);
             $invoices=substr($invoices, 0,-1);


			     ?>
                  </ul>
                </div>
              </div>
            </div>
            <div class="col-sm-6 margin-5">
              <div class="stats-info panel-white">
                <div class="panel-heading">
                  <h4 class="panel-title">Top 5 Selling Products</h4>
                </div>
                <div class="panel-body">
                  <ul class="list-unstyled">
                    <li>Product Name
                      <div class="text-success pull-right">Sale<i class="fa fa-level-up"></i></div>
                    </li>
                    <?php 
					  $counts	=	1;
					  $custArr	= array();
					
            $qry = "  SELECT 
                          od.product_id,p.product_name,SUM(od.quantity) AS totalQuan,p.product_code
                      FROM
                          order_details od
                          LEFT JOIN products p ON p.product_id=od.product_id
                          
                      GROUP BY product_id
                      ORDER BY totalQuan DESC
                      LIMIT 5";
            if($user_type!=1 && $user_type!=3){
              $qry = "  SELECT 
                          od.product_id,p.product_name,SUM(od.quantity) AS totalQuan,p.product_code
                      FROM
                          order_details od
                          LEFT JOIN products p ON p.product_id=od.product_id
                      WHERE p.created_by=$user_id
                      GROUP BY product_id
                      ORDER BY totalQuan DESC
                      LIMIT 5";
            }
					$getOrder	= mysqli_query($con,$qry);
          $prod_count=0;
          $prod_name  ='';
          $prod_qty   ='';
					while($orderDetails	= mysqli_fetch_assoc($getOrder))
					{
						$product_id		=	$orderDetails['product_id'];
						$totalQuan		=	$orderDetails['totalQuan'];
						$product_name	=	$orderDetails['product_name'];
						$product_code	=	$orderDetails['product_code'];
            $prod_name.="'$product_name',";
            $prod_qty.="$totalQuan,";
            $prod_count++;
					 ?>
                    <li> <?php echo $product_name." (".$product_code.")";?>
                      <div class="text-success pull-right"><?php echo $totalQuan ;?><!--<i class="fa fa-level-up"></i>--></div>
                    </li>
                    <?php 
					}
          if($prod_count>0){
            $prod_name = substr($prod_name, 0,-1);
            $prod_qty = substr($prod_qty, 0,-1);;
          }
					?>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-12 col-md-12">
          <div class="panel panel-white">
            <div class="row">
              <div class="col-sm-12">
                <div class="stats-info">
                  <div class="panel-heading">
                    <h4 class="panel-title">Top 10 Customers</h4>
                  </div>
                  <div class="panel-body">
                    <div class="col-md-6">
                      <ul class="list-unstyled weather-info">
                        <li><b>CUSTOMER NAME</b> <span class="pull-right"><b>STATUS</b></span></li>
                      </ul>
                    </div>
                    <div class="col-md-6">
                      <ul class="list-unstyled weather-info">
                        <li><b>NO. ORDERS</b><span class="pull-right"><b>TOTAL</b></span></li>
                      </ul>
                    </div>
                    <?php 
					  $countt	=	1;
					  $custArr	= array();
					
					$getOrder	= mysqli_query($con," SELECT 
                                              count(*) totalOrder,o.cust_id, SUM(o.bill_amount) totalAmt,
                                              c.full_name,c.status
                                          FROM
                                              orders o
                                              LEFT JOIN customers c ON c.customer_id=o.cust_id
                                          GROUP BY o.cust_id
                                          ORDER BY totalAmt DESC
                                          LIMIT 10");
					while($custDetails	= mysqli_fetch_assoc($getOrder))
					{
						$custName		=	$custDetails['full_name'];
						$status			=	$custDetails['status'];
						$totalAmt		= $custDetails['totalAmt'];
						$totalOrderCnt	= $custDetails['totalOrder'];
						
						$productCount	= 0;
						?>
                    <div class="col-md-6">
                      <ul class="list-unstyled weather-info">
                        <li><?php echo $countt++;?>) <?php echo $custName;?> <span class="pull-right"><b><?php echo $status;?></b></span></li>
                      </ul>
                    </div>
                    <div class="col-md-6">
                      <ul class="list-unstyled weather-info">
                        <li><?php echo $totalOrderCnt;?> <span class="pull-right"><b><?php echo $totalAmt;?></b></span></li>
                      </ul>
                    </div>
                    <?php 
					}
					?>
                  </div>
                </div>
              </div>
            </div>
          </div>  
    </div>
  </div>

  <script src="assets/js/Chart.js"></script> 
  <script src="assets/plugins/vertical-timeline/js/main.js"></script> 
  <script src="assets/plugins/chartsjs/Chart.min.js"></script> 
  
  <!--  <script src="assets/js/pages/timeline.js"></script>-->
  <?php include('footer.php');?>
  <script>
      var inv = "<?php echo $invoices; ?>";
      var amt = "<?php echo $amounts; ?>";
      inv_arr=inv.split(",");
      amt_arr=amt.split(",");
      var lineChartData = {
      labels : inv_arr,
      datasets : [
        {
          fillColor : "#3b5998",
          strokeColor : "#3b5998",
          pointColor : "#3b5998",
          pointStrokeColor : "#fff",
          data : amt_arr
        }
      ]      
    };

    var prodCount ="<?php echo $prod_count; ?>";
    var prodNames ="<?php echo $prod_name; ?>";
    var prodQty ="<?php echo $prod_qty; ?>";
    names_arr=prodNames.split(",");
    qty_arr=prodQty.split(",");
    var colorX = ["#ef553a","#00aced","#69D2E7","#3b5998","#3b4448"];
    var finarr=[];
    for (var i = 0; i <prodCount; i++) {
      finarr.push({label : names_arr[i],value: qty_arr[i],color:colorX[i]});
    };
      var pieData = finarr;
    new Chart(document.getElementById("line").getContext("2d")).Line(lineChartData);
    new Chart(document.getElementById("pie").getContext("2d")).Pie(pieData);
  </script>
