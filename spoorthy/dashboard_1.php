<?php
/* * ---------------------------------------------------------------------------------
 * @MAHESHFULSUNDAR
 * @author           : Mahesh Fulsundar
 * @owner            : Mahesh Fulsundar
 * @version          : MAHESHFULSUNDAR 1.0 12-06-2018
 * @Notes            : All copyrights are reserved!
 * @rights           : All rights are reserved to the owner of the file creator.
  No one can do change to this file without permission of the owner.
 * @fileName         :
 * @dependancies     :
  ------------------------------------------------------------------------------------- */
include('cconfig.php');

// to sho active menus
$menu = array();
$menu['ds'] = 1;
include('header.php');
include('left_menu.php');

$query = mysqli_query($con,
        "SELECT sum(bill_amount) as success,count(order_id) as numb FROM orders WHERE order_status_id='6'");

$arrsuccess = mysqli_fetch_assoc($query);

$query = mysqli_query($con,
        "SELECT sum(bill_amount) as pending,count(order_id) as numb FROM orders WHERE order_status_id='1'");

$arrpending = mysqli_fetch_assoc($query);

$query = mysqli_query($con,
        "SELECT sum(bill_amount) as failed,count(order_id) as numb FROM orders WHERE order_status_id='7'");

$arrfail = mysqli_fetch_assoc($query);

$query = mysqli_query($con,
        "SELECT sum(bill_amount) as total,count(order_id) as numb FROM orders");
$arrtotal = mysqli_fetch_assoc($query);

// Get the data for PIE CHART
$date = date('Y-m-d');

$pquery = mysqli_query($con,
        "SELECT sum(bill_amount) as amount,status_name FROM orders o LEFT JOIN order_status os ON os.status_id=o.order_status_id WHERE DATE(create_date)='" . $date . "' group by order_status_id");

$revenue_query = mysqli_query($con,
        "SELECT sum(bill_amount) as amount,create_date FROM orders WHERE order_status_id='1' group by DATE(create_date) order by create_date DESC LIMIT 5");
$arrayLebel = array();
$arraySeries = array();
while ($arrrevenue = mysqli_fetch_assoc($revenue_query)) {
    $arrayLebel[] = date('d/m', strtotime($arrrevenue['create_date']));
    $arraySeries[] = round($arrrevenue['amount'], 0);
}
?>
<!-- Title-->
<title>Dashboard | <?php echo SITENAME; ?></title>
<!-- Nv chart css -->
<link rel="stylesheet" href="assets/plugins/charts/nv-chart/css/nv.d3.css" type="text/css" media="all">
<div class="content-wrapper">

    <!-- Container-fluid starts -->
    <!-- Main content starts -->
    <div class="container-fluid">
        <div class="row">
            <div class="main-header">
                <h4>Dashboard</h4>
            </div>
        </div>
        <!-- 4-blocks row start -->
        <div class="row m-b-30 dashboard-header">
            <div class="col-lg-3 col-sm-6">
                <form action='transactions_report.php' method='post'>
                    <input type='hidden' name='frmDate' value='01/01/2018'>
                    <input type='hidden' name='status' value='Success'>
                    <div class="bg-success dashboard-success">
                        <div class="sales-success">
                            <button type='submit' class="btn btn-sm btn-success" > <i class="icon-check"></i></button> 
                            <div class="f-right">
                                <h2 class="counters1" style="font-size: 16px;"><?php echo $arrsuccess['success']; ?></h2>
                                <span>Success</span>
                            </div>
                        </div>
                        <div class="bg-dark-success">
                            <p class="week-sales">TRANSACTIONS</p>
                            <p class="total-sales "><?php echo $arrsuccess['numb']; ?></p>
                        </div>
                    </div></form>
            </div>
            <div class="col-lg-3 col-sm-6">
                <form action='transactions_report.php' method='post'>
                    <input type='hidden' name='frmDate' value='01/01/2018'>
                    <input type='hidden' name='status' value='Pending'>
                    <div class="bg-success dasboard-success">
                        <div class="sales-success">
                            <button type='submit' class="btn btn-sm btn-success" ><i class="icon-close"></i></button> 
                            <div class="f-right">
                                <h2 class="counters1" style="font-size: 16px;"><?php echo $arrpending['pending']; ?></h2>
                                <span>Pending</span>
                            </div>
                        </div>
                        <div class="bg-dark-success">
                            <p class="week-sales">TRANSACTIONS</p>
                            <p class="total-sales"><?php echo $arrpending['numb']; ?></p>
                        </div>
                    </div></form>
            </div>

            <div class="col-lg-3 col-sm-6">
                <form action='transactions_report.php' method='post'>
                    <input type='hidden' name='frmDate' value='01/01/2018'>
                    <input type='hidden' name='status' value='Failed'>
                    <div class="bg-warning dasboard-warning">
                        <div class="sales-warning">
                            <button type='submit' class="btn btn-sm btn-warning" ><i class="icon-close"></i></button> 
                            <div class="f-right">
                                <h2 class="counters" style="font-size: 16px;"><?php echo $arrfail['failed']; ?></h2>
                                <span>Failed</span>
                            </div>
                        </div>
                        <div class="bg-dark-warning">
                            <p class="week-sales">TRANSACTIONS</p>
                            <p class="total-sales"><?php echo $arrfail['numb']; ?></p>
                        </div>
                    </div></form>
            </div>
            <div class="col-lg-3 col-sm-6">
                <div class="bg-facebook dashboard-facebook">
                    <div class="sales-facebook">
                        <button type='submit' class="btn btn-mini btn-facebook" ><i class="icon-clock"></i></button> 
                        <div class="f-right">
                            <h2 id="system-clock"></h2>

                        </div>
                    </div>
                    <div class="bg-dark-facebook">
                        <p class="week-sales">System Time</p>
                        <p class="total-sales"></p>
                    </div>
                </div>
            </div>

        </div>
        <!-- 4-blocks row end -->
        <!-- Row Starts -->
        <div class="row">  
            <!-- Regular Pie chart start -->
            <div class="col-lg-8 grid-item">
                <div class="card">
                    <div class="card-header">
                        <h5 class="card-header-text">Service Stats</h5>
                    </div>
                    <div class="card-block">
                        <div id="donutchart" class="nvd-chart w-100" ></div>
                    </div>
                </div>
            </div>
            <!-- Regular Pie chart end --> 
            <!-- Revenue chart start -->
            <div class="col-lg-4 grid-item">
                <div class="card">
                    <div class="card-block bg-primary">
                        <h5 class="text-center txt-white">Revenue</h5>
                        <div class="ct-chart1 ct-perfect-fourth"></div>
                    </div>
                    <div class="card-block">
                    <div class="row d-flex m-b-20 p-b-20 br-bottom">
                        <div class="col-sm-9">
                            <h6>Inquiries</h6>
                            <span class="barchart"><canvas width="265" height="50" style="display: inline-block; width: 265px; height: 50px; vertical-align: top; margin-bottom: -2px; margin-left: -2px;"></canvas></span>
                        </div>
                        <div class="col-sm-3">
                            <h1 class="d-inline-block txt-primary">+</h1>
                            <h1 class="counter txt-primary d-inline-block">142</h1>
                            <p class="text-uppercase">From last year</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-6">
                            <h6 class="f-w-300">Gross Revenue</h6>
                            <h2 class="counter txt-primary">9,362.74</h2>
                        </div>
                        <div class="col-xs-6">
                            <h6 class="f-w-300">Net Revenue</h6>
                            <h2 class="counter txt-primary">6,734.89</h2>
                        </div>
                    </div>
                </div>
                </div>
            </div>
            <!-- Revenue chart end --> 

            <!-- Donut Chart start -->
            
<!--            <div class="col-lg-4 grid-item">
                <div class="card">
                    <div class="card-block text-center">
                        <h1 class="f-52 m-b-20"> <i class="icon-wallet d-block"></i> <span class="counter-txt f-w-300 f-35"> INR <span class="counter  m-l-5"><?php echo $balance; ?></span>/- </span> </h1>
                        <h5 class="txt-primary f-w-600">My Wallet</h5>
                    </div>
                </div>
            </div>-->
            <!-- Donut Chart end --> 
        </div>
         
        
    <div class="row">
        <!-- Pie chart table start -->
        <div class="col-xl-8 col-lg-7 grid-item">
            <div class="card ">
                <div class="card-header">
                    <h5 class="card-header-text">Top Rating Customers</h5>
                    <span class="m-t-10 text-capitalize d-block">List of top ordering customers</span>
                </div>
                <div class="card-block">
                    <div class="table-responsive">
                        <table class="table m-b-0 photo-table">
                            <thead>
                                <tr class="text-uppercase">
                                    <th>Img</th>
                                    <th>CUSTOMER NAME</th>
                                    <th>STATUS</th>
                                    <th>NO. ORDERS</th>
                                    <th>TOTAL</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php 
					  $countt	=	1;
					  $custArr	= array();
					
					$getOrder	= mysqli_query($con,"SELECT cust_id,sum(bill_amount) as total FROM orders group by cust_id order by total DESC LIMIT 10");
					while($orderDetails	= mysqli_fetch_assoc($getOrder))
					{
						$custId		= $orderDetails['cust_id'];
						$custArr[]	= $custId;				
					} 
					
					foreach($custArr as $key => $cust_id)
					{
						$custDetails	=	Common::getCustomerDateils($cust_id,$con);
						$custName		=	$custDetails['full_name'];
						$email			=	$custDetails['email_id'];
						$status			=	$custDetails['status'];
						
						//get total number of orders and sum of amount paid
						$getTotalOrder	= mysqli_query($con,"SELECT sum(bill_amount) as totalAmt, count(*) as totalOrder FROM orders WHERE cust_id = '" . $cust_id . "'");
						$totalOrders	= mysqli_fetch_assoc($getTotalOrder);
						
						$totalAmt		= $totalOrders['totalAmt'];
						$totalOrderCnt	= $totalOrders['totalOrder'];
						
						$productCount	= 0;
						?>
                                <tr>
                                    <th>
                                        <img class="img-fluid img-circle" src="assets/images/avatar-2.png" alt="User">
                                    </th>
                                    <td><?php echo $custName;?> 
                                    </td>
                                    <td><?php echo $status;?> 
                                    </td>
                                    <td> <?php echo $totalOrderCnt;?> 
                                    </td> 
                                    <td><b><?php echo $totalAmt;?></b></td>
                                </tr>
                                <?php 
					}
					?>
                                  
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <!-- Pie chart table end -->
        <!-- Recent Items start -->
        <div class="col-xl-4 col-lg-5 grid-item">
            <div class="card">
                <div class="card-header">
                    <h5 class="card-header-text">Top Selling Products</h5>
                    <span class="m-t-10 text-capitalize d-block">Top selling products of the site in terms of quantity</span>
                </div>
                <div class="card-block">
                    <div class="table-responsive">
                        <table class="table m-b-0">
                            <thead>
                                <tr class="text-uppercase">
                                    <th class="p-t-0">Sr.</th>
                                    <th class="p-t-0">Product Name</th>
                                    <th class="p-t-0">Sale<i class="fa fa-level-up"></i></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php 
					  $counts	=	1;
					  $custArr	= array();
					
					$getOrder	= mysqli_query($con,"SELECT product_id,sum(quantity) as totalQuan FROM order_details group by product_id order by totalQuan DESC LIMIT 5");
					while($orderDetails	= mysqli_fetch_assoc($getOrder))
					{
						$product_id		=	$orderDetails['product_id'];
						$totalQuan		=	$orderDetails['totalQuan'];
						$productDetails	= Common::getProductById($product_id,$con);
						$product_name	=	$productDetails['product_name'];
						$product_code	=	$productDetails['product_code'];
					 ?>
                                <tr>
                                    <th><?php echo $counts++;?></th>
                                    <td><?php echo $product_name." (".$product_code.")";?></td>
                                    <td><a href="#"><?php echo $totalQuan ;?></a>
                                    </td>

                                </tr> 
                    <?php 
					}
					?> 
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
        </div>
        <!-- Recent Items end -->
    </div>    
        
        
        
        
        <!-- Container-fluid starts -->
        <div class="container-fluid"> 
            <!-- Main content starts -->
            <div class="col-xl-12 col-lg-12 default-grid-item ">
                <div class="card">
                    <div class="card-header">
                        <h5 class="card-header-text">Latest News </h5>
                    </div>
                    <div class="card-block p-t-0">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="main-timeline timeline-line crm-timeline p-0">
                                    <div class="cd-timeline cd-container m-b-0 p-b-0">
                                        <?php
                                        $qrNews = mysqli_query($con,
                                                "SELECT * FROM announcements WHERE Active=0 order by id DESC limit 5");

                                        if (mysqli_num_rows($qrNews) > 0) {
                                            while ($newsResult = mysqli_fetch_assoc($qrNews)) {
                                                ?>
                                                <div class="cd-timeline-block m-0">
                                                    <div class="cd-timeline-icon bg-primary"> <i class="icon-bubbles"></i> </div>
                                                    <!-- cd-timeline-img -->

                                                    <div class="cd-timeline-content card_main"> 
                                                      <!--<img src="assets/plugins/timeline/images/img1.jpg" class="img-fluid" alt=""/>-->
                                                        <div class="card-block">
                                                            <h6><?php echo $newsResult['subject_eng']; ?></h6>
                                                        </div>
                                                        <span class="cd-date"><?php echo date('M d Y H:i A',
                                                            strtotime($newsResult['updated_on'])); ?></span> <span class="cd-details">
                                                <?php if ($newsResult['image_path'] != '') { ?>
                                                                <img src="<?php echo $newsResult['image_path']; ?>" style="width: 100px; height: 100px;" alt="">
                                                <?php } echo $newsResult['announcement_eng']; ?>
                                                        </span> </div>
                                                    <!-- cd-timeline-content --> 
                                                </div>
                                                <!-- cd-timeline-block -->
    <?php }
}
?>
                                    </div>
                                    <!-- cd-timeline --> 
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Container-fluid ends -->  
    <!----->
</div>
<!-- Main content ends -->
<?php include("footer.php"); ?>
<script type="text/javascript" src="assets/pages/dashboard.js"></script>
<!-- NV chart -->
<script src="assets/plugins/charts/nv-chart/js/d3.js"></script>
<script src="assets/plugins/charts/nv-chart/js/nv.d3.js"></script>
<script src="assets/plugins/charts/nv-chart/js/stream_layers.js"></script>
<!-- Chartlist charts -->
<script type="text/javascript" src="assets/plugins/charts/chartlist/js/chartlist.js"></script>
<script type="text/javascript" src="assets/plugins/charts/chartlist/js/chartist-plugin-threshold.js"></script>
<script>
    new Chartist.Line('.ct-chart1', {
    labels: [<?php
foreach ($arrayLebel as $vl) {
    echo "'" . $vl . "',";
}
?>],
            series: [
            [<?php
foreach ($arraySeries as $val) {
    echo $val . ',';
}
?>]
            ]
    }, {
    showArea: false,
            axisY: {
            onlyInteger: true
            },
            plugins: [
                    Chartist.plugins.ctThreshold({
                    threshold: 5
                    })
            ]
    });
//Donut chart example
    nv.addGraph(function() {
    var chart = nv.models.pieChart()
            .x(function(d) { return d.label })
            .y(function(d) { return d.value })
            .showLabels(true)     //Display pie labels
            .labelThreshold(.05)  //Configure the minimum slice size for labels to show up
            .labelType("percent") //Configure what type of data to show in the label. Can be "key", "value" or "percent"
            .donut(true)          //Turn on Donut mode. Makes pie chart look tasty!
            .donutRatio(0.35)     //Configure how big you want the donut hole size to be.
            ;
    d3.select("#donutchart").append('svg')
            .datum(pieData())
            .transition().duration(350)
            .call(chart);
    nv.utils.windowResize(chart.update);
    return chart;
    });
    //Pie chart example data. Note how there is only a single array of key-value pairs.
    function pieData() {
    return  [
<?php
$color = array('#2196F3', '#ff5252', '#4CAF50', '#f57c00', '#FF0084', '#007BB6',
    '#3b5998', '#CB2027');
$i = 0;
if (mysqli_num_rows($pquery) > 0) {
    while ($arrpie = mysqli_fetch_assoc($pquery)) {
        ?>
            {
            "label": "<?php echo strtoupper($arrpie['status_name']); ?>",
                    "value" : <?php echo $arrpie['amount']; ?>,
                    "color" : "<?php echo $color[$i++]; ?>"
            },
    <?php
    }
} else {
    ?>
        {
        "label": "Pending",
                "value" : 1,
                "color" : "#2196F3"
        },
        {
        "label": "Confirmed",
                "value" : 1,
                "color" : "#ff5252"
        },
        {
        "label": "Dispatched",
                "value" : 1,
                "color" : "#4CAF50"
        },
        {
        "label": "Delivered",
                "value" : 1,
                "color" : "#f57c00"
        },
<?php } ?>
    ];
    }
</script>