<?php
/* * ---------------------------------------------------------------------------------
 * @MAHESHFULSUNDAR
 * @author           : Mahesh Fulsundar
 * @owner            : Mahesh Fulsundar
 * @version          : MAHESHFULSUNDAR 1.0 12-06-2018
 * @Notes            : All copyrights are reserved!
 * @rights           : All rights are reserved to the owner of the file creator.
  No one can do change to this file without permission of the owner.
 * @fileName         :
 * @dependancies     :
  ------------------------------------------------------------------------------------- */
include('cconfig.php');
include('header.php');
include('nav.php');
?>
<title>Deal of day |<?php echo SITENAME; ?></title>
<div class="page-inner">
    <div class="page-breadcrumb">
        <ol class="breadcrumb container">
            <li><a href="dashboard.php">Home</a></li>
            <li class="active">Deal of day</li>
        </ol>
    </div>
    <div class="page-title">
        <div class="container">
            <h3>Deal List</h3>
        </div>
    </div>
    <?php if (isset($_GET['errmsg'])) { ?>
        <div class="alert alert-danger" role="alert"> <strong>Sorry !</strong> <a href="#" class="alert-link"><?php echo unserialize(base64_decode($_GET['errmsg'])); ?></a> </div>
    <?php
    }
    if (isset($_GET['infomsg'])) {
        ?>
        <div class="alert alert-info" role="alert"> <strong>Note : </strong> <a href="#" class="alert-link"><?php echo unserialize(base64_decode($_GET['infomsg'])); ?></a> </div>
    <?php
    }
    if (isset($_GET['sucmsg'])) {
        ?>
        <div class="alert alert-success" role="alert"> <strong>Success !</strong> <a href="#" class="alert-link"><?php echo unserialize(base64_decode($_GET['sucmsg'])); ?></a> </div>
<?php } ?>
    <div class="panel panel-white">
        <div class="panel-heading clearfix" align="right"> 
            <a href="add_deal_of_day.php">
                <input  class="btn btn-sm btn-success" type="submit" name="submit" value="Configure Deal">
            </a>
        </div>
        <div class="panel-body">
            <div class="row m-b-lg table-responsive">
                <?php
                $sql = "";
                $sql .= "SELECT 
                            sc.subcategory_eng, dod.*
                        FROM
                            deal_of_day dod
                                LEFT JOIN
                            subcategory sc ON sc.subcat_id = dod.sub_category_id
                        WHERE
                            id>0";

                $result = mysqli_query($con, $sql);
                ?>
                <table id="example" class="display table" style="width: 100%;" role="grid" aria-describedby="example_info">
                    <thead>
                        <tr>
                            <th>Sr</th>
                            <th>Sub Category</th>
                            <th>Title</th>
                            <th>Marathi Title</th>
                            <th>Sub Title</th>
                            <th>Marathi Sub Title</th> 
                            <th>Start Date</th>
                            <th>End Date</th>
                            <th>Status</th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <<th>Sr</th>
                            <th>Sub Category</th>
                            <th>Title</th>
                            <th>Marathi Title</th>
                            <th>Sub Title</th>
                            <th>Marathi Sub Title</th> 
                            <th>Start Date</th>
                            <th>End Date</th>
                            <th>Status</th>
                        </tr>
                    </tfoot>
                    <tbody>
                        <?php
                        $i = 0;
                        while ($dealList = mysqli_fetch_assoc($result)) {

                            $i++;
                            $deal_id = $dealList['id'];
                            $sub_category_id = $dealList['sub_category_id'];
                            $sub_category_name = $dealList['subcategory_eng'];
                            $title = $dealList['title'];
                            $title_marathi = $dealList['title_marathi'];
                            $sub_title = $dealList['sub_title'];
                            $sub_title_marathi = $dealList['sub_title_marathi'];
                            $start_date = $dealList['start_date'];
                            $end_date = $dealList['end_date'];
                            $image = $dealList['image'];
                            $created_on = $dealList['created_on'];
                            $status = $dealList['status'];
                            ?>
                            <tr>
                                <td><?php echo $i; ?></td>
                                <td><?php echo $sub_category_name; ?></td> 
                                <td><?php echo $title; ?></td> 
                                <td><?php echo $title_marathi; ?></td> 
                                <td><?php echo $sub_title; ?></td> 
                                <td><?php echo $sub_title_marathi; ?></td> 
                                <td><?php echo $start_date; ?></td> 
                                <td><?php echo $end_date; ?></td> 
                                <td><?php if ($status == 1) {
                                    ?>
                                        <a onclick="active(<?php echo $deal_id; ?>, '0')" title="Active">
                                            <button type="button" class="btn btn-success" ><?php echo 'Active'; ?></button>
                                        </a>
                                        <?php
                                    } else {
                                        ?>
                                        <a onclick="active(<?php echo $deal_id; ?>, '1')" title="Inactive">
                                            <button type="button" class="btn btn-danger"><?php echo 'InActive'; ?></button>
                                        </a>
                            <?php }
                            ?></td>
                            </tr>
                            <?php
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<script>
    function active(id, status)
    {
        $.ajax({
            url: 'user_actions.php',
            data: 'status=' + status + '&id=' + id + '&action=dealStatusChange',
            type: "post",
            success: function (data) {
                if ('success' == data)
                {
                    window.location.href = 'deal_of_day_list.php?sucmsg=<?php echo base64_encode(serialize("Status updated successfully!")); ?>';
                } else
                {
                    window.location.href = 'deal_of_day_list.php?errmsg=<?php echo base64_encode(serialize("Error updating the status!")); ?>';
                }
            }
        });

    }
    function deleteid(id)
    {

        $.ajax({
            url: 'user_actions.php',
            data: 'id=' + id + '&action=pdelete',
            type: "post",
            success: function (data) {
                if ('success' == data)
                {
                    window.location.href = 'products_report.php?sucmsg=<?php echo base64_encode(serialize("Product deleted successfully!")); ?>';
                } else
                {
                    window.location.href = 'products_report.php?errmsg=<?php echo base64_encode(serialize("Error deleting the Product!")); ?>';
                }
            }
        });


    }
</script>
<?php include('footer.php'); ?>
