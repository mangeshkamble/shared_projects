<?php
/* * ---------------------------------------------------------------------------------
 * @MAHESHFULSUNDAR
 * @author           : Mahesh Fulsundar
 * @owner            : Mahesh Fulsundar
 * @version          : MAHESHFULSUNDAR 1.0 12-06-2018
 * @Notes            : All copyrights are reserved!
 * @rights           : All rights are reserved to the owner of the file creator.
  No one can do change to this file without permission of the owner.
 * @fileName         :
 * @dependancies     :
  ------------------------------------------------------------------------------------- */
 //----------------Get LANGUAGE DATEAILS---------------------------------------------------
   function getLanguages($con) {
        $query = mysqli_query($con, "SELECT * FROM multi_lang WHERE status = 'ACTIVE' LIMIT 2");
        $State = array(); 
        while ($result = mysqli_fetch_assoc($query)) {
            $State[] = $result;
        } 
        return $State;
    }
//===================Function to get parent Category Details=======================================//
function getparentCategory($con,$categoryID)
{
	$arr        =             array();
	$getCategory     =             mysqli_query($con,"SELECT * FROM rd_category WHERE category_id=".$categoryID);
	while($categoryList         =             mysqli_fetch_assoc($getCategory))
	{
		$arr['category_name']   = $categoryList['category_name'];
		$arr['category_description'] = $categoryList['category_description'];
		$arr['image']                                      = $categoryList['image'];
		$arr['parent_id']                               = $categoryList['parent_id'];
		$arr['category_id']                           = $categoryList['category_id'];
	}
	return $arr;
}
//================================================================================================//
function getSrUserDetails($con,$productID,$user_id,$authId)
{
	//auth_id 4 = dealer
	//get distributor id
	if($authId	== 4)
	{
		$cityDetails	=	getUserDetails($user_id,$con);
		$city			=	$cityDetails['city'];
		
		$checkDistributor	= mysqli_query($con,"SELECT DISTINCT userDetails.user_id FROM 
									rd_user_details as userDetails, rd_user_login as userLogin, rd_assign_products as userChain
									WHERE 
									userLogin.auth_id = 3 AND userDetails.city = '" . $city . "' AND 
									userLogin.user_id = userDetails.user_id AND userChain.product_id = " . $productID . " AND 
									userChain.assign_to = userDetails.user_id
								  ");
		  $distributorDetails	= mysqli_fetch_assoc($checkDistributor);
		  $distributorId		= $distributorDetails['user_id'];
		  $user_id				= $distributorId;
			
			//minus from stock
			//mysqli_query($con, "UPDATE rd_user_chain SET quantity = quantity - '" . $order_quantity . "' WHERE distributor_id = '" . $distributorId . "' AND product_id = '" . $productID . "'");
		
		if($user_id == '')
		{
			$details	=	mysqli_fetch_assoc(mysqli_query($con,"SELECT added_by from rd_product WHERE product_id=".$productID));
			$user_id	=	$details['added_by'];
			//$user_id	= 1;
		}
		return $user_id;
	}
	else
	{
		$details	=	mysqli_fetch_assoc(mysqli_query($con,"SELECT added_by from rd_product WHERE product_id=".$productID));
		$user_id	=	$details['added_by'];
		
		return $user_id;
	}
}

/*******************************************************************************************
Function to get product query to display all products
*******************************************************************************************/
function getProductQuery($con, $status, $authId, $loginId)
{
	$productQuery	= "SELECT * FROM rd_product WHERE status != '" . $status . "'";
 
	 if($authId != 1) //this for other logins than admin login
	 {
		 $productQuery .= " AND added_by = '" . $loginId . "'";
	 }
	
	 $productQuery	.= " ORDER BY product_id";
	 
	 return $productQuery;
}

function displayDate($date)
{
	$newDate	= date("d M, Y", strtotime($date));
	return $newDate;
}

function getChildDetails($catId, $con)
{
	$getChildDetails	= mysqli_query($con, "SELECT * FROM rd_category WHERE parent_id = '" . $catId . "'");
	$childDetails		= mysqli_fetch_assoc($getChildDetails);
	$childId			= $childDetails['category_id'];
	
	if($childId > 0)
	{
		array_push($_SESSION['child_array'], $childId);
		getChildDetails($childId, $con);
	}
	return $_SESSION['child_array'];
}

//TODO : 11 aug
function getParentList($catId, $con)
{
	if($catId > 0)
	{
		$getParentDetails	= mysqli_query($con, "SELECT * FROM rd_category WHERE category_id = '" . $catId . "'");
		$parentDetails		= mysqli_fetch_assoc($getParentDetails);
		$parentNameDB		= $parentDetails['category_name'];
		
		$parentParentId		= $parentDetails['parent_id'];
		
		if($_SESSION['parentName'] != '')
			$_SESSION['parentName'] .= " > ";
		
		$_SESSION['parentName'] .= $parentNameDB;
		
		getParentList($parentParentId, $con);	 
	}
	return $_SESSION['parentName'];
}
//function to set image path
function displayImage($imageName, $prefix, $zoom)
{
	if($zoom == 1)
		$defaultImage	= "no_image_500x500.jpg";
	else
		$defaultImage	= "no_image.jpg";
		
		
    $location		= $prefix . "upload";
	
	//check if image is in database or not
	//if not then pick default image
	if($imageName == '')
	  $productImage	= $defaultImage;
	else
	  $productImage	= $imageName;
	
	//set complete image path
	$productImagePath	= $location . "/" . $productImage;
	
	//check if image exists at location
	//if not set it to default path
	if(file_exists($productImagePath))
	  $productImagePath = $productImagePath;
	else
	  $productImagePath	= $location . "/" . $defaultImage;
	
	//return image path
	return $productImagePath;
}
function getAgentDetailsbyID($con,$agentID)
	{
		$query	=	mysqli_query($con,"SELECT comp_name FROM user WHERE id = '$agentID'");

		$result 	= 	mysqli_fetch_assoc($query);

		return $result;
	 }
	 
	 function getUserTypeName($con,$uid)
	{
		$qrvalues = mysqli_query($con,"SELECT type FROM user us INNER JOIN user_type ut ON ut.id=us.userType WHERE us.id = $uid");
		$val	  =	mysqli_fetch_assoc($qrvalues);
		return $val['type'];
	}
//function to send email 
function send_email($to, $subject, $message, $fromEmail)
{ 
	$mail = new PHPMailer(); // create a new object
	$mail->IsHTML(true);
	$mail->SetFrom($fromEmail);
	$mail->Subject = $subject;
	$mail->Body = $message;
	$mail->AddAddress($to);
	if(!$mail->Send())
	{
		$response = "Mailer Error: " . $mail->ErrorInfo;
	}
	else
	{
		$response = "Message has been sent";
	}
	
	return $response;
}

function send_sms($mobile,$message,$con)
{
	$numbers = urlencode($mobile);
	$username=urlencode('5432');
	$pass=urlencode('4332');
	$route=urlencode('343234');
	$senderid=urlencode('2343');
	$message=urlencode($message);
	$ch=curl_init();
	mysqli_query($con,"INSERT INTO rd_sms (mobile, message) VALUES ('$mobile', '$message')"); 
	
	/*curl_setopt($ch, CURLOPT_URL,"http://173.45.76.226:81/send.aspx?username=".$username."&pass=".$pass."&route=".$route."&senderid=".$senderid."&numbers=".$numbers."&message=".$message);
	curl_setopt($ch, CURLOPT_HEADER, 0);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_exec($ch);
	curl_close($ch); */
 
}

function genrate_ucode($length){
	$arrAlf = array("A","B","C","D","E","F","G",'H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z');
	$lF = rand(0,24);
	$lS = rand(0,24);
	
	$random = $arrAlf[$lF]."".$arrAlf[$lS].substr(number_format(time() * rand(), 0, '', ''), 0, $length);
	return $random;
}

function randstring($length)
	{
    	$chars =  'ABCDEFGHIJKLMNOPQRSTUVWXYZ'.'abcdefghijklmnopqrstuvwxyz'.
            '0123456789'.'"-=~!@#$%&*()_+,./\<>?;:[]{}';
        $str = '';
        $max = strlen($chars) - 1;
        for ($i=0; $i < $length; $i++)
        $str .= $chars[mt_rand(0, $max)];
        return $str;
     }

function get_random_no($len) {

	$random = substr(number_format(time() * rand(), 0, '', ''), 0, $len);

	return $random;
}

function getAuthorityDetails($authId, $con)
{
	$getAuthDetails	= mysqli_query($con, "SELECT * FROM rd_authority WHERE auth_id = '" . $authId . "'");
	$authDetails	= mysqli_fetch_assoc($getAuthDetails);
	
	return $authDetails;
}


function getCompanyMobile($con)
{
	$getAuthDetails	= mysqli_query($con, "SELECT mobile_no FROM rd_company_details");
	$authDetails	= mysqli_fetch_assoc($getAuthDetails);
	
	return $authDetails['mobile_no'];
}

//function to resize image and then upload
function resize_image_upload($txtName, $tableName, $uploadFolderName, $subFolderName, $conditionColumnName, $columnName, $condition, $con, $action)
{
	$extension = end(explode(".", $_FILES[$txtName]["name"]));
	$extension = strtolower($extension);
	
	//check for extension
	if (($extension != "jpg") && ($extension != "jpeg") && ($extension != "png") && ($extension != "gif")) 
	{
		$change='<div class="msgdiv">Unknown Image extension </div> ';
		$errors=1;
	} else {
		$size=filesize($_FILES[$txtName]['tmp_name']);
		if ($size > MAX_SIZE*1024)
		{
		  $change='<div class="msgdiv">You have exceeded the size limit!</div> ';
		  $errors=1;
		}
		//for jpeg image
		if($extension=="jpg" || $extension=="jpeg" )
		{
		  $tempFileName = $_FILES[$txtName]['tmp_name'];
		  $src = imagecreatefromjpeg($tempFileName);
		}
		//for png images
		else if($extension=="png")
		{
		  $tempFileName = $_FILES[$txtName]['tmp_name'];
		  $src = imagecreatefrompng($tempFileName);
		}
		//for gif images
		else 
		{
		  $src = imagecreatefromgif($tempFileName);
		}
	   
		list($width,$height)=getimagesize($tempFileName);
		
		//set width array here
		$widthArray = array('200');
		
		//resize image
		foreach($widthArray as $key => $newwidth)
		{
			$newheight = ($height/$width)*$newwidth;
			$tmp = imagecreatetruecolor($newwidth,$newheight);
			imagealphablending($tmp, false);
			imagesavealpha($tmp,true);
			$transparent = imagecolorallocatealpha($tmp, 255, 255, 255, 127);
			imagefilledrectangle($tmp, 0, 0, $newwidth, $newheight, $transparent);
			
			imagecopyresampled($tmp,$src,0,0,0,0,$newwidth,$newheight,$width,$height);
			
			//get file extension
			$ext = pathinfo($_FILES[$txtName]['name'], PATHINFO_EXTENSION);
			$oldFileName = pathinfo($_FILES[$txtName]['name'], PATHINFO_FILENAME);
			
			//split file name
			$pieces = explode('-', $oldFileName);
			$last_word = array_pop($pieces);
			
			$newFileName = $subFolderName . "/" . $subFolderName . "_" . get_random_no(6) . "-" . $newwidth . "x" . $newwidth . "." . $ext;
			$FileToUpload = $uploadFolderName . "/" . $newFileName;
			
			if($ext == "" || $ext == "")
			{
				imagejpeg($tmp,$FileToUpload,100);
			}
			else
			{
				imagepng($tmp,$FileToUpload);
			}
			
			if($action == 'insert')
			{
				mysqli_query($con, "INSERT INTO " . $tableName ." 
									(" . $conditionColumnName . ", " . $columnName . ") 
									VALUES 
									('" . $condition . "', '" . mysqli_real_escape_string($con, $newFileName) . "')
								   ");
				$lastInserId	= mysqli_insert_id($con);
			} else {
				//update in datbase
				$updateName	= "UPDATE " . $tableName . " SET 
								   " . $columnName . " = '" . mysqli_real_escape_string($con, $newFileName) . "' 
								   WHERE `" . $conditionColumnName . "` = '" . $condition . "'";
				
				mysqli_query($con, $updateName);
				$lastInserId = 0;
			}
		}
		
		
		imagedestroy($src);
		imagedestroy($tmp);
	}
	
	return $lastInserId;
}

//function to upload image
function upload_image($con,$txtName, $columnName, $tableName, $uploadFolderName, $subFolderName, $conditionColumnName,$condition)
{
	$allowedExts = array("gif", "jpeg", "jpg", "png");
	$extension = end(explode(".", $_FILES[$txtName]["name"]));
	if (($_FILES[$txtName]["size"] < 2000000) && in_array($extension, $allowedExts))
	{
		if ($_FILES[$txtName]["error"] > 0)
		{
			return $error=$_FILES[$txtName]["error"] ."<br>";
		}
		else
		{
			$filename = $subFolderName . "/" . $subFolderName . "_" . get_random_no(6) . "." . $extension;
			move_uploaded_file($_FILES[$txtName]["tmp_name"], $uploadFolderName . "/" . $filename);
		
			mysqli_query($con,"UPDATE " . $tableName . " SET 
							   " . $columnName . " = '" . mysql_real_escape_string($filename) . "' 
							   WHERE `" . $conditionColumnName . "` = '" . $condition . "'") 
						 or die(mysqli_error());
		}
	}
	else
	{
	    $errmsg = base64_encode(serialize("Error uploading image!."));
	 	header("Location:banners_report.php?errmsg=$errmsg");
		exit;
	}
	return $filename;
}

//function to display options on product hover
function displayOptions($product_id, $userID, $price)
{
	ob_start();
	?>
    <div class="wishlist2">
      <button type="button"  title="Add to Cart" onclick="cart_add(<?php echo $product_id;?>,<?php echo $userID;?>,<?php echo $price;?>, 1)"><span>Add to Cart</span></button>
    </div>
    <div class="compare2">
      <button type="button"  title="Add to Wishlist" onclick="addwislist(<?php echo $product_id;?>,<?php echo $userID;?>,<?php echo $price;?>)"><span>Add to Wishlist</span></button>
    </div>
    <!--<div class="compare2">
      <button type="button"  title="Add to Compare" onclick=""><span>Add to Compare</span></button>
    </div>-->
    <div class="compare2 product_detail"><a href="product_info.php?product_id=<?php echo base64_encode($product_id); ?>">Details</a> </div>
    <?php
	$options = ob_get_clean();
	
	return $options;
}


//get category details by category_id from rd_category
function getCategoryById($category_id,$con) {
    $qrCategoryDetail = mysqli_query($con,"SELECT * FROM rd_category WHERE category_id = " . $category_id);
    $getCategoryDetail = mysqli_fetch_assoc($qrCategoryDetail);
    return $getCategoryDetail;
}

//get product details by product_id from rd_product
function getProductById($con,$product_id) {
    $qrProductDetail 	= mysqli_query($con,"SELECT * FROM products WHERE product_id = " . $product_id);
    $getProductDetail 	= mysqli_fetch_assoc($qrProductDetail);
    return $getProductDetail;
}


//get product_to_category details
function getProductToCategoryById($product_cat_id,$con) {
    $qrProductToCategoryDetail = mysqli_query($con,"SELECT * FROM rd_product_to_category WHERE product_cat_id = " . $product_cat_id);
    $getProductToCategoryDetail = mysqli_fetch_assoc($qrProductToCategoryDetail);
    return  $getProductToCategoryDetail;
}


//get verification code
function get_verification_code($len) {
 	$verificaitionCode = substr(number_format(time() * rand(), 0, '', ''), 0, $len);
 	return $verificaitionCode;
}

//get manufacturer details
function getManufacturerDetails($manufacturer_id, $con)
{
	$getManufacturer		= mysqli_query($con, "SELECT * FROM rd_manufacturer WHERE manufacturer_id = '" . $manufacturer_id . "'");
	$manufacturerDetails	= mysqli_fetch_assoc($getManufacturer);
	
	return $manufacturerDetails;
}


//----------------------------Function to get productDateils form productId---------------------------//
function getProductDetails($product_id,$connection)		
{
	$getproductQuery	=	mysqli_query($connection,"SELECT * FROM rd_product WHERE product_id='".$product_id."'"); 
	while($product	=	mysqli_fetch_assoc($getproductQuery))
	{
		$products	=	$product;
	}
	return $products;
}
//--------------------------------------------------------------------------------------------------//

//------------------Function to get the productImages from the productID----------------------------//
			
function getProductImages($product_id,$connection)		
{
	$productImage	=	array();
	$getproductImgQuery	=	mysqli_query($connection,"SELECT * FROM rd_product_image WHERE product_id='".$product_id."'"); 
	while($productImg	=	mysqli_fetch_assoc($getproductImgQuery))
	{
		$productImage	=	$productImg;
	}
	return $productImage;
}
//--------------------------------------------------------------------------------------------------//

//--------------------------------------------------------------------------------------------------//
function getAllState($con)
{
	$getState	=	mysqli_query($con,"SELECT * FROM rd_state");
	while($stateList	=	mysqli_fetch_assoc($getState))
	{
		$state[]	=	$stateList;
	}
	return $state;
}
//--------------------------------------------------------------------------------------------------//

//--------------------------------------------------------------------------------------------------//	
function getCustomerDateils($con,$custId)
{
	$getcustomer	=	mysqli_query($con,"SELECT * FROM customers WHERE customer_id	=	".$custId);
	$customer		=	mysqli_fetch_assoc($getcustomer);
	
	return $customer;
}
//--------------------------------------------------------------------------------------------------//


//--------------Function to get all Country List-----------------//
function getAllCountry($con)
{
	$getcountry	=	mysqli_query($con,"SELECT * FROM rd_country"); 
	while($countries		=	mysqli_fetch_assoc($getcountry))
	{
		$country[]	=	$countries;
	}
	return $country;
}
//--------------------------------------------------------------------//


//--------------Function to get all City List-----------------//
function getAllCity($con)
{
	$getcity	=	mysqli_query($con,"SELECT * FROM rd_city"); 
	while($cities		=	mysqli_fetch_assoc($getcity))
	{
		$city[]	=	$cities;
	}
	return $city;
}
//--------------------------------------------------------------------//



//--------------Function to get the order details---------------------//
function getOrderDetails($con,$orderID)
{ 
	$getorderQuery	=	mysqli_query($con,"SELECT * FROM orders WHERE order_id='".$orderID."'"); 
	while($orders	=	mysqli_fetch_assoc($getorderQuery))
	{
		$orderdetails	=	$orders;
	}
	return $orderdetails;
}

//--------------------------------------------------------------------//

//--------------Function to get the order product details---------------------//
function getOrderProducts($con,$orderID)
{
	$getorderQuery	=	mysqli_query($con,"SELECT order_details.*,product.product_name,product.product_code, product.image1  FROM order_details order_details INNER JOIN products product ON order_details.product_id=product.product_id  WHERE order_id='".$orderID."'"); 
	while($orders	=	mysqli_fetch_assoc($getorderQuery))
	{
		$orderdetails[]	=	$orders;
	}
	return $orderdetails;
}
//--------------------------------------------------------------------//



//--------------Function to get the order product details---------------------//
function getOrderProductsNew($orderID,$userID)
{
  if($userID!=1)
  {
  	$getorderQuery = mysql_query("SELECT order_details.*,product.product_name,product.product_code, product.product_image  FROM rd_order_details order_details,rd_product product, rd_assign_products as assProduct WHERE order_details.product_id=product.product_id AND order_id='".$orderID."' AND assProduct.product_id=product.product_id AND (dealer_id='$userID' OR distributor_id = '$userID') group by product_id"); 
  }
  else
  {
	  $getorderQuery = mysql_query("SELECT order_details.*,product.product_name,product.product_code, product.product_image  FROM rd_order_details order_details,rd_product product, rd_assign_products as assProduct WHERE order_details.product_id=product.product_id AND order_id='".$orderID."' AND assProduct.product_id=product.product_id group by product_id"); 
  }
  while($orders = mysqli_fetch_assoc($getorderQuery))
  {
		$orderdetails[] = $orders;
  }
  return $orderdetails;
}
//--------------------------------------------------------------------//




//--------------Function to get the city Name from Id-----------------//
function getCityDetails($con,$city_id)
{
	$getcity	=	mysqli_query($con,"SELECT * FROM city WHERE city_id='".$city_id."'"); 
	$citys		=	mysqli_fetch_assoc($getcity);
	return $citys;
}

//--------------------------------------------------------------------//

//--------------Function to get the State Name from Id-----------------//
function getstateDetails($con,$state_id)
{
	$getstate	=	mysqli_query($con,"SELECT * FROM state WHERE state_id='".$state_id."'"); 
	$states		=	mysqli_fetch_assoc($getstate);
	return $states;
}

//--------------------------------------------------------------------//

//--------------Function to get the Status Name from Id-----------------//
function getStatus($con,$status_id)
{
	$getstatus	=	mysqli_query($con,"SELECT * FROM order_status WHERE status_id='".$status_id."'"); 
	$status		=	mysqli_fetch_assoc($getstatus);
	return $status['status_name'];
}

//--------------------------------------------------------------------//

//--------------Function to get the Country Name from Id-----------------//
function getCountry($con,$country_id)
{
	$getcountry	= mysqli_query($con,"SELECT * FROM country WHERE country_id='".$country_id."'"); 
	$countries	= mysqli_fetch_assoc($getcountry);
	return $countries;
}

//--------------------------------------------------------------------//

//--------------Function to get the order tracking history details---------------------//
/*function getTrackingHistory($orderID,$con)
{
	$getorderQuery	=	mysqli_query($con,"SELECT * FROM rd_order_tracking WHERE order_id='".$orderID."'"); 
	if(mysqli_num_rows($getorderQuery) > 0)
	{
		while($orders	=	mysqli_fetch_assoc($getorderQuery))
		{
			$orderdetails[]	=	$orders;
		}
		return $orderdetails;
	}
	else
	{
		return '';
	}
}*/
//--------------------------------------------------------------------//


//--------------Function to get the order tracking history details---------------------//
function getTrackingHistory($con,$orderID, $product_id)
{ 
	$getorderQuery = mysqli_query($con,"SELECT * FROM order_tracking WHERE order_id='".$orderID."'"); 
	if(mysqli_num_rows($getorderQuery) > 0)
	{
 		while($orders = mysqli_fetch_assoc($getorderQuery))
		{
			$orderdetails[] = $orders;
		}
		return $orderdetails;
	}
	else
	{
		return '';
	}
}
//--------------------------------------------------------------------//

//-------------------function to get the wishlist count--------------------//
function getWishlist($custID,$con)
{
                $getlist =             mysqli_query($con,"SELECT count(wish_id) as totalCount FROM rd_wishlist WHERE customer_id                =             ".$custID);
                $wishlist                               =             mysqli_fetch_assoc($getlist);
                
                return $wishes  =             $wishlist['totalCount'];
}

//--------------------------------------------------------------------//

//--------------------function to return the user details from userID--------//
function getUserDetails($con,$userID)
{
	$getlist = mysqli_query($con,"SELECT * FROM user WHERE id=".$userID);
	$users   = mysqli_fetch_assoc($getlist);
	
	return $users;
}

//--------------------------------------------------------------------//

//--------------Function to get all Status details---------------------//
function getAllStatus($con)
{
	$getstatusQuery	=	mysqli_query($con,"SELECT * FROM order_status"); 
	while($statusResult	=	mysqli_fetch_assoc($getstatusQuery))
	{
		$status[]	=	$statusResult;
	}
	return $status;
}
//--------------------------------------------------------------------//


//function to append zeros
function checkLength($number)
{
	$length = strlen($number);
	
	$newNumber = $number;
	
	for($i=$length;$i<6; $i++)
	{
		$newNumber	=	"0".$newNumber;
	}
	return $newNumber;
}

//=====================Function to return the Settings values==========================//
function getSettingsValues($title,$con)
{
	$getValue	=	mysqli_query($con,"SELECT value from rd_settings WHERE title='".$title."'");
	$myvalue	=	mysqli_fetch_assoc($getValue);
	
	return $myvalue['value'];
}

//=====================================================================================//

//=====================Function to return the banner Potiions==========================//
function getBannerPosition($con,$positionID)
{
	$getValue	=	mysqli_query($con,"SELECT * from banner_position WHERE position_id='".$positionID."'");
	$myvalue	=	mysqli_fetch_assoc($getValue);
	
	return $myvalue;
}

//=====================================================================================//


//--------------------function to return the user details from loginID--------//
function getUserDetailsByLoginID($con,$loginID)
{
	$getlist = mysqli_query($con,"SELECT * FROM user WHERE id=".$loginID);
	$users   = mysqli_fetch_assoc($getlist);
	
	return $users;
}

//--------------------------------------------------------------------//

//===================Function to get product Quantity from the order_details Table================//
function getProductCount($orderId,$con)
{
	$getQuantity	= mysqli_query($con,"SELECT sum(quantity) as total FROM rd_order_details WHERE order_id=".$orderId);
	while($quanList	= mysqli_fetch_assoc($getQuantity))
	{
		$total	= $quanList['total'];
	}
	return $total;     
}
//================================================================================================//

//--------------------------
function readNumber($num, $depth=0)
{
    $num = (int)$num;
    $retval ="";
    if ($num < 0) // if it's any other negative, just flip it and call again
        return "negative " + readNumber(-$num, 0);
    if ($num > 99) // 100 and above
    {
	  if ($num > 99999)
		$retval .= readNumber($num/100000, $depth+5);
		$num %= 100000;

	  if ($num > 999) // 1000 and higher
		$retval .= readNumber($num/1000, $depth+3);
		$num %= 1000; // now we just need the last three digits

	  if ($num > 99) // as long as the first digit is not zero
		$retval .= readNumber($num/100, 2)." hundred ";
		$retval .=readNumber($num%100, 1); // our last two digits                       
    }
    else // from 0 to 99
    {
        $mod = floor($num / 10);
        if ($mod == 0) // ones place
        {
            if ($num == 1) $retval.="One";
            else if ($num == 2) $retval.="Two";
            else if ($num == 3) $retval.="Three";
            else if ($num == 4) $retval.="Four";
            else if ($num == 5) $retval.="Five";
            else if ($num == 6) $retval.="Six";
            else if ($num == 7) $retval.="Seven";
            else if ($num == 8) $retval.="Eight";
            else if ($num == 9) $retval.="Nine";
        }
        else if ($mod == 1) // if there's a one in the ten's place
        {
            if ($num == 10) $retval.="ten";
            else if ($num == 11) $retval.="eleven";
            else if ($num == 12) $retval.="twelve";
            else if ($num == 13) $retval.="thirteen";
            else if ($num == 14) $retval.="fourteen";
            else if ($num == 15) $retval.="fifteen";
            else if ($num == 16) $retval.="sixteen";
            else if ($num == 17) $retval.="seventeen";
            else if ($num == 18) $retval.="eighteen";
            else if ($num == 19) $retval.="nineteen";
        }
        else // if there's a different number in the ten's place
        {
            if ($mod == 2) $retval.="twenty ";
            else if ($mod == 3) $retval.="thirty ";
            else if ($mod == 4) $retval.="forty ";
            else if ($mod == 5) $retval.="fifty ";
            else if ($mod == 6) $retval.="sixty ";
            else if ($mod == 7) $retval.="seventy ";
            else if ($mod == 8) $retval.="eighty ";
            else if ($mod == 9) $retval.="ninety ";
            if (($num % 10) != 0)
            {
                $retval = rtrim($retval); //get rid of space at end
                $retval .= "-";
            }
            $retval.=readNumber($num % 10, 0);
        }
    }

    if ($num != 0)
    {
        if ($depth == 5)
            $retval.=" lakh ";
        if ($depth == 3)
            $retval.=" thousand ";
        else if ($depth == 6)
            $retval.=" million ";
        if ($depth == 9)
            $retval.=" billion ";
    }
    return $retval;
}
//-----------------------------------------------------------------------------------------------------------//

//---------------------Get details from rd_distributor_dealer-----------------------------------------------//
function getDistributorsProduct($distributorId,$productId,$con)
{
	$getProducts	=	mysqli_query($con,"SELECT *,sum(quantity)as quantity FROM rd_assign_products WHERE assign_to=".$distributorId." AND product_id=".$productId);
	$productList	=	mysqli_fetch_assoc($getProducts);
	return $productList;	
}

//-----------------------------------------------------------------------------
function getCompanyDetails($compId , $con)
{
	$getCompanyDetails	= mysqli_query($con, "SELECT * FROM rd_company_details WHERE company_id = '" . $compId . "'");
	$companyDetails		= mysqli_fetch_assoc($getCompanyDetails);

	return $companyDetails;
}

function getInvoiceDetails($userId,$po_id,$productId,$con)
{
	$getProducts	=	mysqli_query($con,"SELECT sum(quantity) as quantity FROM rd_invoice_master rim INNER JOIN rd_invoice_details rid ON rid.invoice_id=rim.invoice_id WHERE rim.user_id=".$userId." AND rim.po_id=$po_id AND rid.product_id=$productId");
	while($productList	=	mysqli_fetch_assoc($getProducts))
	{
		$products	=	$productList;
	}
	return $products;		
}

function getPODetails($po_id,$productId,$con)
{
	$getProducts	=	mysqli_query($con,"SELECT * FROM rd_purchase_order rpo INNER JOIN rd_purchase_order_details rpod ON rpod.po_id=rpo.po_id WHERE rpo.po_id=$po_id AND rpod.product_id=$productId");
	while($productList	=	mysqli_fetch_assoc($getProducts))
	{
		$products	=	$productList;
	}
	return $products;		
}

function getUserDetailfromPo($poid,$con)
{
	$getUserIdFromPo	=   mysqli_query($con, "SELECT * FROM rd_purchase_order WHERE po_id = '".$poid."' ");
	$podetails 			=   mysqli_fetch_assoc($getUserIdFromPo);
	
	return $podetails;
}


function invoiceDetails($invoiceId,$con)
{
	$getInvoice		=   mysqli_query($con, "SELECT SUM(total_amount) as totalAmount FROM rd_invoice_details WHERE invoice_id = '".$invoiceId."' ");
	$invoicedetails =   mysqli_fetch_assoc($getInvoice);
	
	return $invoicedetails;
}

//-------------------Get total product available in stock for the resellers-------------------------//
function getTotalQuantity($user_id,$productId,$con)
{
	$settings	=	getSettingsValues('Expiry Days',$con);

	$settings	=	round($settings,0);
	
	$checkDate	=	date('Y-m-d',strtotime("+$settings days"));
	
	//------------Total Quantity of the Products from product table------------------------//
	$getProducts	=   mysqli_query($con, "SELECT IFNULL(sum(quantity),0) as quantity  FROM rd_assign_products WHERE product_id = '".$productId."' AND assign_to='$user_id' AND expiry_date >='".$checkDate."'");
	$Productdetails =   mysqli_fetch_assoc($getProducts);
	
	$productQuant	=	$Productdetails['quantity'];
	
	
	/*//-------------------Total quantity from the distributor table--------------------------//
	$getDist	=   mysqli_query($con, "SELECT sum(quantity) as quantity FROM rd_user_chain WHERE reseller_id='".$user_id."' AND product_id = '".$productId."' ");
	$Distdetails =   mysqli_fetch_assoc($getDist);
	
	$distQuant	=	$Distdetails['quantity'];
	
	//-------------------Total quantity from the dealers table--------------------------//
	$getdelar	=   mysqli_query($con, "SELECT sum(quantity) as quantity  FROM rd_distributor_dealer WHERE product_id = '".$productId."' ");
	$dealdetails =   mysqli_fetch_assoc($getdelar);
	
	$dealerQuant	=	$dealdetails['quantity'];
	
	return $productQuant - ($dealerQuant + $distQuant);*/
	
	return $productQuant;
}



//--------------------function to return the user details from userID--------//
function getDistUserDetails($userID,$con)
{
	$getlist = mysqli_query($con,"SELECT * FROM rd_user_details rud INNER JOIN rd_user_login rul ON rul.user_id=rud.user_id WHERE rud.user_id=".$userID." AND rul.auth_id=3");
	$users   = mysqli_fetch_assoc($getlist);
	
	return $users;
}

//--------------------------------------------------------------------//


//--------------------function to return the user details from userID--------//
function getallUserDetails($userID,$con)
{
	$getlist = mysqli_query($con,"SELECT *,userdtls.user_code as u_code FROM rd_user_details as userdtls, rd_user_login as login WHERE login.user_id=".$userID." AND userdtls.user_id = login.user_id");
	$users   = mysqli_fetch_assoc($getlist);
	
	return $users;
}

//--------------------------------------------------------------------//
//--------------------------------------------------------------------//
//id proof uploads
function uploadIDProof($cid=0,$flnm="",$flag=0,$con)
{
$allowedExts = array(".doc", ".docx", "pdf", "gif", "jpeg", "jpg", "png","vnd.openxmlformats-officedocument.wordprocessingml.document");
$extension = end(explode(".", $flnm["name"]));
if (($flnm["type"] == "application/pdf")
|| ($flnm["type"] == "image/gif")
|| ($flnm["type"] == "image/jpeg")
|| ($flnm["type"] == "image/jpg")
|| ($flnm["type"] == "application/msword")
|| ($flnm["type"] == "application/vnd.openxmlformats-officedocument.wordprocessingml.document")
|| ($flnm["type"] == "image/pjpeg")
|| ($flnm["type"] == "image/x-png")
|| ($flnm["type"] == "image/png")
&& ($flnm["size"] < 2000000)
&& in_array($extension, $allowedExts))
{
if ($flnm["error"] > 0)
{
     return $error=$flnm["error"] ."<br>";
}
else
{       
		//$recno = mysql_insert_id();
        $filename = time().$flnm["name"];
 		if($flag==1)
		{
 			$isUploaded = move_uploaded_file($flnm["tmp_name"],"upload/kyc/" . $filename);	
 		}
		else
		{
			$isUploaded = move_uploaded_file($flnm["tmp_name"],"../upload/kyc/" . $filename);	
		}
		if($isUploaded)
		{
 			/*if($cid>0)
			{
			   	unlink("uploads/".$flnm);
			} */
			return $filename;
		}
		else
		{
			echo $flnm["name"]; exit;	
			return $error=$flnm["error"] ."<br>";
		}
  		//$sql="update $table set $fieldname='$filename' where `id`='$id'";
		//mysql_query($sql) or die(mysql_error());
 		if($thumb==true)
		{
			thumb_nail($filename,$flnm["type"],100);
		}
 }
}
else
{
   return $error="";
}
}
//this is the function for the MLM to give income to the users uplines
function getUpline($user_id, $con, $level_id, $percentageArray, $levelCount, $from_user_id, $business, $qualified_mrp)
{
	 $getRefUserDetails	= mysqli_query($con, "SELECT * FROM rd_user_details WHERE user_id = '" . $user_id . "'");
	 $refUserDetails		= mysqli_fetch_assoc($getRefUserDetails);
	 $referral_id		= $refUserDetails['referal_id'];
	 $level_income		= $percentageArray[$level_id];
	 $self_business_mrp	= $refUserDetails['self_product_mrp'];  
 	 $net_amount			= $level_income;
 	 
	/*//get service tax precentage
	$serviceTaxTitle	= "Service Tax";
	$serviceTaxPer		= getSettingsValues($serviceTaxTitle, $con);

	//get user details
	$userDetails	= getUserDetails($referral_id, $con);
	$pan_card_no	= $userDetails['pan_card_no'];
	
	
	//get tds precentage
	if($pan_card_no != '')
		$tdsTitle	= "TDS";
	else
	$tdsTitle	= "TDS_NO_PAN";
	$TDSPer		= getSettingsValues($tdsTitle, $con);
	
 	//calculate net amount here
	$service_tax 	= ($net_amount * $serviceTaxPer) / 100;
	$tds 			= ($net_amount * $TDSPer) / 100;
	$net_amount		= $net_amount - $service_tax - $tds;  */
	
	//&& $self_business_mrp >= $qualified_mrp
	if($referral_id > 0 )
	{
 		mysqli_query($con, "INSERT INTO rd_level_income
							(from_user_id, to_user_id, business, level_id, amount, net_amount, entry_time )
							VALUES
							('" . $from_user_id . "', '" . $referral_id . "', '" . $business . "',
							 '" . $level_id . "', '" . $level_income . "', '" . $net_amount . "', 
							 '" . date("Y-m-d H:i:s") . "')
						   ");
	}
	
 	if($referral_id > 0 && $level_id < $levelCount)
	{
		$level_id	= $level_id + 1;
		getUpline($referral_id, $con, $level_id, $percentageArray, $levelCount, $from_user_id, $business, $qualified_mrp);
	}
 	
}
?>