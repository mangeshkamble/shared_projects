<!DOCTYPE html>
<html>
<head>
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<meta charset="UTF-8">
<meta name="description" content="Admin Dashboard Template" />
<meta name="keywords" content="admin,dashboard" />
<meta name="author" content="Steelcoders" />

<!-- Styles -->
<link href='http://fonts.googleapis.com/css?family=Ubuntu:300,400,500,700' rel='stylesheet' type='text/css'>
<link href="assets/plugins/pace-master/themes/blue/pace-theme-flash.css" rel="stylesheet"/>
<link href="assets/plugins/uniform/css/uniform.default.min.css" rel="stylesheet"/>
<link href="assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
<link href="assets/plugins/fontawesome/css/font-awesome.css" rel="stylesheet" type="text/css"/>
<link href="assets/plugins/line-icons/simple-line-icons.css" rel="stylesheet" type="text/css"/>
<link href="assets/plugins/waves/waves.min.css" rel="stylesheet" type="text/css"/>
<link href="assets/plugins/switchery/switchery.min.css" rel="stylesheet" type="text/css"/>
<link href="assets/plugins/3d-bold-navigation/css/style.css" rel="stylesheet" type="text/css"/>
<link href="assets/plugins/slidepushmenus/css/component.css" rel="stylesheet" type="text/css"/>
<link href="assets/plugins/bootstrap-datepicker/css/datepicker3.css" rel="stylesheet" type="text/css"/>
<link href="assets/plugins/datatables/css/jquery.datatables.min.css" rel="stylesheet" type="text/css"/>
<link href="assets/plugins/datatables/css/jquery.datatables_themeroller.css" rel="stylesheet" type="text/css"/>
<link href="assets/plugins/x-editable/bootstrap3-editable/css/bootstrap-editable.css" rel="stylesheet" type="text/css">

<!-- Custom CSS -->
<link href="css/style.css" rel='stylesheet' type='text/css' />

 
<!-- Theme Styles -->
<link href="assets/css/modern.min.css" rel="stylesheet" type="text/css"/>
<link href="assets/css/custom.css" rel="stylesheet" type="text/css"/>
<script src="assets/plugins/3d-bold-navigation/js/modernizr.js"></script>
<script src="assets/js/jquery-1.11.1.js"></script>
<link rel="apple-touch-icon" sizes="57x57" href="favicon/apple-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="favicon/apple-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="favicon/apple-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="favicon/apple-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="favicon/apple-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="favicon/apple-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="favicon/apple-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="favicon/apple-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="favicon/apple-icon-180x180.png">
<link rel="icon" type="image/png" sizes="192x192"  href="favicon/android-icon-192x192.png">
<link rel="icon" type="image/png" sizes="32x32" href="favicon/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="96x96" href="favicon/favicon-96x96.png">
<link rel="icon" type="image/png" sizes="16x16" href="favicon/favicon-16x16.png">
<link rel="manifest" href="favicon/manifest.json">
<!-- Favicon End HEre -->
<style>
.table tfoot{display:none !important;}
</style>
</head>
<body class="page-header-fixed compact-menu page-horizontal-bar">
<div class="overlay"></div>

<form class="search-form" action="#" method="GET">
  <div class="input-group">
    <input type="text" name="search" class="form-control search-input" placeholder="Search...">
    <span class="input-group-btn">
    <button class="btn btn-default close-search waves-effect waves-button waves-classic" type="button"><i class="fa fa-times"></i></button>
    </span> </div>
  <!-- Input Group -->
</form>
<!-- Search Form -->
<main class="page-content content-wrap">
<div class="navbar">
  <div class="navbar-inner container">
    <div class="sidebar-pusher"> <a href="javascript:void(0);" class="waves-effect waves-button waves-classic push-sidebar"> <i class="fa fa-bars"></i> </a> </div>
    <div class="logo-box"> <a href="dashboard.php" class="logo-text"><span style="color:#48ac3c"><?php echo SITENAME;?></span></a> </div>
    <!-- Logo Box -->
    <div class="search-button"> <a href="javascript:void(0);" class="waves-effect waves-button waves-classic show-search"><i class="fa fa-search"></i></a> </div>
    <div class="topmenu-outer">
      <div class="top-menu">
        <ul class="nav navbar-nav navbar-right">
          <li class="dropdown"> <a href="#" class="dropdown-toggle waves-effect waves-button waves-classic" data-toggle="dropdown"> <span class="user-name"><?php //echo $_SESSION['compName'];?><i class="fa fa-angle-down"></i></span> <img class="img-circle avatar" src="../images/brand7.png" width="50" height="50" alt=""> </a>
            <ul class="dropdown-menu dropdown-list" role="menu">
              <li role="presentation"><a href="profile.php"><i class="fa fa-user"></i>Profile</a></li>
               
              <li role="presentation"><a href="change_password.php"><i class="fa fa-lock"></i>Change Password</a></li>
              <li role="presentation" class="divider"></li>              
              <li role="presentation"><a href="logout.php"><i class="fa fa-sign-out m-r-xs"></i>Log out</a></li>
            </ul>
          </li>
         <!-- <li> <a href="javascript:void(0);" class="waves-effect waves-button waves-classic" id="showRight"> <i class="fa fa-comments"></i> </a> </li>-->
        </ul>
        <!-- Nav --> 
      </div>
      <!-- Top Menu --> 
    </div>
  </div>
</div> 