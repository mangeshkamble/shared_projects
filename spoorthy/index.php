<!DOCTYPE html>
<html>
 
<head>

<!-- Title -->
<title>Login - Sign in | Spoorthy Bazaar</title>
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<meta charset="UTF-8">
<meta name="description" content="Grocery Shop" />
<meta name="keywords" content="Grocery Shop" />
<meta name="author" content="Steelcoders" />

<!-- Styles -->
<link href='http://fonts.googleapis.com/css?family=Ubuntu:300,400,500,700' rel='stylesheet' type='text/css'>
<link href="assets/plugins/pace-master/themes/blue/pace-theme-flash.css" rel="stylesheet"/>
<link href="assets/plugins/uniform/css/uniform.default.min.css" rel="stylesheet"/>
<link href="assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
<link href="assets/plugins/fontawesome/css/font-awesome.css" rel="stylesheet" type="text/css"/>
<link href="assets/plugins/line-icons/simple-line-icons.css" rel="stylesheet" type="text/css"/>
<link href="assets/plugins/waves/waves.min.css" rel="stylesheet" type="text/css"/>
<link href="assets/plugins/switchery/switchery.min.css" rel="stylesheet" type="text/css"/>
<link href="assets/plugins/3d-bold-navigation/css/style.css" rel="stylesheet" type="text/css"/>

<!-- Theme Styles -->
<link href="assets/css/modern.min.css" rel="stylesheet" type="text/css"/>
<link href="assets/css/custom.css" rel="stylesheet" type="text/css"/>
<script src="assets/plugins/3d-bold-navigation/js/modernizr.js"></script>

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
<!-- Favicon start HEre -->
<link rel="apple-touch-icon" sizes="57x57" href="favicon/apple-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="favicon/apple-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="favicon/apple-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="favicon/apple-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="favicon/apple-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="favicon/apple-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="favicon/apple-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="favicon/apple-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="favicon/apple-icon-180x180.png">
<link rel="icon" type="image/png" sizes="192x192"  href="favicon/android-icon-192x192.png">
<link rel="icon" type="image/png" sizes="32x32" href="favicon/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="96x96" href="favicon/favicon-96x96.png">
<link rel="icon" type="image/png" sizes="16x16" href="favicon/favicon-16x16.png">
<link rel="manifest" href="favicon/manifest.json">
<!-- Favicon End HEre -->
</head>
<body class="page-login">
<main class="page-content">
  <div class="page-inner">
    <div id="main-wrapper">
      <div class="row"><br>
<br>
<br>

        <div class="col-md-3 center">
          <div class="login-box"> <a href="index.php" class="logo-name text-lg text-center">ADMIN PANEL</a>
            <!--<p class="text-center m-t-md">Please login into your account.</p>-->
            <?php if(isset($_GET['msg']))
			{ echo "<p class='text-center m-t-md' style='color:red'>".unserialize(base64_decode($_GET['msg']))."</p>";
			}
			if(isset($_GET['smsg']))
			{
				echo "<p class='text-center m-t-md' style='color:green'>".unserialize(base64_decode($_GET['smsg']))."</p>";
			}
			?>
            <form class="m-t-md" action="login_check.php" method="post">
              <div class="form-group">
                <input type="text" name="username" class="form-control" placeholder="UserName" required>
              </div>
              <div class="form-group">
                <input type="password" name="password" class="form-control" placeholder="Password" required>
              </div>
              <button type="submit" class="btn btn-success btn-block">Login</button>
             <!-- <a href="forgot.php" class="display-block text-center m-t-md text-sm">Forgot Password?</a>-->
              <!--<p class="text-center m-t-xs text-sm">Do not have an account?</p>
              <a href="register.php" class="btn btn-default btn-block m-t-md">Create an account</a>-->
            </form>
            <p class="text-center m-t-xs text-sm">2018 &copy; Spoorthybazaar.com</p>
             
          </div>
        </div>
      </div>
      <!-- Row --> 
    </div>
    <!-- Main Wrapper --> 
  </div>
  <!-- Page Inner --> 
</main>

<!-- Page Content --> 

<!-- Javascripts --> 
<script src="assets/plugins/jquery/jquery-2.1.4.min.js"></script> 
<script src="assets/plugins/jquery-ui/jquery-ui.min.js"></script> 
<script src="assets/plugins/pace-master/pace.min.js"></script> 
<script src="assets/plugins/jquery-blockui/jquery.blockui.js"></script> 
<script src="assets/plugins/bootstrap/js/bootstrap.min.js"></script> 
<script src="assets/plugins/jquery-slimscroll/jquery.slimscroll.min.js"></script> 
<script src="assets/plugins/switchery/switchery.min.js"></script> 
<script src="assets/plugins/uniform/jquery.uniform.min.js"></script> 
<script src="assets/plugins/classie/classie.js"></script> 
<script src="assets/plugins/waves/waves.min.js"></script> 
<script src="assets/js/modern.min.js"></script>
</body> 
</html>