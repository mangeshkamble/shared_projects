<?php
/* * ---------------------------------------------------------------------------------
 * @MAHESHFULSUNDAR
 * @author           : Mahesh Fulsundar
 * @owner            : Mahesh Fulsundar
 * @version          : MAHESHFULSUNDAR 1.0 12-06-2018
 * @Notes            : All copyrights are reserved!
 * @rights           : All rights are reserved to the owner of the file creator.
  No one can do change to this file without permission of the owner.
 * @fileName         :
 * @dependancies     :
  ------------------------------------------------------------------------------------- */
include('config.php');
$con =	connectDatabse();
session_start();
date_default_timezone_set('Asia/Kolkata');
 
$username		=	$_POST['username'];
$password 		= 	hash('sha256',$_POST['password']);
//Password decryption
$username = stripslashes($username); 
$loginQuery	=	mysqli_query($con,"SELECT * FROM user WHERE user_id='".$username."' AND password='".$password."' AND status='Active' AND deleteFlag='No'");
// mysqli_num_rows($loginQuery);

if(mysqli_num_rows($loginQuery)> 0)
{ 
	$logins	=	mysqli_fetch_assoc($loginQuery);  
	$_SESSION['userName']		=	$logins['fname'].' '.$logins['lname'];
	$_SESSION['email']		=	$logins['email'];
	$_SESSION['compName']	=	$logins['comp_name'];
	$_SESSION['UserId']		=	$logins['user_id'];
	$_SESSION['balance']	=	$logins['balance']; 
	$_SESSION['user_type']		=	$logins['userType'];
	$_SESSION['UserId']		=	$logins['id'];  
	$logiIP				=	$_SERVER['REMOTE_ADDR'];
	$_SESSION['login_ip']		=	$logiIP;
	$_SESSION['user_data']		=	$logins;
	
	mysqli_query($con,"UPDATE user SET online_status='Online',last_login='".date("Y-m-d H:i:s")."',login_ip='$logiIP' WHERE id=".$logins['id']);
	
	mysqli_query($con,"INSERT INTO login_history(user_id,ip_address) VALUES ('".$logins['id']."','$logiIP')");
        
	if($logins['userType']==1)
	{
            // Admin Login
            header("location:dashboard.php");
		exit;
	}elseif($logins['userType']==2){
            // Vendor Login    
            header("location:dashboard.php");
            exit;
        }elseif($logins['userType']==3){
            // Franchisee Login    
            header("location:dashboard.php");
            exit;
        }elseif($logins['userType']==4){
            // Manager Login    
            header("location:dashboard.php");
            exit;
        }elseif($logins['userType']==5){
            // Employee Login    
            header("location:dashboard.php");
            exit;
        }
	else
	{
		$msg	=	base64_encode(serialize("Sorry! Invalid Username / Password!"));
		header("location:index.php?msg=$msg");
		exit;
	}
	
}
else
{
	$msg	=	base64_encode(serialize("Invalid Username / Password "));
	header("location:index.php?msg=$msg");
	exit;
}


?>