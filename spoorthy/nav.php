<div class="page-sidebar sidebar horizontal-bar">
    <div class="page-sidebar-inner">
        <ul class="menu accordion-menu">
            <li class="nav-heading"><span>Navigation</span></li>
            <li><a href="dashboard.php"><span class="menu-icon icon-speedometer"></span>
                    <p>Dashboard</p>
                </a></li>
            <?php
            if ($_SESSION["user_type"] ==1) {
                ?>
                <li class="droplink"><a href="#" title="Category"><span class="menu-icon icon-briefcase"></span>
                        <p>Category</p>
                        <span class="arrow"></span></a>
                    <ul class="sub-menu">        
                        <li><a href="category.php">Category</a></li>
                        <li><a href="subcategory.php">Sub Category</a></li> 

                    </ul>
                </li>
                <?php
            }
            ?>

            <?php
            if ($_SESSION["user_type"] != 3 && $_SESSION["user_type"] != 5) {
                ?>
                <li class="droplink"><a href="#" title="Products"><span class="menu-icon icon-basket"></span>
                        <p>Products</p>
                        <span class="arrow"></span></a>
                    <ul class="sub-menu">        
                        <li><a href="products_report.php">Products</a></li>
                        <li><a href="reviews_report.php">Product Reviews</a></li>
                    </ul>
                </li>
                <?php
            }
            ?>

            <?php
            if ($_SESSION["user_type"] != 2) {
                ?>
                <li class="droplink"><a href="#" title="Sales"><span class="menu-icon icon-basket-loaded"></span>
                        <p>Sales</p>
                        <span class="arrow"></span></a>
                    <ul class="sub-menu">
                        <?php if ($_SESSION["user_type"] == 3) { ?>
                        <li><a href="create_sales_order.php">Sales Order</a></li>
                        <?php } ?>
                        <li><a href="orders_report.php">Orders</a></li>
                        <li><a href="customers_report.php">Customers</a></li>
                    </ul>
                </li>
                <?php
            }
            ?>

            <?php
            if ($_SESSION["user_type"] == 1 || $_SESSION["user_type"] == 4 ) {
                ?>
                <li class="droplink"><a href="#" title="System"><span class="menu-icon icon-magic-wand"></span>
                        <p>System</p>
                        <span class="arrow"></span></a>
                    <ul class="sub-menu">
                        <li><a href="banners_report.php">Ad Banners</a></li>
                        <?php if ($_SESSION["user_type"] == 1){ ?>
                        <li><a href="deal_of_day_list.php">Deal of Day</a></li>
                        <?php } ?>
                        <li><a href="coupon_reports.php">Discount Coupons</a>
                        <li><a href="news.php">News</a>
                    </ul>
                </li>
                <?php
            }
            ?>

            <?php
            if ($_SESSION["user_type"] == 1 || $_SESSION["user_type"] == 4) {
                ?>
                <li class="droplink"><a href="#" title="System"><span class="menu-icon icon-users"></span>
                        <p>Users</p>
                        <span class="arrow"></span></a>
                    <ul class="sub-menu">
                        <li><a href="user_report.php">Users Report</a></li>
                    </ul>
                </li>
                <?php
            }
            ?>

            <?php
            if ($_SESSION["user_type"] != 2 && $_SESSION["user_type"] != 5) {
                ?> 
            <li class="droplink"><a href="#" title="Billing"><span class="menu-icon icon-docs"></span>
                    <p>Billing</p>
                    <span class="arrow"></span></a>
                <ul class="sub-menu">
                    <li><a href="orders_report.php">Generate Bill</a></li>
                </ul>
            </li>
            <?php
            }
            ?>
            <li class="droplink"><a href="#" title="Reports"><span class="menu-icon icon-briefcase"></span>
                    <p>Reports</p>
                    <span class="arrow"></span></a>
                <ul class="sub-menu">
                    <li><a href="orders_report.php">Sales Report</a></li>
                    <li><a href="product_viewed.php">Product Viewed</a></li>
                    <li><a href="product_purchased.php">Product Purchased</a></li>
                    <li><a href="product_prices.php">Product Prices</a></li>
                    <li><a href="customer_class_report.php">Customer Report</a></li>
                </ul>
            </li>
            <li class="droplink"><a href="#" title="Profile"><span class="menu-icon icon-user"></span>
                    <p>Profile</p>
                    <span class="arrow"></span></a>
                <ul class="sub-menu">
                    <li><a href="profile.php">Profile</a></li>
                    <li><a href="change_password.php">Change Password</a></li>
                    <li><a href="logout.php">Logout</a></li>
                </ul>
            </li>
       <!--    <li class="nav-heading"><span>Features</span></li>
            <li class="droplink"><a href="#"><span class="menu-icon icon-briefcase"></span>
              <p>UI Kits</p>
              <span class="arrow"></span></a>
              <ul class="sub-menu">
                <li><a href="ui-alerts.html">Alerts</a></li>
                <li><a href="ui-buttons.html">Buttons</a></li>
                <li><a href="ui-icons.html">Icons</a></li>
                <li><a href="ui-typography.html">Typography</a></li>
                <li><a href="ui-notifications.html">Notifications</a></li>
                <li><a href="ui-grid.html">Grid</a></li>
                <li><a href="ui-tabs-accordions.html">Tabs &amp; Accordions</a></li>
                <li><a href="ui-modals.html">Modals</a></li>
                <li><a href="ui-panels.html">Panels</a></li>
                <li><a href="ui-progress.html">Progress Bars</a></li>
                <li><a href="ui-sliders.html">Sliders</a></li>
                <li><a href="ui-nestable.html">Nestable</a></li>
                <li><a href="ui-tree-view.html">Tree View</a></li>
              </ul>
            </li>
             <li class="droplink"><a href="#"><span class="menu-icon icon-layers"></span>
              <p>Layouts</p>
              <span class="arrow"></span></a>
              <ul class="sub-menu">
                <li><a href="layout-blank.html">Blank Page</a></li>
                <li><a href="layout-fixed-sidebar.html">Fixed Menu</a></li>
                <li><a href="layout-static-header.html">Static Header</a></li>
                <li><a href="layout-collapsed-sidebar.html">Collapsed Sidebar</a></li>
                <li><a href="layout-large-menu.html">Large Menu</a></li>
              </ul>
            </li>
            <li class="droplink"><a href="#"><span class="menu-icon icon-grid"></span>
              <p>Tables</p>
              <span class="arrow"></span></a>
              <ul class="sub-menu">
                <li><a href="table-static.html">Static Tables</a></li>
                <li><a href="table-responsive.html">Responsive Tables</a></li>
                <li><a href="table-data.html">Data Tables</a></li>
              </ul>
            </li>
            <li class="droplink active open"><a href="#"><span class="menu-icon icon-note"></span>
              <p>Forms</p>
              <span class="arrow"></span></a>
              <ul class="sub-menu">
                <li><a href="form-elements.html">Form Elements</a></li>
                <li class="active"><a href="form-wizard.html">Form Wizard</a></li>
                <li><a href="form-upload.html">File Upload</a></li>
                <li><a href="form-image-crop.html">Image Crop</a></li>
                <li><a href="form-image-zoom.html">Image Zoom</a></li>
                <li><a href="form-select2.html">Select2</a></li>
                <li><a href="form-x-editable.html">X-editable</a></li>
              </ul>
            </li>
            <li class="droplink"><a href="#"><span class="menu-icon icon-bar-chart"></span>
              <p>Charts</p>
              <span class="arrow"></span></a>
              <ul class="sub-menu">
                <li><a href="charts-sparkline.html">Sparkline</a></li>
                <li><a href="charts-rickshaw.html">Rickshaw</a></li>
                <li><a href="charts-morris.html">Morris</a></li>
                <li><a href="charts-flotchart.html">Flotchart</a></li>
                <li><a href="charts-chartjs.html">Chart.js</a></li>
              </ul>
            </li>
            <li class="droplink"><a href="#"><span class="menu-icon icon-user"></span>
              <p>Login</p>
              <span class="arrow"></span></a>
              <ul class="sub-menu">
                <li><a href="login.html">Login Form</a></li>
                <li><a href="login-alt.html">Login Alt</a></li>
                <li><a href="register.html">Register Form</a></li>
                <li><a href="register-alt.html">Register Alt</a></li>
                <li><a href="forgot.html">Forgot Password</a></li>
                <li><a href="lock-screen.html">Lock Screen</a></li>
              </ul>
            </li>
            <li class="droplink"><a href="#"><span class="menu-icon icon-pointer"></span>
              <p>Maps</p>
              <span class="arrow"></span></a>
              <ul class="sub-menu">
                <li><a href="maps-google.html">Google Maps</a></li>
                <li><a href="maps-vector.html">Vector Maps</a></li>
              </ul>
            </li>-->
            <!--<li class="droplink"><a href="#"><span class="menu-icon icon-present"></span>
              <p>Extra</p>
              <span class="arrow"></span></a>
              <ul class="sub-menu">
                <li><a href="404.html">404 Page</a></li>
                <li><a href="500.html">500 Page</a></li>
                <li><a href="invoice.html">Invoice</a></li>
                <li><a href="calendar.html">Calendar</a></li>
                <li><a href="faq.html">FAQ</a></li>
                <li><a href="todo.html">Todo</a></li>
                <li><a href="pricing-tables.html">Pricing Tables</a></li>
                <li><a href="shop.html">Shop</a></li>
                <li><a href="gallery.html">Gallery</a></li>
                <li><a href="timeline.html">Timeline</a></li>
                <li><a href="search.html">Search Results</a></li>
                <li><a href="coming-soon.html">Coming Soon</a></li>
                <li><a href="contact.html">Contact</a></li>
              </ul>
            </li>-->
            <!--<li class="droplink"><a href="#"><span class="menu-icon icon-folder"></span>
              <p>Levels</p>
              <span class="arrow"></span></a>
              <ul class="sub-menu">
                <li class="droplink"><a href="#">
                  <p>Level 1.1</p>
                  <span class="arrow"></span></a>
                  <ul class="sub-menu">
                    <li class="droplink"><a href="#">
                      <p>Level 2.1</p>
                      <span class="arrow"></span></a>
                      <ul class="sub-menu">
                        <li><a href="#">Level 3.1</a></li>
                      </ul>
                    </li>
                    <li><a href="#">Level 2.2</a></li>
                  </ul>
                </li>
                <li><a href="#">Level 1.2</a></li>
              </ul>
            </li>-->
        </ul>
    </div>
    <!-- Page Sidebar Inner --> 
</div>
