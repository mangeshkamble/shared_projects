<?php 

/* * ---------------------------------------------------------------------------------
 * @MAHESHFULSUNDAR
 * @author           : Mahesh Fulsundar
 * @owner            : Mahesh Fulsundar
 * @version          : MAHESHFULSUNDAR 1.0 12-06-2018
 * @Notes            : All copyrights are reserved!
 * @rights           : All rights are reserved to the owner of the file creator.
  No one can do change to this file without permission of the owner.
 * @fileName         :
 * @dependancies     :
  ------------------------------------------------------------------------------------- */
include('cconfig.php');

// to sho active menus
$menu = array();
$menu['ns'] = 1;  
include('header.php');
include('nav.php'); 
  
?>
<title>News | <?php echo SITENAME;?></title>
<!-- Sidebar chat end-->
<div class="content-wrapper"> 
  <!-- Container-fluid starts -->
  <div class="container-fluid"> 
    
    <!-- Header Starts -->
    <div class="row">
      <div class="col-sm-12 p-0">
        <div class="main-header">
          <h4>News</h4>
          <ol class="breadcrumb breadcrumb-title breadcrumb-arrow">
            <li class="breadcrumb-item"> <a href="dashboard.php"> <i class="icofont icofont-home"></i> </a> </li>
            <li class="breadcrumb-item"><a href="#">News</a> </li>
          </ol>
        </div>
      </div>
    </div>
    <!-- Header end --> 
    
    <!-- Tables start --> 
    <!-- Row start -->
    <div class="row">
      <div class="col-sm-12">
        <?php if(isset($_GET['errmsg'])) { ?>
        <div class="card-block button-list notifications"><a href="#!" class="btn btn-danger waves-effect" data-type="danger"><strong>Sorry! </strong> <?php echo unserialize(base64_decode($_GET['errmsg']));?> </a></div>
        <?php } 
  if(isset($_GET['infomsg'])) { ?>
        <div class="card-block button-list notifications"><a href="#!" class="btn btn-info waves-effect" data-type="info"><strong>Note : </strong><?php echo unserialize(base64_decode($_GET['infomsg']));?></a></div>
        <?php } 
  if(isset($_GET['sucmsg'])) {?>
        <div class="card-block button-list notifications"> <a href="#!" class="btn btn-success waves-effect" data-type="success"><strong>Success! </strong><?php echo unserialize(base64_decode($_GET['sucmsg']));?></a></div>
        <?php } ?>
        <!-- Hover effect table starts -->
        <div class="card">
          <div class="card-header">
            <h5 class="card-header-text"><strong>NEWS</strong> Management</h5>
            <a href="news_add.php" class="btn btn-primary waves-effect waves-light f-right md-trigger" data-modal="modal-13"> <i class="icofont icofont-plus"></i> Add News </a> </div>
          <div class="card-block">
            <?php		
		 $getusers	= "SELECT * FROM announcements where active= '0' order by id DESC";
		
		$resultUsers	= mysqli_query($con,$getusers);
		
		if(mysqli_num_rows($resultUsers) > 0)
		{    ?>
            <div class="row">
              <div class="col-sm-12 table-responsive">
                <table class="table table-hover table-striped table-bordered" id="advanced-table">
                  <thead>
                    <tr>
                      <th>Sr</th>
                      <th>Subject English</th>
                      <th>Subject Marathi</th>
                      <th>Description English</th>
                      <th>Description Marathi</th>
                      <th>Uploads</th>
                      <th>Status</th>
                      <th>Date</th>
                      <th>Action</th>
                  </thead>
                  <tfoot>
                    <tr>
                      <th>Sr</th>
                      <th>Subject English</th>
                      <th>Subject Marathi</th>
                      <th>Description English</th>
                      <th>Description Marathi</th>
                      <th>Uploads</th>
                      <th>Status</th>
                      <th>Date</th>
                      <th>Action</th>
                  </tfoot>
                  <tbody>
                    <?php
		  $i = 0;
		  
          while($userList	= mysqli_fetch_assoc($resultUsers))
          {
			  $i++;
		  ?>
                    <tr>
                      <td><?php echo $i;?></td>
                      <td><?php echo $userList['subject_eng'];?></td>
                      <td><?php echo $userList['subject_mar'];?></td>
                      <td><?php echo substr($userList['announcement_eng'],0,50);?></td>
                      <td><?php echo substr($userList['announcement_mar'],0,50);?></td>
                      <th> <?php if($userList['image_path']=='') { echo 'NA'; } else { ?>
                        <a href="<?php echo $userList['image_path'];?>" title="Receipt" target="_blank">
                        <div class="col-md-3" style="border:#999"> <img src="<?php echo $userList['image_path'];?>" alt="No Image" height="40" width="40" border="2"> </div>
                        </a>
                        <?php } ?></th>
                      <td><?php if($userList['active']==0) { echo "Active"; } else { echo "Inactive"; } ?></td>
                      <td><?php echo date('d-m-Y h:i A',strtotime($userList['updated_on']));?></td>
                      <td><a href="news_edit.php?id=<?php echo $userList['id'];?>" title="Edit"><span class="icofont icofont-ui-edit"></span></a></td>
                    </tr>
                    <?php }?>
                  </tbody>
                </table>
                <?php }
	  else
	  { echo "No records Found!";}
	  
	   
	  ?>
              </div>
            </div>
          </div>
        </div>
        <!-- Hover effect table ends --> 
        
      </div>
    </div>
    <!-- Row end --> 
    <!-- Tables end --> 
  </div>
  
  <!-- Container-fluid ends --> 
</div>
</div>
<?php include("footer.php");?>
