<?php 

/* * ---------------------------------------------------------------------------------
 * @MAHESHFULSUNDAR
 * @author           : Mahesh Fulsundar
 * @owner            : Mahesh Fulsundar
 * @version          : MAHESHFULSUNDAR 1.0 12-06-2018
 * @Notes            : All copyrights are reserved!
 * @rights           : All rights are reserved to the owner of the file creator.
  No one can do change to this file without permission of the owner.
 * @fileName         :
 * @dependancies     :
  ------------------------------------------------------------------------------------- */
include('cconfig.php');

//=========if form is posted ===========================// 
// code to update into database
	if(isset($_POST['submit']))
	{ 
		$subject1	=	$_POST['subject1'];
		$subject2	=	$_POST['subject2'];
		$news1		= $_POST['news1']; 
		$news2		= $_POST['news2']; 
		$id			=	$_POST['annId'];
		$caf_form		=	$_POST['kyc-form'];
		
		//==============UPLOAD FOR id_proof=============================//
		if($_FILES['kyc-form'] && $_FILES["kyc-form"]["name"]!='')
		{	
			$name 				= 	$_FILES["kyc-form"]["name"];
			$extension	 		= 	end((explode(".", $name)));
			$filetype	 		= 	$_FILES['kyc-form']['type'];
			$size 				= 	$_FILES['kyc-form']['size'];
			$destinationPath 	= 	'../announcements/';
			
			
			$idProofFileName	=	time();
			$idProofFileName	=	$userId.'_'.date('Ymdhis').'.'.$extension;
			$idUrl				= 	'../announcements/'.$idProofFileName;
			
							
			//if image file is > 1Mb
			if($size > 5024000)
			{
				// show error
				$errmsg	=	base64_encode(serialize("File size is greater than 1Mb!"));
				header("Location:news.php?errmsg=$errmsg");
				exit;	
			}
			if(($extension=='JPG' || $extension=='JPEG' || 
				$extension=='jpg' || $extension=='jpeg' || 
				$extension=='png' || $extension=='PNG' || 
				$extension=='pdf' || $extension=='PDF') )
			{
				try 
				{
					
					move_uploaded_file($_FILES["kyc-form"]["tmp_name"],$idUrl);  
	   
				} 
				catch(Exception $e) 
				{ 
					$errmsg	=	base64_encode(serialize("Error occured while uploading file please try again..!"));
					header("Location:news.php?errmsg=$errmsg");
					exit;			
				}
			}
			else
			{
				$errmsg	=	base64_encode(serialize("Invalid File Format.. Only JPG/JPEG/PNG upto 5 mb is allowed"));
				header("Location:news.php?errmsg=$errmsg");
				exit;
			}
		}			
  
		$query 	=	mysqli_query($con,"UPDATE announcements SET subject_eng='".$subject1."',subject_mar='".$subject2."',image_path='".$idUrl."',announcement_eng='".$news1."',announcement_mar='".$news2."',updated_on='".date('Y-m-d H:i:s')."' WHERE id=".$id);
		
		
		$sucmsg	=	base64_encode(serialize("New Updated sucessfully!"));
		header("Location:news.php?sucmsg=$sucmsg");
		exit;
	}

// to sho active menus
$menu = array();
$menu['ns'] = 1;  
include('header.php');
include('nav.php'); 
 
$annauncments =	array();
$getannouncements	= "SELECT * FROM announcements where id=".$_GET['id'];

$resultAnn	= mysqli_query($con,$getannouncements);
while($AnnList	= mysqli_fetch_assoc($resultAnn))
{
	$annauncments 	=	$AnnList;
}  ?>
<title>Edit News | <?php echo SITENAME;?></title>
<!-- Sidebar chat end-->
<div class="content-wrapper"> 
  <!-- Container-fluid starts -->
  <div class="container-fluid"> 
    
    <!-- Header Starts -->
    <div class="row">
      <div class="col-sm-12 p-0">
        <div class="main-header">
          <h4>Edit News</h4>
          <ol class="breadcrumb breadcrumb-title breadcrumb-arrow">
            <li class="breadcrumb-item"> <a href="dashboard.php"> <i class="icofont icofont-home"></i> </a> </li>
            <li class="breadcrumb-item"> <a href="news.php"> News </a> </li>
            <li class="breadcrumb-item"><a href="#">Edit News</a> </li>
          </ol>
        </div>
      </div>
    </div>
    <!-- Header end --> 
    
    <!-- Tables start --> 
    <!-- Row start -->
    <div class="row">
      <div class="col-sm-12">
        <?php if(isset($_GET['errmsg'])) { ?>
        <div class="card-block button-list notifications"><a href="#!" class="btn btn-danger waves-effect" data-type="danger"><strong>Sorry! </strong> <?php echo unserialize(base64_decode($_GET['errmsg']));?> </a></div>
        <?php } 
  if(isset($_GET['infomsg'])) { ?>
        <div class="card-block button-list notifications"><a href="#!" class="btn btn-info waves-effect" data-type="info"><strong>Note : </strong><?php echo unserialize(base64_decode($_GET['infomsg']));?></a></div>
        <?php } 
  if(isset($_GET['sucmsg'])) {?>
        <div class="card-block button-list notifications"> <a href="#!" class="btn btn-success waves-effect" data-type="success"><strong>Success! </strong><?php echo unserialize(base64_decode($_GET['sucmsg']));?></a></div>
        <?php } ?>
        
        <!-- Hover effect table starts -->
        <div class="card">
          <div class="card-header">
            <h5 class="card-header-text"><strong>Edit </strong> News</h5>
          </div>
          <form class="form-horizontal" id="profileedit" name="profileedit" data-collabel="3" method="post" enctype="multipart/form-data">
            <div class="card-block">
              <div class="row">
                <div class="col-sm-12 table-responsive">
                  <div class="card-block">
                    <div class="row">
                      <div class="col-sm-6">
                          <input type="hidden" name="annId" id="annId" value="<?php echo $annauncments['id'];?>">
                          <?php foreach($multiLang as $lan)
                          { $var1 = 'subject_'.$lan['short_lang'];?>
                        <div class="md-input-wrapper">
                          
                          <input type="text" name="subject<?php echo $lan['lang_id'];?>" id="subject<?php echo $lan['lang_id'];?>" class="md-form-control md-static" value="<?php echo $annauncments[$var1];?>">
                          <label><?php echo ucfirst($lan['lang']);?> Subject</label>
                        </div><?php } ?>
                      </div>
                      <div class="col-sm-6">
                          <?php foreach($multiLang as $lan)
                          { 
                              $var = 'announcement_'.$lan['short_lang'];?>
                        <div class="md-group-add-on"> <span class="md-add-on"> <i class="icofont icofont-industries-alt-2"></i> </span>
                          <div class="md-input-wrapper">
                            <textarea  name="news<?php echo $lan['lang_id'];?>"  class="md-form-control md-static"  id="news<?php echo $lan['lang_id'];?>"><?php echo $annauncments[$var];?> </textarea>
                            <label><?php echo ucfirst($lan['lang']);?> Announcement</label>
                          </div>
                        </div><?php } ?>
                      </div>
                    </div>
                    <div class="col-sm-6">
                      <div class="md-group-add-on"> <span class="md-add-on-file">
                        <button class="btn btn-success waves-effect waves-light">File</button>
                        </span>
                        <div class="md-input-file">
                          <input type="file" class="" id="kyc-form" name="kyc-form"/>
                          <input type="text" class="md-form-control md-form-file" name="kyc-form" id='kyc-form'>
                          <label class="md-label-file">Image</label>
                        </div>
                      </div>
                    </div>
                    <div class="col-sm-6">
                      <div class="md-group-add-on">
                        <div class="md-input-wrapper">
                          <button type="submit" name="submit" class="btn btn-primary m-b-15 m-r-15"><i class="icofont icofont-save"></i> UPDATE</button>
                          <button type="reset" class="btn btn-danger waves-effect waves-light m-b-15 m-r-15">Cancel</button>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
<script>

jQuery(document).ready(function()
{
	document.getElementById("uploadBtn1").onchange = function () 
	{
		document.getElementById("uploadFile1").value = this.value;
	};		
	 
});

</script>
<?php include('footer.php');?>
