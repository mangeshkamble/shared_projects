<?php 
/* * ---------------------------------------------------------------------------------
 * @MAHESHFULSUNDAR
 * @author           : Mahesh Fulsundar
 * @owner            : Mahesh Fulsundar
 * @version          : MAHESHFULSUNDAR 1.0 12-06-2018
 * @Notes            : All copyrights are reserved!
 * @rights           : All rights are reserved to the owner of the file creator.
  No one can do change to this file without permission of the owner.
 * @fileName         :
 * @dependancies     :
  ------------------------------------------------------------------------------------- */
include('cconfig.php');
include('header.php');
include('nav.php');
?>
<title>Orders Report |<?php echo SITENAME;?></title>
<div class="page-inner">
  <div class="page-breadcrumb">
    <ol class="breadcrumb container">
      <li><a href="dashboard.php">Home</a></li>
      <li class="active">Orders Report</li>
    </ol>
  </div>
  <div class="page-title">
    <div class="container">
      <h3>Orders Report</h3>
    </div>
  </div>
  <div class="panel panel-white">





        <div class="panel-heading clearfix" align="right"> 
        <?php
                $user_type = $_SESSION["user_type"];
                $user_id = $_SESSION["UserId"];

                if($user_type!=2){
        ?>
            <a href="export_excel.php?fileName=user_report&query=<?php echo "SELECT o.invoice_no,o.shipping_code,o.order_amount,o.create_date,c.full_name,os.status_name FROM orders o LEFT JOIN customers c ON c.customer_id = o.cust_id LEFT JOIN order_status os ON os.status_id = o.order_status_id WHERE o.create_date >= DATE_SUB(NOW(), INTERVAL 7 DAY)"; ?>">
                <button  class="btn btn-sm btn-default" type="button" title="Weekly Product Report download"><i class="fa fa-download"></i>Weekly</button>
            </a>
            <a href="export_excel.php?fileName=user_report&query=<?php echo "SELECT o.invoice_no,o.shipping_code,o.order_amount,o.create_date,c.full_name,os.status_name FROM orders o LEFT JOIN customers c ON c.customer_id = o.cust_id LEFT JOIN order_status os ON os.status_id = o.order_status_id WHERE o.create_date >= DATE_SUB(NOW(), INTERVAL 30 DAY)"; ?>">
                <button  class="btn btn-sm btn-primary" type="button" title="Monthly Product Report download"><i class="fa fa-download"></i>Monthly</button>
            </a>
            <a href="export_excel.php?fileName=user_report&query=<?php echo "SELECT o.invoice_no,o.shipping_code,o.order_amount,o.create_date,c.full_name,os.status_name FROM orders o LEFT JOIN customers c ON c.customer_id = o.cust_id LEFT JOIN order_status os ON os.status_id = o.order_status_id WHERE o.create_date >= DATE_SUB(NOW(), INTERVAL 365 DAY)"; ?>">
                <button  class="btn btn-sm btn-danger" type="button" title="Yearly Product Report download"><i class="fa fa-download"></i>Yearly</button>
            </a>
            <?php 
                }else{
            ?>
            <a href="export_excel.php?fileName=user_report&query=<?php echo "SELECT o.invoice_no,o.shipping_code,o.order_amount,o.create_date,c.full_name,os.status_name FROM orders o LEFT JOIN customers c ON c.customer_id = o.cust_id LEFT JOIN order_status os ON os.status_id = o.order_status_id LEFT JOIN order_details od on od.order_id=o.order_id LEFT JOIN products p ON p.product_id=od.product_id WHERE p.created_by=$user_id AND o.create_date >= DATE_SUB(NOW(), INTERVAL 7 DAY) GROUP BY o.order_id"; ?>">
                <button  class="btn btn-sm btn-default" type="button" title="Weekly Product Report download"><i class="fa fa-download"></i>Weekly</button>
            </a>
            <a href="export_excel.php?fileName=user_report&query=<?php echo "SELECT o.invoice_no,o.shipping_code,o.order_amount,o.create_date,c.full_name,os.status_name FROM orders o LEFT JOIN customers c ON c.customer_id = o.cust_id LEFT JOIN order_status os ON os.status_id = o.order_status_id LEFT JOIN order_details od on od.order_id=o.order_id LEFT JOIN products p ON p.product_id=od.product_id WHERE p.created_by=$user_id AND o.create_date >= DATE_SUB(NOW(), INTERVAL 30 DAY) GROUP BY o.order_id"; ?>">
                <button  class="btn btn-sm btn-primary" type="button" title="Monthly Product Report download"><i class="fa fa-download"></i>Monthly</button>
            </a>
            <a href="export_excel.php?fileName=user_report&query=<?php echo "SELECT o.invoice_no,o.shipping_code,o.order_amount,o.create_date,c.full_name,os.status_name FROM orders o LEFT JOIN customers c ON c.customer_id = o.cust_id LEFT JOIN order_status os ON os.status_id = o.order_status_id LEFT JOIN order_details od on od.order_id=o.order_id LEFT JOIN products p ON p.product_id=od.product_id WHERE p.created_by=$user_id AND o.create_date >= DATE_SUB(NOW(), INTERVAL 365 DAY) GROUP BY o.order_id"; ?>">
               <button  class="btn btn-sm btn-danger" type="button" title="Yearly Product Report download"><i class="fa fa-download"></i>Yearly</button>
            </a>

            <?php        
                }
            ?>
        </div>
















    <div class="panel-body">
      <div class="row m-b-lg table-responsive">
        <?php 
              $user_type  = $_SESSION["user_type"];
              $user_id    = $_SESSION["UserId"];
              $getproducts= " SELECT 
                                    o.order_id,
                                    o.invoice_no,
                                    c.full_name,
                                    o.shipping_code,
                                    o.bill_amount,
                                    create_date,
                                    order_status_id,
                                    os.status_name
                                FROM
                                    orders o
                                        LEFT JOIN
                                    customers c ON c.customer_id = o.cust_id
                                        LEFT JOIN
                                    order_status os ON os.status_id = o.order_status_id
                                        LEFT JOIN
                                    order_details od ON od.order_id = o.order_id
                                        LEFT JOIN
                                    products p ON p.product_id = od.product_id";
              if($user_type==2){
                $getproducts.= " WHERE p.created_by = $user_id";
              }
              $getproducts.= " ORDER BY o.order_id DESC";							  
		    
			$getTransaction	= mysqli_query($con,$getproducts);
		    ?>
        <table id="example" class="display table" style="width: 100%;" role="grid" aria-describedby="example_info">
          <thead>
            <tr>
              <th>Sr</th>
              <th>Invoice No</th> 
              <th>Customer</th>
              <th>Shipping Code</th> 
              <th>Amount</th>
              <th>Date</th>
              <th>Status </th>
              <th>Action</th>
            </tr>
          </thead>
          <tfoot>
            <tr>
              <th>Sr</th>
               <th>Invoice No</th> 
              <th>Customer</th>
              <th>Shipping Code</th> 
              <th>Amount</th>
              <th>Date</th>
              <th>Status </th>
              <th>Action</th>
            </tr>
          </tfoot>
          <tbody>
            <?php
		  $i = 0;
          while($productList	= mysqli_fetch_assoc($getTransaction))
          {  
			 $i++;
			 $order_id		   		= $productList['order_id'];
			$invoice_no		   		= $productList['invoice_no'];
			$cust_name				  = $productList['full_name'];
			$shipping_code		  = $productList['shipping_code'];
			$bill_amount    		= $productList['bill_amount'];
			$create_date    		= $productList['create_date'];
			$order_status    		= $productList['status_name']; 
					   			 
              ?>
            <tr>
              <td><?php echo $i;?></td>
              <td><?php echo $invoice_no;?></td>
              <td><?php echo $cust_name;?></td>
              <td><?php echo $shipping_code;?></td>
              <td><?php echo $bill_amount;?></td>
              <td><?php echo $create_date;?></td> 
              <td class="text-right"><div class="btn-group text-right">
                  <?php if($order_status=='Pending')
					{ ?>
                  <button type="button" class="btn btn-warning"><?php echo $order_status;?></button>
                  <?php }
					elseif($order_status=='Delivered')
					{ ?>
                  <button type="button" class="btn btn-success" ><?php echo $order_status;?></button>
                  <?php
					}
					else
					{?>
                  <button type="button" class="btn btn-danger"><?php echo $order_status;?></button>
                  <?php
					} ?>
                </div></td>
              <td><a href="orders_report_details.php?tid=<?php echo base64_encode(serialize($order_id));?>" title="View Details"><i class="fa fa-eye"></i></a>&nbsp; | &nbsp; <!--<a href="customer_invoice.php?tid=<?php //echo base64_encode(serialize($order_id));?>" target="_blank" title="Customer Invoice"><i class="fa fa-print"></i></a>--></td>
            </tr>
            <?php
          }
          ?>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
<?php include('footer.php');?>
