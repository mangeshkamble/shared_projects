<?php 
/* * ---------------------------------------------------------------------------------
 * @MAHESHFULSUNDAR
 * @author           : Mahesh Fulsundar
 * @owner            : Mahesh Fulsundar
 * @version          : MAHESHFULSUNDAR 1.0 12-06-2018
 * @Notes            : All copyrights are reserved!
 * @rights           : All rights are reserved to the owner of the file creator.
  No one can do change to this file without permission of the owner.
 * @fileName         :
 * @dependancies     :
  ------------------------------------------------------------------------------------- */
include('cconfig.php');
include('header.php');
include('nav.php');
?>
<title>Order Details |<?php echo SITENAME;?></title>
<div class="page-inner">
  <div class="page-breadcrumb">
    <ol class="breadcrumb container">
      <li><a href="dashboard.php">Home</a></li>
      <li class="active">Order Details</li>
    </ol>
  </div>
  <div class="page-title">
    <div class="container">
      <h3>Order Details</h3>
    </div>
  </div>
  <div class="panel panel-white">
    <div class="panel-body">
      <div class="row m-b-lg table-responsive">
        <div role="tabpanel"> 
          <!-- Nav tabs -->
          <?php
		  $orderNo		=	unserialize(base64_decode($_GET['tid']));
		   $order_details	=	 getOrderDetails($con,$orderNo);
	
	$invoice_no			=	$order_details['invoice_no'];
	$cust_id			=	$order_details['cust_id'];
	$store_id			=	$order_details['store_id'];
	$cust_name			=	$order_details['cust_name'];
	$email_id			=	$order_details['email_id'];
	$mobile				=	$order_details['mobile'];
	$shipping_address1	=	$order_details['shipping_address1'];
	$shipping_address2	=	$order_details['shipping_address2'];
	$shipping_city_id	=	$order_details['shipping_city_id'];
	$cityDetails		=	getCityDetails($con,$shipping_city_id);								
	$cityName			=	$cityDetails['city_name'];
	
	$shipping_state_id	=	$order_details['shipping_state_id'];
	$stateDetails		=	getstateDetails($con,$shipping_state_id);
	$stateName			=	$stateDetails['state'];
	$shipping_country_id =	$order_details['shipping_country_id'];
	$shipping_pincode	=	$order_details['shipping_pincode'];
	$shipping_method	=	$order_details['shipping_method'];
	if($shipping_method=='Home')
	{
		$shipping_method =	'Home Delivery';
	}
	else
	{
		$shipping_method =	'By hand';
	}
	$shipping_code		=	$order_details['shipping_code'];
	$order_amount		=	0;
	$shipping_charges	=	$order_details['shipping_charges'];
	$coupan_code		=	$order_details['coupan_code'];
	$discount_amount	=	$order_details['discount_amount'];
	$bill_amount		=	$order_details['bill_amount'];
	$payment_mode		=	$order_details['payment_mode'];
	$transaction_id		=	$order_details['transaction_id'];
	$receipt_no			=	$order_details['receipt_no'];
	$cust_ipaddress		=	$order_details['cust_ipaddress'];
	$transaction_status =	$order_details['transaction_status'];
	$transaction_remarks=	$order_details['transaction_remarks'];
	$transaction_type	=	$order_details['transaction_type'];
	$status_id	=	$order_details['order_status_id'];
	$status = getStatus($con,$status_id);
	$create_date		=	date('d-m-Y',strtotime($order_details['create_date']));
	if($order_details['delivery_date'] != '0000-00-00')
		$delivery_date	=	date('d-m-Y',strtotime($order_details['delivery_date']));
	else
		$delivery_date	=	'';
	$delivery_by		=	$order_details['delivery_by'];
	$expected_delivery_date = date('d-m-Y',strtotime($order_details['expected_delivery_date']));
	
	//$status			=	getStatus($order_status_id,$con);
	$countryDetails	=	getCountry($con,$shipping_country_id);
	$countryName	=	$countryDetails['name'];
	//-------------GetOrder details------------------------------------------//
	
	//if($authId == 1) {
		$orderProduct 	= getOrderProducts($con,$orderNo);
	//}
	//else {
		//$orderProduct	= getOrderProductsNew($orderNo, $userId);
//}
	$productIdArr	= 	array();
	
	foreach($orderProduct as $key)
	{
		array_push($productIdArr, $key['product_id']);
	}
	
	$myarry	=	base64_encode(json_encode($productIdArr));
	?>
          <ul class="nav nav-pills nav-tabs nav-justified" role="tablist">
          
            <li role="presentation" class="active"><a href="#tab21" role="tab" data-toggle="tab">Order Details</a></li>
            <li role="presentation"><a href="#tab22" role="tab" data-toggle="tab">Payment Details</a></li>
            <li role="presentation"><a href="#tab23" role="tab" data-toggle="tab">Shipping Details</a></li>
            <li role="presentation"><a href="#tab24" role="tab" data-toggle="tab">Products</a></li>
            <li role="presentation"><a href="#tab25" role="tab" data-toggle="tab">History</a></li>
          </ul>
          <!-- Tab panes -->
          <div class="tab-content">
            <div role="tabpanel" class="tab-pane active fade in" id="tab21">
               <table class="table table-bordered">
                <tbody>
                  <tr>
                    <td>Order ID:</td>
                    <td><?php echo $orderNo;?></td>
                  </tr>
                  <tr>
                    <td>Invoice No.:</td>
                    <td><?php echo $invoice_no;?></td>
                  </tr>
                  <tr>
                    <td>Store Name:</td>
                    <td><?php echo $orderNo;?></td>
                  </tr>
                  <tr>
                  <tr>
                    <td>Customer:</td>
                    <td><?php echo $cust_name;?></td>
                  </tr>
                  <tr>
                    <td>E-Mail:</td>
                    <td><?php echo $email_id;?></td>
                  </tr>
                  <tr>
                    <td>Telephone:</td>
                    <td><?php echo $mobile;?></td>
                  </tr>
                  <tr>
                    <td>Total:</td>
                    <td><?php echo $bill_amount;?></td>
                  </tr>
                  <tr>
                    <td>Order Status:</td>
                    <td id="order-status"><?php echo $status;?></td>
                  </tr>
                  <tr>
                    <td>IP Address:</td>
                    <td><?php echo $cust_ipaddress;?></td>
                  </tr>
                  <tr>
                    <td>Date Added:</td>
                    <td><?php echo $create_date;?></td>
                  </tr>
                  <tr>
                    <td>Expected Date Delivery:</td>
                    <td><?php echo $expected_delivery_date;?></td>
                  </tr>
                  <tr>
                    <td>Date Delivery:</td>
                    <td><?php echo $delivery_date;?></td>
                  </tr>
                </tbody>
              </table> 
            </div>
            <div role="tabpanel" class="tab-pane fade" id="tab22">
              <table class="table table-bordered">
                <tbody>
                  <tr>
                    <td>Customer Name:</td>
                    <td><?php echo $cust_name;?></td>
                  </tr>
                  <tr>
                    <td>Address :</td>
                    <td><?php echo $shipping_address1.' , '.$shipping_address2;?></td>
                  </tr>
                  <tr>
                    <td>City:</td>
                    <td><?php echo $cityName;?></td>
                  </tr>
                  <tr>
                    <td>Postcode:</td>
                    <td><?php echo $shipping_pincode;?></td>
                  </tr>
                  <tr>
                    <td>Region / State:</td>
                    <td><?php echo $stateName;?></td>
                  </tr>
                  <tr>
                    <td>Country:</td>
                    <td><?php echo $countryName; ?></td>
                  </tr>
                  <tr>
                    <td>Payment Method:</td>
                    <td><?php if($payment_mode=='COD') { echo 'Cash On Delivery'; } else { echo $payment_mode; }?></td>
                  </tr>
                </tbody>
              </table>
            </div>
            <div role="tabpanel" class="tab-pane fade" id="tab23">
               <table class="table table-bordered">
                <tbody>
                  <tr>
                    <td>Customer Name:</td>
                    <td><?php echo $cust_name;?></td>
                  </tr>
                  <tr>
                    <td>Address :</td>
                    <td><?php echo $shipping_address1.' , '.$shipping_address2;?></td>
                  </tr>
                  <tr>
                    <td>City:</td>
                    <td><?php echo $cityName;?></td>
                  </tr>
                  <tr>
                    <td>Postcode:</td>
                    <td><?php echo $shipping_pincode;?></td>
                  </tr>
                  <tr>
                    <td>Region / State:</td>
                    <td><?php echo $stateName;?></td>
                  </tr>
                  <tr>
                    <td>Country:</td>
                    <td><?php echo $countryName;?></td>
                  </tr>
                  <tr>
                    <td>Shipping Method:</td>
                    <td><?php echo $shipping_method;?></td>
                  </tr>
                </tbody>
              </table>
            </div>
            <div role="tabpanel" class="tab-pane fade" id="tab24">
            <table class="table table-bordered">
                <thead>
                  <tr>
                    <td class="text-left">Product</td>
                    <td class="text-left">Model</td>
                    <td class="text-right">Quantity</td>
                    <td class="text-right">Unit Price</td>
                    <td class="text-right">Total</td>
                  </tr>
                </thead>
                <tbody>
                  <?php 
				  foreach($orderProduct as $product)
				  {
					  ?>
                      <tr>
                        <td class="text-left"><?php echo $product['product_name'];?></td>
                        <td class="text-left"><?php echo $product['product_code'];?></td>
                        <td class="text-right"><?php echo $product['quantity'];?></td>
                        <td class="text-right"><?php echo $product['costper_unit'];?></td>
                        <td class="text-right"><?php echo $product['total_cost'];?></td>
                      </tr>
                      <?php
					  $order_amount += $product['total_cost'];
					  $bill_amount  += $product['total_cost'];
				  }
				  ?>
                  <!--<tr>
                    <td colspan="4" class="text-right" align="right">Sub-Total:</td>
                    <td class="text-right"><?php //echo $order_amount;?></td>
                  </tr>-->
                  <tr>
                    <td colspan="4" class="text-right">Flat Shipping Rate:</td>
                    <td class="text-right"><?php echo $shipping_charges;?></td>
                  </tr>
                  <tr>
                    <td colspan="4" class="text-right">Discount:</td>
                    <td class="text-right"><?php echo $discount_amount;?></td>
                  </tr>
                  <!--<tr>
                    <td colspan="4" class="text-right">Tax:</td>
                    <td class="text-right"><?php echo '0';?></td>
                  </tr>
                  <tr>
                    <td colspan="4" class="text-right">VAT (0%):</td>
                    <td class="text-right"><?php echo '0';?></td>
                  </tr>-->
                  <tr>
                    <td colspan="4" class="text-right">Total:</td>
                    <td class="text-right"><?php echo ($order_amount +$shipping_charges - $discount_amount);?></td>
                  </tr>
                </tbody>
              </table>
            </div>
            <div role="tabpanel" class="tab-pane fade" id="tab25">
            <table class="table table-bordered" id="tblHistory">
                <thead>
                  <tr>
                    <td class="text-left">Date Added</td> 
                    <td class="text-left">Comment</td>
                    <td class="text-left">Status</td>
                    <td class="text-left">Customer Notified</td>
                  </tr>
                </thead>
                <tbody>
                  <?php 
				  foreach($productIdArr as $product_id)
				  {
					  $trackHistory	= getTrackingHistory($con,$orderNo,$product_id);
					  foreach($trackHistory as $tracker)
					  { 
						  $trackStatus		= getStatus($con,$tracker['status_id']);
						  ?>
						  <tr>
							<td class="text-left"><?php echo date('d/m/Y H:i:s',strtotime($tracker['date_added'])); ?></td>
							 
							<td class="text-left"><?php echo $tracker['comments']; ?></td>
							<td class="text-left"><?php echo $trackStatus; ?></td>
							<td class="text-left"><?php echo $tracker['customer_notify']; ?></td>
						  </tr>
						  <?php 
					  }
				  }
				  ?>
                </tbody>
              </table>
            <?php
            if($_SESSION["user_type"]!=3 && $_SESSION["user_type"]!=2){
            ?>
            <div class="navbar-inner span6">
                  <table class="table table-bordered ">
                    <tbody>
                      <tr>
                        <td colspan="2"><h3>Add Order History</h3></td>
                      </tr>
                      <tr>
                        <td>Order Status :</td>
                        <td>
                            <select name="inputStatus" id="inputStatus" class="form-control">
                            <?php 
							$statusDetails	=	getAllStatus($con);
							foreach($statusDetails as $rstatus)
							{
							
								$oStatusId		= $rstatus['status_id'];
								$oStatusName	= $rstatus['status_name'];
								?>
                                <option value="<?php echo $oStatusId; ?>" <?php if($status_id >= $oStatusId) echo "disabled"; ?> 
									<?php if($status == $oStatusId) echo "selected"; ?>>
									<?php echo $oStatusName;?>
                                </option>
								<?php 
							}
							?>
                            </select>
                        </td>
                      </tr>
                      <tr>
                        <td>Notify Customer :</td>
                        <td><div class="col-sm-10">
                            <input type="checkbox" name="inputNotify" id="inputNotify" onChange="notify()">
                            <input type="hidden" name="usernotify" id="usernotify" value="No">
                          </div></td>
                      </tr>
                      <tr>
                        <td>Comment :</td>
                        <td><div class="col-sm-10">
                            <textarea name="inputComment" rows="8" id="inputComment" class="form-control"></textarea>
                          </div></td>
                      </tr>
                      <tr>
                        <td colspan="2">
                        <div class="col-sm-10 text-right" align="right">
                        <?php //$status	= getStatus($status);
						if($status=='Delivered' || $status=='Cancelled' || $status=='Return')
						{ ;?>
                            <button id="addHistory" disabled onClick="add_history(<?php echo $orderNo;?>, '<?php echo $myarry ?>')" data-loading-text="Loading..." class="btn btn-primary"><i class="fa fa-plus-circle"></i> Add History</button>
                            <?php 
						}
						else
						{ ?>
                        	<button id="addHistory" onClick="add_history(<?php echo $orderNo;?>, '<?php echo $myarry ?>')" data-loading-text="Loading..." class="btn btn-primary"><i class="fa fa-plus-circle"></i> Add History</button>
                        	<?php 
						} ?>
                          </div></td>
                      </tr>
                    </tbody>
                  </table>
                </div>
            <?php
               }
            ?>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<script>
function add_history(orderNo,product_id)
{
	var comments	=	document.getElementById("inputComment").value;
	var inputStatus	=	document.getElementById("inputStatus").value;
	var inputNotify	=	document.getElementById("usernotify").value;
	
	var myData = 'comments='+ comments +'&inputStatus='+inputStatus+'&inputNotify='+inputNotify+'&orderNo='+orderNo+'&product_id='+product_id;
	
	jQuery.ajax({
		type: "POST", // HTTP method POST or GET
		url: "add_history.php", //Where to make Ajax calls
		dataType:"text", // Data type, HTML, json etc.
		data:myData, //Form variables
		success:function(response){
			console.log(response);
			if(response == 'no_quantity'){
				alert('Sorry, Not enough quantity available!');
			} else {
				obj = JSON.parse(response);
				$("#tblHistory").append(obj.track);
			}
		},
		error:function (xhr, ajaxOptions, thrownError){
 			alert(thrownError);
		}
	});
}

	function notify()
	{
		var x=$("#inputNotify").is(":checked");
		if(x==true)
		{
			$("#usernotify").val('Yes');
		}
		else
		{
			$("#usernotify").val('No');
		}
	}
</script>
<!-- leftpanel -->
<?php include('footer.php');?>
