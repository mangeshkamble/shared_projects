<?php 
/* * ---------------------------------------------------------------------------------
 * @MAHESHFULSUNDAR
 * @author           : Mahesh Fulsundar
 * @owner            : Mahesh Fulsundar
 * @version          : MAHESHFULSUNDAR 1.0 12-06-2018
 * @Notes            : All copyrights are reserved!
 * @rights           : All rights are reserved to the owner of the file creator.
  No one can do change to this file without permission of the owner.
 * @fileName         :
 * @dependancies     :
  ------------------------------------------------------------------------------------- */
include('cconfig.php'); 

if(isset($_POST['action']) && $_POST['action']=='add')
{
	 $units			=	$_POST['units'];
	 $product_id	=	$_POST['product_id'];
	 $price			=	$_POST['price'];
	 $tax			=	$_POST['tax'];
	 $discount		=	$_POST['discount'];
	 $quantity		=	$_POST['quantity'];
	 
	 $salPrice		=	$price+$tax-$discount;
 	
	$myQuery		=	mysqli_query($con,"SELECT * FROM products_price WHERE units ='".$units."' AND product_id=".$product_id);
	$ResultCount	=	mysqli_num_rows($myQuery);
	
	if($ResultCount >0)
	{
		$errmsg	=	base64_encode(serialize("Product Units already Exists!"));
		header("Location:products_price_add.php?id=".base64_encode(serialize($product_id))."&errmsg=$errmsg");
		exit;
	}
	else
	{
		$query 	=	mysqli_query($con,"INSERT INTO products_price(product_id,price,tax,discount,sale_price,units,quantity) VALUES  ('".$product_id."','".$price."','".$tax."','".$discount."','".$salPrice."','".$units."','".$quantity."')");
		
		$query1 	=	mysqli_query($con,"UPDATE products SET quantity = quantity +".$quantity." WHERE product_id =".$product_id);
		
		$sucmsg	=	base64_encode(serialize("Price added sucessfully!"));
		header("Location:products_price_add.php?id=".base64_encode(serialize($product_id))."&sucmsg=$sucmsg");
		exit;
	}
}
elseif(isset($_POST['action']) && $_POST['action']=='quantity')
{ 
	$quantity	=	$_POST['quantity'];
	$price_id	=	$_POST['price_id'];
	$product_id	=	$_POST['product_id']; 
	$query 	=	mysqli_query($con,"UPDATE products_price SET quantity = quantity +".$quantity." WHERE price_id =".$price_id);
	$query1 	=	mysqli_query($con,"UPDATE products SET quantity = quantity +".$quantity." WHERE product_id =".$product_id);
	$sucmsg	=	base64_encode(serialize("Quantity added sucessfully!"));
	header("Location:products_price_add.php?id=".base64_encode(serialize($product_id))."&sucmsg=$sucmsg");
	exit;
}
?>