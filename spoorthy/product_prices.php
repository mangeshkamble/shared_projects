<?php 
/* * ---------------------------------------------------------------------------------
 * @MAHESHFULSUNDAR
 * @author           : Mahesh Fulsundar
 * @owner            : Mahesh Fulsundar
 * @version          : MAHESHFULSUNDAR 1.0 12-06-2018
 * @Notes            : All copyrights are reserved!
 * @rights           : All rights are reserved to the owner of the file creator.
  No one can do change to this file without permission of the owner.
 * @fileName         :
 * @dependancies     :
  ------------------------------------------------------------------------------------- */
include('cconfig.php');
include('header.php');
include('nav.php');
?>
<title>Products Prices |<?php echo SITENAME;?></title>
<div class="page-inner">
  <div class="page-breadcrumb">
    <ol class="breadcrumb container">
      <li><a href="dashboard.php">Home</a></li>
      <li class="active">Products Prices</li>
    </ol>
  </div>
  <div class="page-title">
    <div class="container">
      <h3>Products Prices</h3>
    </div>
  </div>
  <?php if(isset($_GET['errmsg'])) { ?>
  <div class="alert alert-danger" role="alert"> <strong>Sorry !</strong> <a href="#" class="alert-link"><?php echo unserialize(base64_decode($_GET['errmsg']));?></a> </div>
  <?php } 
  if(isset($_GET['infomsg'])) { ?>
  <div class="alert alert-info" role="alert"> <strong>Note : </strong> <a href="#" class="alert-link"><?php echo unserialize(base64_decode($_GET['infomsg']));?></a> </div>
  <?php } 
  if(isset($_GET['sucmsg'])) {?>
  <div class="alert alert-success" role="alert"> <strong>Success !</strong> <a href="#" class="alert-link"><?php echo unserialize(base64_decode($_GET['sucmsg']));?></a> </div>
  <?php } ?>
  <div class="panel panel-white">
    <div class="panel-body">
      <div class="row m-b-lg table-responsive">
        <?php 
          $user_type  = $_SESSION["user_type"];
          $user_id    = $_SESSION["UserId"];

          $qry="SELECT 
                    product_name,
                    product_code,
                    prod.product_id,
                    prod.quantity,
                    view_count
                FROM
                    order_details ordersdls
                        LEFT JOIN
                    products prod ON ordersdls.product_id = prod.product_id ";
          if($user_type==2){
            $qry.=" WHERE prod.created_by=$user_id ";
          }
          $qry.=" GROUP BY prod.product_id";
         $order_details	=	mysqli_query($con,$qry);
		  $orderCount	=	mysqli_num_rows($order_details);
		    ?>
        <table id="example" class="display table" style="width: 100%;" role="grid" aria-describedby="example_info">
          <thead>
            <tr>
              <th>Sr</th>
              <th>Product Code</th>
              <th>Product Name</th>
              <th>Quantity</th>
              <th>View Count</th>
            </tr>
          </thead>
          <tfoot>
            <tr>
              <th>Sr</th>
              <th>Product Code</th>
              <th>Product Name</th>
              <th>Quantity</th>
              <th>View Count</th>
            </tr>
          </tfoot>
          <tbody>
            <?php $counter	=	1;
		  if($orderCount>0)
		  { 
			  while($orderList = mysqli_fetch_assoc($order_details))
			  {  
				 $product_name	=	$orderList['product_name'];
				 $product_code	=	$orderList['product_code'];
				 $quantity		=	$orderList['quantity'];
				 $view_count	=	$orderList['view_count'];
				?>
            <tr>
              <td><?php echo $counter++;?></td>
              <td><?php echo $product_name;?></td>
              <td><?php echo $product_code;?></td>
              <td><?php echo $quantity;?></td>
              <td><?php echo $view_count;?></td>
            </tr>
            <?php 
			  }
		  }?>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
<?php include("footer.php");?>