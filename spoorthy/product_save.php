<?php
/* * ---------------------------------------------------------------------------------
 * @MAHESHFULSUNDAR
 * @author           : Mahesh Fulsundar
 * @owner            : Mahesh Fulsundar
 * @version          : MAHESHFULSUNDAR 1.0 12-06-2018
 * @Notes            : All copyrights are reserved!
 * @rights           : All rights are reserved to the owner of the file creator.
  No one can do change to this file without permission of the owner.
 * @fileName         :
 * @dependancies     :
  ------------------------------------------------------------------------------------- */
include('cconfig.php');
 
if(isset($_POST['submit']))
{ 
	$image1	=	'';
	$image2	=	'';
	$image3	=	'';
	$image4	=	'';
	$image5	=	'';
        $create_date = date('Y-m-d H:i:s');
        $createdBy = 0;
        
	if($_FILES['image1'] && $_FILES["image1"]["name"]!=='')
	{	
		$name 				= 	$_FILES["image1"]["name"];
		$extension	 		=	 end((explode(".", $name)));
		$filetype	 		= 	$_FILES['image1']['type'];
		$size 				= 	$_FILES['image1']['size'];
		$destinationPath 	= 	'../upload/products/';
		
		$idProofFileName	=	time();
		$idProofFileName	=	'1-'.$idProofFileName.'.'.$extension;
		$image1				= 	'../upload/products/'.$idProofFileName;
		
		move_uploaded_file($_FILES["image1"]["tmp_name"],$image1);  
		$image1				= 	'upload/products/'.$idProofFileName;
	}
	
	if($_FILES['image2'] && $_FILES["image2"]["name"]!=='')
	{	
		$name 				= 	$_FILES["image2"]["name"];
		$extension	 		= 	end((explode(".", $name)));
		$filetype	 		= 	$_FILES['image2']['type'];
		$size 				= 	$_FILES['image2']['size'];
		$destinationPath 	= 	'../upload/products/';
		
		$idProofFileName	=	time();
		$idProofFileName	=	'2-'.$idProofFileName.'.'.$extension;
		$image2				= 	'../upload/products/'.$idProofFileName;
		
		move_uploaded_file($_FILES["image2"]["tmp_name"],$image2);  
		$image2				= 	'upload/products/'.$idProofFileName;
	}
	if($_FILES['image3'] && $_FILES["image3"]["name"]!=='')
	{	
		$name 				= 	$_FILES["image3"]["name"];
		$extension	 		= 	end((explode(".", $name)));
		$filetype	 		= 	$_FILES['image3']['type'];
		$size 				= 	$_FILES['image3']['size'];
		$destinationPath 	= 	'../upload/products/';
		
		$idProofFileName	=	time();
		$idProofFileName	=	'3-'.$idProofFileName.'.'.$extension;
		$image3				= 	'../upload/products/'.$idProofFileName;
		
		move_uploaded_file($_FILES["image3"]["tmp_name"],$image3);  
		$image3				= 	'upload/products/'.$idProofFileName;
	}
	if($_FILES['image4'] && $_FILES["image4"]["name"]!=='')
	{	
		$name 				= 	$_FILES["image4"]["name"];
		$extension	 		= 	end((explode(".", $name)));
		$filetype	 		= 	$_FILES['image4']['type'];
		$size 				= 	$_FILES['image4']['size'];
		$destinationPath 	= 	'../upload/products/';
		
		$idProofFileName	=	time();
		$idProofFileName	=	'4-'.$idProofFileName.'.'.$extension;
		$image4				= 	'../upload/products/'.$idProofFileName;
		
		move_uploaded_file($_FILES["image4"]["tmp_name"],$image4);  
		$image4				= 	'upload/products/'.$idProofFileName;
	}
	if($_FILES['image5'] && $_FILES["image5"]["name"]!=='')
	{	
		$name 				= 	$_FILES["image5"]["name"];
		$extension	 		= 	end((explode(".", $name)));
		$filetype	 		= 	$_FILES['image5']['type'];
		$size 				= 	$_FILES['image5']['size'];
		$destinationPath 	= 	'../upload/products/';
		
		$idProofFileName	=	time();
		$idProofFileName	=	'5-'.$idProofFileName.'.'.$extension;
		$image5				= 	'../upload/products/'.$idProofFileName;
		
		move_uploaded_file($_FILES["image5"]["tmp_name"],$image5);  
		$image5				= 	'upload/products/'.$idProofFileName;
	}
	 
	 
	$productsadded	=	mysqli_query($con,"INSERT INTO products(category_id,product_code,product_name,product_brand,product_desc,"
                . "image1,image2,image3,image4,image5,product_definition,product_cat_definition,product_stock,"
                . "video_link,health_benifits,long_description,product_price,discounted_price,city_price,"
                . "tax_category,tax_slab,receipe,created_by,created_on) "
                . "VALUES ('".$_POST['category_id']."','".$_POST['product_code']."','".$_POST['product_name']."','".$_POST['product_brand']."','".$_POST['product_desc']."',
                '".$image1."','".$image2."','".$image3."','".$image4."','".$image5."','".$_POST['product_definition']."',
                '".$_POST['product_cat_definition']."','".$_POST['product_stock']."','".$_POST['video_link']."','".$_POST['health_benefits']."',
                '".$_POST['long_decription']."','".$_POST['product_price']."','".$_POST['discounted_price']."',
                '".$_POST['city_wise_price']."','".$_POST['type_of_tax']."','".$_POST['tax_per']."','".$_POST['receipe']."',
                '".$createdBy."','".$create_date."'
                )");
	
	if($productsadded)
	{
                $productId = $con->insert_id;
                
                $productMarathi = mysqli_query($con,"INSERT INTO products_marathi(product_id,product_name,product_brand,product_desc,health_benifits,long_description,receipe) 
                VALUES ('".$productId."','".$_POST['product_name_mar']."','".$_POST['product_brand_mar']."','".$_POST['product_desc_mar']."',
                '".$_POST['health_benefits_mar']."','".$_POST['long_decription_mar']."','".$_POST['receipe_mar']."')");
                
                if($productMarathi){
                    $sucmsg	=	base64_encode(serialize("New product added successfully!"));
                    header("Location:products_report.php?sucmsg=$sucmsg");
                    exit;
                }else{
                    $errmsg	=	base64_encode(serialize("Error adding marathi details!"));
                    header("Location:products_report.php?errmsg=$errmsg");
                    exit;
                }
		
	}
	else
	{
		$errmsg	=	base64_encode(serialize("Error adding new product!"));
		header("Location:products_report.php?errmsg=$errmsg");
		exit;
	}
}
?>
