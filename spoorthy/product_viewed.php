<?php 
/* * ---------------------------------------------------------------------------------
 * @MAHESHFULSUNDAR
 * @author           : Mahesh Fulsundar
 * @owner            : Mahesh Fulsundar
 * @version          : MAHESHFULSUNDAR 1.0 12-06-2018
 * @Notes            : All copyrights are reserved!
 * @rights           : All rights are reserved to the owner of the file creator.
  No one can do change to this file without permission of the owner.
 * @fileName         :
 * @dependancies     :
  ------------------------------------------------------------------------------------- */
include('cconfig.php');
include('header.php');
include('nav.php');

#print_r($_SESSION);exit;
?>
<title>Products Viewed |<?php echo SITENAME;?></title>
<div class="page-inner">
  <div class="page-breadcrumb">
    <ol class="breadcrumb container">
      <li><a href="dashboard.php">Home</a></li>
      <li class="active">Products Viewed</li>
    </ol>
  </div>
  <div class="page-title">
    <div class="container">
      <h3>Products Viewed</h3>
    </div>
  </div>
  <?php if(isset($_GET['errmsg'])) { ?>
  <div class="alert alert-danger" role="alert"> <strong>Sorry !</strong> <a href="#" class="alert-link"><?php echo unserialize(base64_decode($_GET['errmsg']));?></a> </div>
  <?php } 
  if(isset($_GET['infomsg'])) { ?>
  <div class="alert alert-info" role="alert"> <strong>Note : </strong> <a href="#" class="alert-link"><?php echo unserialize(base64_decode($_GET['infomsg']));?></a> </div>
  <?php } 
  if(isset($_GET['sucmsg'])) {?>
  <div class="alert alert-success" role="alert"> <strong>Success !</strong> <a href="#" class="alert-link"><?php echo unserialize(base64_decode($_GET['sucmsg']));?></a> </div>
  <?php } ?>
  <div class="panel panel-white">
    <div class="panel-body">
      <div class="row m-b-lg table-responsive">
        <?php 
              $getproducts  = '';
              $user_type    = $_SESSION["user_type"];
              $user_id      = $_SESSION["UserId"];
              $getproducts.= " SELECT 
                                    product_code,product_name,view_count
                                FROM
                                    products
                                    INNER JOIN user ON user.id=products.created_by
                                        LEFT JOIN
                                    category ON category.category_id = products.category_id
                                WHERE
                                    products.deleteFlag = 'No'";
              if($user_type==2){
                $getproducts.=" AND user.id=$user_id";
              }

              $getproducts.=" ORDER BY product_id DESC";								  
		
			$getTransaction	= mysqli_query($con,$getproducts);
		    ?>
        <table id="example" class="display table" style="width: 100%;" role="grid" aria-describedby="example_info">
          <thead>
            <tr>
              <th>Sr</th>
              <th>Product Code</th>
              <th>Product Name</th>
              <th>Viewed</th>
              <th>Percent</th>
            </tr>
          </thead>
          <tfoot>
            <tr>
              <th>Sr</th>
              <th>Product Code</th>
              <th>Product Name</th>
              <th>Viewed</th>
              <th>Percent</th>
            </tr>
          </tfoot>
          <tbody>
            <?php $counter	=	1;
		  
              $qry="SELECT sum(view_count) as totalSum FROM products";
        if($user_type==2){
          $qry="SELECT sum(view_count) as totalSum FROM products where created_by=$user_id";
        }
			$product_counters	=	mysqli_query($con,"SELECT sum(view_count) as totalSum FROM products");
			
			$productCounter		=	mysqli_fetch_assoc($product_counters);
			
			$totalSum	=	$productCounter['totalSum'];
		  
		  $productCount	=	mysqli_num_rows($getTransaction);
		  
		  if($productCount>0)
		  { 
			  while($productList = mysqli_fetch_assoc($getTransaction))
			  { 
			  	//print_r($orderList);die;
				 $product_name	=	$productList['product_name'];
				 $product_code	=	$productList['product_code'];
				 $view_count	=	$productList['view_count'];
				 if($totalSum !=0)
				 {
				 	$view_percentage	=	($view_count/$totalSum)*100;
				 }
				 else
				 {
					 $view_percentage	=0;
				 }
				?>
            <tr>
              <td><?php echo $counter++;?></td>
              <td><?php echo $product_name;?></td>
              <td><?php echo $product_code;?></td>
              <td><?php echo $view_count;?></td>
              <td><?php echo round($view_percentage,2);?> % </td>
            </tr>
            <?php 
			  }
		  }?>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
<?php include("footer.php");?>