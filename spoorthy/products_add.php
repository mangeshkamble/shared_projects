<?php
/* * ---------------------------------------------------------------------------------
 * @MAHESHFULSUNDAR
 * @author           : Mahesh Fulsundar
 * @owner            : Mahesh Fulsundar
 * @version          : MAHESHFULSUNDAR 1.0 12-06-2018
 * @Notes            : All copyrights are reserved!
 * @rights           : All rights are reserved to the owner of the file creator.
  No one can do change to this file without permission of the owner.
 * @fileName         :
 * @dependancies     :
  ------------------------------------------------------------------------------------- */
include('cconfig.php');
include('header.php');
include('nav.php');
//Get state List 
 
?>
<title>Add New Product |<?php echo SITENAME;?></title>
<div class="page-inner">
    <div class="page-breadcrumb">
        <ol class="breadcrumb container">
            <li><a href="dashboard.php">Home</a></li>
            <li class="active">Add New Product</li>
        </ol>
    </div>
    <div class="page-title">
        <div class="container">
            <h3>Add New Product</h3>
        </div>
    </div>
    <div id="main-wrapper" class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-white">
                    <div class="panel-heading clearfix">
                        <h4 class="panel-title">Add New Product</h4>
                    </div>
                    <div class="panel-body">
                        <form class="form-horizontal" enctype="multipart/form-data" name="profilecreate" id="profilecreate" method="post" action="product_save.php">
                            <div class="col-md-6">
                                <div class="form-group">
                                <label for="input-help-block" class="col-sm-2 control-label">Category : </label>
                                <div class="col-sm-10">
                                    <select id="category_id" name="category_id" class="form-control">
                                        <option value="" selected="selected">Category...</option>
                                        <?php
                                        $query = mysqli_query($con, "SELECT * FROM subcategory sc INNER JOIN category cc ON cc.category_id=sc.category_id WHERE subcategory_status='Active'");
                                        while ($category = mysqli_fetch_assoc($query)) {
                                            ?>
                                            <option value="<?php echo $category['subcat_id']; ?>"><?php echo $category['category_eng'] . ' - ' . $category['subcategory_eng']; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <!--<div class="form-group">
                              <label for="input-placeholder" class="col-sm-2 control-label">Product Type : </label>
                              <div class="col-sm-10">
                                <select id="product_type" name="product_type" class="form-control">
                                  <option value="" selected="selected">Product Type</option>
                                  <option value="MEN">MEN</option>
                                  <option value="WOMEN">WOMEN</option>
                                  </select>
                              </div>
                            </div>-->
                            <div class="form-group">
                                <label for="input-Default" class="col-sm-2 control-label">Product Name : </label>
                                <div class="col-sm-10">
                                    <input type="text" name="product_name" class="form-control" id="product_name" placeholder="Enter product name">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Product Code : </label>
                                <div class="col-sm-10">
                                    <input type="text" name="product_code" id="product_code" class="form-control" placeholder="Product Code">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="input-rounded" class="col-sm-2 control-label">Product Brand : </label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" name="product_brand" id="product_brand"  placeholder="Product Brand No" >
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="input-rounded" class="col-sm-2 control-label">Product Price : </label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" name="product_price" id="product_price"  placeholder="Product Price" >
                                </div>
                            </div>
                            <!-- <div class="form-group">
                               <label for="input-disabled" class="col-sm-2 control-label">Product Price :</label>
                               <div class="col-sm-10">
                                 <input type="text" class="form-control" name="product_price" id="product_price" placeholder="Enter product price">
                               </div>
                             </div>
                             <div class="form-group">
                               <label for="input-readonly" class="col-sm-2 control-label">Product Discount : </label>
                               <div class="col-sm-10">
                                 <input type="text" class="form-control" name="discount" value="0" id="discount" >
                               </div>
                             </div>-->
                            <div class="form-group">
                                <label for="input-Default" class="col-sm-2 control-label">Product Description : </label>
                                <div class="col-sm-10">
                                    <textarea class="form-control" id="product_desc"  name="product_desc" placeholder="Product Description"></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="input-Default" class="col-sm-2 control-label">Product Type : </label>
                                <div class="col-sm-10">
                                    <input class="form-control radio" type="radio" name="product_definition" value="Veg" checked />Veg
                                    <input class="form-control radio" type="radio" name="product_definition" value="NonVeg" />Non-Veg
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="input-Default" class="col-sm-2 control-label">Product Bundle : </label>
                                <div class="col-sm-10">
                                    <input class="form-control radio" type="radio" name="product_cat_definition" value="Single" checked>Single
                                    <input class="form-control radio" type="radio" name="product_cat_definition" value="Combo">Combo
                                    <input class="form-control radio" type="radio" name="product_cat_definition" value="Personalized">Personalized
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="input-Default" class="col-sm-2 control-label">Delivery : </label>
                                <div class="col-sm-10">
                                    <input class="form-control radio" type="checkbox" name="delivery_type" value="Single" checked="checked">Regular
                                    <input class="form-control radio" type="checkbox" name="delivery_type" value="Combo">Express
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="input-Default" class="col-sm-2 control-label">Product Stock : </label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" name="product_stock" id="product_stock"  placeholder="Enter Stock" >
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="input-Default" class="col-sm-2 control-label">Health Benefits : </label>
                                <div class="col-sm-10">
                                    <textarea class="form-control" id="health_benefits"  name="health_benefits" placeholder="Health Benefits"></textarea>
                                </div>
                            </div>
<!--                            <div class="form-group">
                                <label for="input-Default" class="col-sm-2 control-label">Comments / Feedback : </label>
                                <div class="col-sm-10">
                                    <textarea class="form-control" id="comments"  name="comments" placeholder="Enter Comments"></textarea>
                                </div>
                            </div>-->
                            <div class="form-group">
                                <label for="input-Default" class="col-sm-2 control-label">Long Description : </label>
                                <div class="col-sm-10">
                                    <textarea class="form-control" id="long_decription"  name="long_decription" placeholder="Description"></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="input-Default" class="col-sm-2 control-label">Recipe : </label>
                                <div class="col-sm-10">
                                    <textarea class="form-control" id="receipe"  name="receipe" placeholder="Recipe"></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="input-rounded" class="col-sm-2 control-label">Discounted Price : </label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" name="discounted_price" id="discounted_price"  placeholder="Discounted Price" >
                                </div>
                            </div>
<!--                            <div class="form-group">
                                <label for="input-rounded" class="col-sm-2 control-label">City Price : </label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" name="city_wise_price" id="city_wise_price"  placeholder="City Price" >
                                </div>
                            </div>-->
                            <div class="form-group">
                                <label for="input-help-block" class="col-sm-2 control-label">Tax : </label>
                                <div class="col-sm-10">
                                    <select id="type_of_tax" name="type_of_tax" class="form-control">
                                        <option value="" selected="selected">Tax...</option>
                                        <?php
                                        $query_tax = mysqli_query($con, "SELECT * FROM tax_category WHERE status=1");
                                        while ($tax_category = mysqli_fetch_assoc($query_tax)) {
                                            ?>
                                            <option value="<?php echo $tax_category['id']; ?>"><?php echo $tax_category['name'] . ' - ' . $category['subcategory_eng']; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="input-rounded" class="col-sm-2 control-label">Tax Percentage : </label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" name="tax_per" id="tax_per"  placeholder="Tax" >
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Status : </label>
                                <div class="col-sm-10">
                                    <select name="status" id="status" class="form-control">
                                        <option value="Active" Selected >Active</option>
                                        <option value="Inactive">Inactive</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="input-rounded" class="col-sm-2 control-label">Video Link : </label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" name="video_link" id="video_link"  placeholder="Link" >
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="input-Default" class="col-sm-2 control-label">Product Images : </label>
                                <div class="col-sm-10">
                                    <input type="file" name="image1" id="image1" class="text-center event-name gui-input br-light bg-light" placeholder="Image 1">
                                    <br />
                                    <br />
                                    <input type="file" name="image2" id="image2" class="text-center event-name gui-input br-light bg-light" placeholder="Image 2">
                                    <br />
                                    <br />
                                    <input type="file" name="image3" id="image3" class="text-center event-name gui-input br-light bg-light" placeholder="Image 3">
                                    <br />
                                    <br />
                                    <input type="file" name="image4" id="image4" class="text-center event-name gui-input br-light bg-light" placeholder="Image 4">
                                    <br />
                                    <br />
                                    <input type="file" name="image5" id="image5" class="text-center event-name gui-input br-light bg-light" placeholder="Image 5">
                                </div>
                            </div>
                            </div>
                            <div class="col-md-6">
                                <div class="">
                                    <h6 class="panel-title">Details in Marathi</h6>
                                </div>
                                <div class="form-group">
                                    
                                <!--<label for="input-help-block" class="col-sm-3 control-label">Details in Marathi</label>-->
<!--                                <div class="col-sm-10">
                                    <select id="category_id" name="category_id" class="form-control">
                                        <option value="" selected="selected">Category...</option>
                                        <?php
                                        //$query = mysqli_query($con, "SELECT * FROM subcategory sc INNER JOIN category cc ON cc.category_id=sc.category_id WHERE subcategory_status='Active'");
                                        //while ($category = mysqli_fetch_assoc($query)) {
                                            ?>
                                            <option value="<?php //echo $category['subcat_id']; ?>"><?php // echo $category['category_eng'] . ' - ' . $category['subcategory_eng']; ?></option>
                                        <?php //} ?>
                                    </select>
                                </div>-->
                            </div>
                            <!--<div class="form-group">
                              <label for="input-placeholder" class="col-sm-2 control-label">Product Type : </label>
                              <div class="col-sm-10">
                                <select id="product_type" name="product_type" class="form-control">
                                  <option value="" selected="selected">Product Type</option>
                                  <option value="MEN">MEN</option>
                                  <option value="WOMEN">WOMEN</option>
                                  </select>
                              </div>
                            </div>-->
                            <div class="form-group">
                                <label for="input-Default" class="col-sm-2 control-label">Product Name : </label>
                                <div class="col-sm-10">
                                    <input type="text" name="product_name_mar" class="form-control" id="product_name_mar" placeholder="Enter product name">
                                </div>
                            </div>
<!--                            <div class="form-group">
                                <label class="col-sm-2 control-label">Product Code : </label>
                                <div class="col-sm-10">
                                    <input type="text" name="product_code" id="product_code" class="form-control" placeholder="Product Code">
                                </div>
                            </div>-->
                            <div class="form-group">
                                <label for="input-rounded" class="col-sm-2 control-label">Product Brand : </label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" name="product_brand_mar" id="product_brand_mar"  placeholder="Product Brand No" >
                                </div>
                            </div>
<!--                            <div class="form-group">
                                <label for="input-rounded" class="col-sm-2 control-label">Product Price : </label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" name="product_price" id="product_price"  placeholder="Product Price" >
                                </div>
                            </div>-->
                            <!-- <div class="form-group">
                               <label for="input-disabled" class="col-sm-2 control-label">Product Price :</label>
                               <div class="col-sm-10">
                                 <input type="text" class="form-control" name="product_price" id="product_price" placeholder="Enter product price">
                               </div>
                             </div>
                             <div class="form-group">
                               <label for="input-readonly" class="col-sm-2 control-label">Product Discount : </label>
                               <div class="col-sm-10">
                                 <input type="text" class="form-control" name="discount" value="0" id="discount" >
                               </div>
                             </div>-->
                            <div class="form-group">
                                <label for="input-Default" class="col-sm-2 control-label">Product Description : </label>
                                <div class="col-sm-10">
                                    <textarea class="form-control" id="product_desc_mar"  name="product_desc_mar" placeholder="Product Description"></textarea>
                                </div>
                            </div>
<!--                            <div class="form-group">
                                <label for="input-Default" class="col-sm-2 control-label">Product Type : </label>
                                <div class="col-sm-10">
                                    <input class="form-control radio" type="radio" name="product_definition" value="Veg" checked />Veg
                                    <input class="form-control radio" type="radio" name="product_definition" value="NonVeg" />Non-Veg
                                </div>
                            </div>-->
<!--                            <div class="form-group">
                                <label for="input-Default" class="col-sm-2 control-label">Product Bundle : </label>
                                <div class="col-sm-10">
                                    <input class="form-control radio" type="radio" name="product_cat_definition" value="Veg" checked>Single
                                    <input class="form-control radio" type="radio" name="product_cat_definition" value="NonVeg">Combo
                                    <input class="form-control radio" type="radio" name="product_cat_definition" value="NonVeg">Personalized
                                </div>
                            </div>-->
<!--                            <div class="form-group">
                                <label for="input-Default" class="col-sm-2 control-label">Product Stock : </label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" name="product_stock" id="product_stock"  placeholder="Enter Stock" >
                                </div>
                            </div>-->
                            <div class="form-group">
                                <label for="input-Default" class="col-sm-2 control-label">Health Benefits : </label>
                                <div class="col-sm-10">
                                    <textarea class="form-control" id="health_benefits_mar"  name="health_benefits_mar" placeholder="Health Benefits"></textarea>
                                </div>
                            </div>
<!--                            <div class="form-group">
                                <label for="input-Default" class="col-sm-2 control-label">Comments / Feedback : </label>
                                <div class="col-sm-10">
                                    <textarea class="form-control" id="comments"  name="comments" placeholder="Enter Comments"></textarea>
                                </div>
                            </div>-->
                            <div class="form-group">
                                <label for="input-Default" class="col-sm-2 control-label">Long Description : </label>
                                <div class="col-sm-10">
                                    <textarea class="form-control" id="long_decription_mar"  name="long_decription_mar" placeholder="Description"></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="input-Default" class="col-sm-2 control-label">Recipe : </label>
                                <div class="col-sm-10">
                                    <textarea class="form-control" id="receipe_mar"  name="receipe_mar" placeholder="Recipe"></textarea>
                                </div>
                            </div>
                            
                            
                            
                            
                            </div>
                            <hr>
                            <div class="form-group col-md-12" align="right">
                                <button type="submit" class="btn btn-primary btn-addon m-b-sm" name="submit">Add product</button>
                            </div>
                        </form>
                    </div>
                </div>
                <!--<div class="panel panel-white">-->
                    
                <!--</div>-->
            </div>
        </div>
    </div>
    <?php include('footer.php'); ?>
    <script>
        jQuery("#profilecreate").validate({
            rules: {
                'product_name': {
                    //'alphabets-only':true,
                    required: true
                },
                'category_id': {
                    //'alphabets-only':true,
                    required: true
                },
                'product_type': {
                    //'alphabets-only':true,
                    required: true
                },
                'product_code': {
                    //'alphabets-only':true,
                    //digits:true,
                    required: true
                },
                'product_price': {
                    digits: true,
                    required: true
                },
                'discount':
                        {
                            required: true,
                            digits: true
                        },
                'product_desc': {
                    required: true
                },
                'image1': {
                    required: true
                },

            }
        });

    </script>