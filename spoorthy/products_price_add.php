<?php 
/* * ---------------------------------------------------------------------------------
 * @MAHESHFULSUNDAR
 * @author           : Mahesh Fulsundar
 * @owner            : Mahesh Fulsundar
 * @version          : MAHESHFULSUNDAR 1.0 12-06-2018
 * @Notes            : All copyrights are reserved!
 * @rights           : All rights are reserved to the owner of the file creator.
  No one can do change to this file without permission of the owner.
 * @fileName         :
 * @dependancies     :
  ------------------------------------------------------------------------------------- */
include('cconfig.php'); 
include('header.php');
include('nav.php'); 
$myQuery	=	mysqli_query($con,"SELECT * FROM products LEFT JOIN subcategory ON subcategory.subcat_id = products.category_id WHERE products.deleteFlag='No' AND product_id=".unserialize(base64_decode($_GET['id'])));
	$prodResult	=	mysqli_fetch_assoc($myQuery);
	$product_id =	unserialize(base64_decode($_GET['id']));
?>
<title>Add Product Prices |<?php echo SITENAME;?></title>
<div class="page-inner">
  <div class="page-breadcrumb">
    <ol class="breadcrumb container">
      <li><a href="dashboard.php">Home</a></li>
      <li class="active">Add Product Prices</li>
    </ol>
  </div>
  
  <div class="page-title">
    <div class="container">
      <h3>Add Product Prices</h3>
    </div>
  </div>
   <?php if(isset($_GET['errmsg'])) { ?>
  <div class="alert alert-danger" role="alert"> <strong>Sorry !</strong> <a href="#" class="alert-link"><?php echo unserialize(base64_decode($_GET['errmsg']));?></a> </div>
  <?php } 
  if(isset($_GET['infomsg'])) { ?>
  <div class="alert alert-info" role="alert"> <strong>Note : </strong> <a href="#" class="alert-link"><?php echo unserialize(base64_decode($_GET['infomsg']));?></a> </div>
  <?php } 
  if(isset($_GET['sucmsg'])) {?>
  <div class="alert alert-success" role="alert"> <strong>Success !</strong> <a href="#" class="alert-link"><?php echo unserialize(base64_decode($_GET['sucmsg']));?></a> </div>
  <?php } ?>
  <div id="main-wrapper" class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="panel panel-white">
          <div class="panel-body">
            <div class="form-horizontal" id="category">
            <div class="form-group">
                <label for="input-Default" class="col-sm-2 control-label">SubCategory Name : </label>
                <div class="col-sm-5">
                 <input type="text" name="category_name" id="category_name" class="form-control" value="<?php echo $prodResult['subcategory_eng'];?>" readonly="readonly">
                </div>
              </div>
            <div class="form-group">
                <label for="input-Default" class="col-sm-2 control-label">Product Name : </label>
                <div class="col-sm-5">
                 <input type="text" name="product_name" id="product_name" class="form-control" value="<?php echo $prodResult['product_name'];?>" readonly="readonly">
                </div>
              </div>
             
              <div class="form-group">
                <label for="input-Default" class="col-sm-2 control-label">Product Code : </label>
                <div class="col-sm-5">
                  <input type="text" name="product_code" id="product_code" class="form-control" value="<?php echo $prodResult['product_code'];?>" readonly="readonly">
                  <input type="hidden" name="product_id" id="product_id" value="<?php echo $product_id;?>">
                </div>
              </div>
              <div class="form-group">
                <label for="input-Default" class="col-sm-2 control-label">Product Description : </label>
                <div class="col-sm-5">
                   <textarea name="product_desc" id="product_desc" class="form-control" readonly="readonly"> <?php echo $prodResult['product_desc'];?></textarea>
                </div>
              </div>
               
               <div class="form-group">
                <label for="input-Default" class="col-sm-2 control-label">Product Image: </label>
                <div class="col-sm-5">
                  <img src="../<?php echo $prodResult['image1'];?>" height="110" width="200">
                </div>
              </div>
              <div class="form-group">
                <label for="input-Default" class="col-sm-2 control-label">Status : </label>
                <div class="col-sm-5">
                  <select name="category_status" id="category_status" class="form-control" disabled="disabled">
                  	<option value="Active" <?php if($prodResult['status']=='Active') echo 'Selected';?>>Active</option>
                    <option value="Inactive" <?php if($prodResult['status']=='Inactive') echo 'Selected';?>>Inactive</option>
                  </select> 
                </div>
              </div>
              <div class="panel-heading clearfix" align="right"> 
      <input  class="btn btn-sm btn-success" type="button" name="submit" value="Add New Price" data-toggle="modal" data-target="#myModal">
      </div>
              <table id="example" class="display table" style="width: 100%;" role="grid" aria-describedby="example_info">
          <thead>
            <tr>
              <th>Sr</th>
              <th>Units</th>
              <th>Price</th>
              <th>TAX</th>
              <th>Discount</th>
              <th>Sale Price</th>
              <th>Quantity</th>
              <th>Status</th>
               <th>Actions</th>
            </tr>
          </thead>
          <tfoot>
            <tr>
               <th>Sr</th>
              <th>Units</th>
              <th>Price</th>
              <th>TAX</th>
              <th>Discount</th>
              <th>Sale Price</th>
              <th>Quantity</th>
              <th>Status</th>
              <th>Actions</th>
            </tr>
          </tfoot>
          <tbody>
          <?php 
		  $i	=1;
		  	$priceQuery	=	mysqli_query($con,"SELECT * FROM products_price WHERE product_id =".$product_id); 
			while($priceResult	=	mysqli_fetch_assoc($priceQuery))
				{ 
				 $product_id	=	$priceResult['product_id'];
				 $price_id		=	$priceResult['price_id'];
				 $price			=	$priceResult['price'];
				 $tax			=	$priceResult['tax'];
				 $discount		=	$priceResult['discount'];
				 $sale_price	=	$priceResult['sale_price'];
				 $units			=	$priceResult['units'];
				 $quantity		=	$priceResult['quantity'];
				 $price_status	=	$priceResult['price_status'];
				 
				 ?> 
          <tr>
              <td><?php echo $i;?></td> 
              <td><?php echo $units;?></td> 
              <td><?php echo $price;?></td> 
              <td><?php echo $tax;?></td>
              <td><?php echo $discount;?></td>
              <td><?php echo $sale_price;?></td>
              <td><?php echo $quantity;?></td>
               <td><?php  if($price_status=='Active')
					{ ?>
                <a onclick="active(<?php echo $price_id;?>,'Active')" title="Active">
                <button type="button" class="btn btn-success" ><?php echo $price_status;?></button>
                </a>
                <?php
					}
					else
					{?>
                <a onclick="active(<?php echo $price_id;?>,'Inactive')" title="Inactive">
                <button type="button" class="btn btn-danger"><?php echo $price_status;?></button>
                </a>
                <?php
					} ?></td>
                    <td><a title="Add Quantity" data-toggle="modal" data-target="#myQuantity" onclick="hits(<?php echo $price_id;?>)"><span class="fa fa-plus"></span></a></td>
             </tr>
             <?php  $i++;} ?>
          </tbody>
        </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div> 
	<!-- Modal -->
            <form id="add-row-form" method="post" action="product_price_save.php" enctype="multipart/form-data">
              <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                  <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                      <h4 class="modal-title" id="myModalLabel">Product Price Details</h4>
                    </div>
                    <div class="modal-body">
                      <div class="form-group">
                        <input type="text" name="units" id="units" class="form-control" placeholder="Units" required>
                       <input type="hidden" name="product_id" id="product_id" value="<?php echo $product_id;?>">
                       <input type="hidden" name="action" id="action" value="add">
                      </div>
                      <div class="form-group">
                        <input type="text" id="price" name="price" class="form-control" placeholder="Price" required>
                      </div>
                      <div class="form-group">
                        <input type="text" id="tax" name="tax" class="form-control" placeholder="Tax" required>
                      </div>
                      <div class="form-group">
                        <input type="text" id="discount" name="discount" class="form-control" placeholder="Discount" required>
                      </div>
                       <div class="form-group">
                        <input type="text" id="quantity" name="quantity" class="form-control" placeholder="Quantity" required>
                      </div>
                      
                    <div class="modal-footer">
                      <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                      <input type="submit" id="add-row" class="btn btn-success" name="Submit" value="Add" onClick="myform()">
                    </div>
                  </div>
                </div>
              </div>
              </div>
            </form>
            <!-- Modal -->
            <form id="add-quantity" method="post" action="product_price_save.php" enctype="multipart/form-data">
              <div class="modal fade" id="myQuantity" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                  <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                      <h4 class="modal-title" id="myModalLabel">Add Quantity</h4>
                    </div>
                    <div class="modal-body">
                      <div class="form-group">
                        <input type="text" name="quantity" id="quantity" class="form-control" placeholder="Quantity" required>
                       <input type="hidden" name="price_id" id="price_id">
                        <input type="hidden" name="product_id" id="product_id" value="<?php echo $product_id;?>">
                       <input type="hidden" name="action" id="action" value="quantity">
                      </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                      <input type="submit" id="add-row" class="btn btn-success" name="Submit" value="Add" onClick="myforms()">
                    </div>
                  </div>
                </div>
              </div>
              </div>
            </form>
            <script>
			function hits(id)
			{
				document.getElementById('price_id').value	=	id;
			}
			function myform()
			{
				document.getElementById('add-row-form').submit();
			}
			function myforms()
			{
				document.getElementById('add-quantity').submit();
			}
			</script>
            <script>
function active(id,status)
{			
	$.ajax({
	url:'user_actions.php',
	data: 'status=' + status + '&id=' + id +'&action=priceactive',
	type:"post",
	success:function(data){ 
		if('success' == data)
		{
			window.location.href = 'products_price_add.php?id=<?php echo base64_encode(serialize($product_id));?>&sucmsg=<?php echo base64_encode(serialize("Status updated successfully!"));?>';
		}
		else
		{
			window.location.href = 'products_price_add.php?id=<?php echo base64_encode(serialize($product_id));?>&errmsg=<?php echo base64_encode(serialize("Error updating the status!"));?>';
		}
	}
});

}
</script>
  <?php include('footer.php');?>