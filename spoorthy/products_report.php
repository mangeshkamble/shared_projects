<?php
/* * ---------------------------------------------------------------------------------
 * @MAHESHFULSUNDAR
 * @author           : Mahesh Fulsundar
 * @owner            : Mahesh Fulsundar
 * @version          : MAHESHFULSUNDAR 1.0 12-06-2018
 * @Notes            : All copyrights are reserved!
 * @rights           : All rights are reserved to the owner of the file creator.
  No one can do change to this file without permission of the owner.
 * @fileName         :
 * @dependancies     :
  ------------------------------------------------------------------------------------- */
include('cconfig.php');
include('header.php');
include('nav.php');
?>
<title>Products Report |<?php echo SITENAME; ?></title>
<div class="page-inner">
    <div class="page-breadcrumb">
        <ol class="breadcrumb container">
            <li><a href="dashboard.php">Home</a></li>
            <li class="active">Products Report</li>
        </ol>
    </div>
    <div class="page-title">
        <div class="container">
            <h3>Products Report</h3>
        </div>
    </div>
    <?php if (isset($_GET['errmsg'])) { ?>
        <div class="alert alert-danger" role="alert"> <strong>Sorry !</strong> <a href="#" class="alert-link"><?php echo unserialize(base64_decode($_GET['errmsg'])); ?></a> </div>
    <?php }
    if (isset($_GET['infomsg'])) {
        ?>
        <div class="alert alert-info" role="alert"> <strong>Note : </strong> <a href="#" class="alert-link"><?php echo unserialize(base64_decode($_GET['infomsg'])); ?></a> </div>
    <?php }
    if (isset($_GET['sucmsg'])) {
        ?>
        <div class="alert alert-success" role="alert"> <strong>Success !</strong> <a href="#" class="alert-link"><?php echo unserialize(base64_decode($_GET['sucmsg'])); ?></a> </div>
<?php } ?>
    <div class="panel panel-white">
        <div class="panel-heading clearfix" align="right"> 
        <?php
                $user_type = $_SESSION["user_type"];
                $user_id = $_SESSION["UserId"];

                if($user_type!=2){
        ?>
            <a href="export_excel.php?fileName=user_report&query=<?php echo "SELECT p.product_id,p.category_id,p.product_code,p.product_type,p.product_name,p.product_brand,p.product_desc,p.quantity,p.status,p.view_count,p.created_on,concat(u.fname,' ',u.lname) creator FROM products p LEFT JOIN user u ON u.id=p.created_by WHERE created_on >= DATE_SUB(NOW(),INTERVAL 7 DAY)"; ?>">
                <button  class="btn btn-sm btn-default" type="button" title="Weekly Product Report download"><i class="fa fa-download"></i>Weekly</button>
            </a>
            <a href="export_excel.php?fileName=user_report&query=<?php echo "SELECT p.product_id,p.category_id,p.product_code,p.product_type,p.product_name,p.product_brand,p.product_desc,p.quantity,p.status,p.view_count,p.created_on,concat(u.fname,' ',u.lname) creator FROM products p LEFT JOIN user u ON u.id=p.created_by WHERE created_on >= DATE_SUB(NOW(),INTERVAL 30 DAY)"; ?>">
                <button  class="btn btn-sm btn-primary" type="button" title="Monthly Product Report download"><i class="fa fa-download"></i>Monthly</button>
            </a>
            <a href="export_excel.php?fileName=user_report&query=<?php echo "SELECT p.product_id,p.category_id,p.product_code,p.product_type,p.product_name,p.product_brand,p.product_desc,p.quantity,p.status,p.view_count,p.created_on,concat(u.fname,' ',u.lname) creator FROM products p LEFT JOIN user u ON u.id=p.created_by WHERE created_on >= DATE_SUB(NOW(),INTERVAL 365 DAY)"; ?>">
                <button  class="btn btn-sm btn-danger" type="button" title="Yearly Product Report download"><i class="fa fa-download"></i>Yearly</button>
            </a>
            <?php 
                }else{
            ?>
            <a href="export_excel.php?fileName=user_report&query=<?php echo "SELECT p.product_id,p.category_id,p.product_code,p.product_type,p.product_name,p.product_brand,p.product_desc,p.quantity,p.status,p.view_count,p.created_on FROM products p WHERE created_on >= DATE_SUB(NOW(),INTERVAL 7 DAY) AND p.created_by=$user_id"; ?>">
                <button  class="btn btn-sm btn-default" type="button" title="Weekly Product Report download"><i class="fa fa-download"></i>Weekly</button>
            </a>
            <a href="export_excel.php?fileName=user_report&query=<?php echo "SELECT p.product_id,p.category_id,p.product_code,p.product_type,p.product_name,p.product_brand,p.product_desc,p.quantity,p.status,p.view_count,p.created_on FROM products p  WHERE created_on >= DATE_SUB(NOW(),INTERVAL 30 DAY) AND p.created_by=$user_id"; ?>">
                <button  class="btn btn-sm btn-primary" type="button" title="Monthly Product Report download"><i class="fa fa-download"></i>Monthly</button>
            </a>
            <a href="export_excel.php?fileName=user_report&query=<?php echo "SELECT p.product_id,p.category_id,p.product_code,p.product_type,p.product_name,p.product_brand,p.product_desc,p.quantity,p.status,p.view_count,p.created_on FROM products p  WHERE created_on >= DATE_SUB(NOW(),INTERVAL 365 DAY) AND p.created_by=$user_id"; ?>">
               <button  class="btn btn-sm btn-danger" type="button" title="Yearly Product Report download"><i class="fa fa-download"></i>Yearly</button>
            </a>

            <?php        
                }
            ?>
            <a href="products_add.php">
                <input  class="btn btn-sm btn-success" type="submit" name="submit" value="Add Product">
            </a>
        </div>
        <div class="panel-body">
            <div class="row m-b-lg table-responsive">
                <?php
                $getproducts="";
                $getproducts.= "SELECT 
                                    product_id,
                                    product_code,
                                    product_type,
                                    product_name,
                                    product_brand,
                                    subcategory_eng,
                                    quantity,
                                    image1,
                                    products.status,
                                    CONCAT(fname,' ',lname) created_by
                                FROM
                                    products
                                        LEFT JOIN
                                    subcategory ON subcategory.subcat_id = products.category_id
                                                LEFT JOIN
                                        user ON user.id=products.created_by
                                WHERE
                                    products.deleteFlag = 'No'";
                if($_SESSION["user_type"]==2){
                    $user_id = $_SESSION["UserId"];
                    $getproducts.=" AND user.id =$user_id "; 
                }
                
                $getproducts.= "ORDER BY product_id DESC";

                $getTransaction = mysqli_query($con, $getproducts);
                ?>
                <table id="example" class="display table" style="width: 100%;" role="grid" aria-describedby="example_info">
                    <thead>
                        <tr>
                            <th>Sr</th>
                            <th>Image</th>
                            <th>SubCategory</th>
                            <th>Product Code</th>
                            <th>Product Name</th>
                            <th>Qunatity</th> 
                            <th>Status</th>
                    <?php
                    if($_SESSION["user_type"]==1){
                    ?>
                        <th>Created By</th>
                    <?php
                    }
                    ?>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th>Sr</th>
                            <th>Image</th>
                            <th>SubCategory</th>
                            <th>Product Code</th>
                            <th>Product Name</th>
                            <th>Qunatity</th>  
                            <th>Status</th>
                    <?php
                    if($_SESSION["user_type"]==1){
                    ?>
                        <th>Created By</th>
                    <?php
                    }
                    ?>
                            <th>Action</th>
                        </tr>
                    </tfoot>
                    <tbody>
                        <?php
                        $i = 0;
                        while ($productList = mysqli_fetch_assoc($getTransaction)) {

                            $i++;
                            $product_id = $productList['product_id'];
                            $product_code = $productList['product_code'];
                            $product_type = $productList['product_type'];
                            $product_name = $productList['product_name'];
                            $product_brand = $productList['product_brand'];
                            $category = $productList['subcategory_eng'];
                            $quantity = $productList['quantity'];
                            $image1 = $productList['image1'];
                            $status = $productList['status'];
                            $created_by = $productList['created_by'];
                            ?>
                            <tr>
                                <td><?php echo $i; ?></td>
                                <td class="w100"><img class="img-responsive" title="<?php echo $product_name; ?>" src="../<?php echo $image1; ?>" height="50" width="50" alt='<?php echo $product_name; ?>'></td>
                                <td class=""><?php echo $category; ?></td>
                                <td><?php echo $product_code; ?></td> 
                                <td><?php echo $product_name; ?></td> 
                                <td><?php echo $quantity; ?></td> 
                                <td><?php if ($status == 'Active') {
                                    ?>
                                        <a onclick="active(<?php echo $product_id; ?>, 'Active')" title="Active">
                                            <button type="button" class="btn btn-success" ><?php echo $status; ?></button>
                                        </a>
                                        <?php
                                    } else {
                                        ?>
                                        <a onclick="active(<?php echo $product_id; ?>, 'Inactive')" title="Inactive">
                                            <button type="button" class="btn btn-danger"><?php echo $status; ?></button>
                                        </a>
                                        <?php }
                                    ?></td>
                                <?php
                                if($_SESSION["user_type"]==1){
                                ?>
                                <td><?php echo $created_by; ?></td>
                                <?php
                                }
                                ?>
                                <td><a href="products_price_add.php?id=<?php echo base64_encode(serialize($product_id)); ?>" title="Edit"><span class="fa fa-pencil"></span></a>&nbsp; | &nbsp; <a title="Delete" onclick="if (confirm('Do you really want to delete this product?'))
                                        {
                                            deleteid(<?php echo $product_id; ?>);
                                        } else {
                                            return  false;
                                        }"><span class="fa fa-trash"></span></a></td>
                            </tr>
    <?php
}
?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<script>
    function active(id, status)
    {
        $.ajax({
            url: 'user_actions.php',
            data: 'status=' + status + '&id=' + id + '&action=pactive',
            type: "post",
            success: function (data) {
                if ('success' == data)
                {
                    window.location.href = 'products_report.php?sucmsg=<?php echo base64_encode(serialize("Status updated successfully!")); ?>';
                } else
                {
                    window.location.href = 'products_report.php?errmsg=<?php echo base64_encode(serialize("Error updating the status!")); ?>';
                }
            }
        });

    }
    function deleteid(id)
    {

        $.ajax({
            url: 'user_actions.php',
            data: 'id=' + id + '&action=pdelete',
            type: "post",
            success: function (data) {
                if ('success' == data)
                {
                    window.location.href = 'products_report.php?sucmsg=<?php echo base64_encode(serialize("Product deleted successfully!")); ?>';
                } else
                {
                    window.location.href = 'products_report.php?errmsg=<?php echo base64_encode(serialize("Error deleting the Product!")); ?>';
                }
            }
        });


    }
</script>
<?php include('footer.php'); ?>
