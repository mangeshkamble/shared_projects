<?php 
/* * ---------------------------------------------------------------------------------
 * @MAHESHFULSUNDAR
 * @author           : Mahesh Fulsundar
 * @owner            : Mahesh Fulsundar
 * @version          : MAHESHFULSUNDAR 1.0 12-06-2018
 * @Notes            : All copyrights are reserved!
 * @rights           : All rights are reserved to the owner of the file creator.
  No one can do change to this file without permission of the owner.
 * @fileName         :
 * @dependancies     :
  ------------------------------------------------------------------------------------- */
include('cconfig.php');
include('header.php');
include('nav.php'); 
 
// code to update into database
if(isset($_POST['update']))
{
	
	$compnamyName            = $_POST['comp_name'];
	$firstName               = $_POST['fname'];
	$lastName                = $_POST['lname'];
	$mobileNo                = $_POST['mobile'];
	$email                   = $_POST['email'];
	$country                 = $_POST['country'];
	$state                   = $_POST['state'];
	$city                    = $_POST['city'];
	$area                    = $_POST['address1'];
	$address                 = $_POST['address2'];
	$pan                     = $_POST['pan'];		
	
	$displayProfile 	  = "SELECT * FROM user WHERE id = '$userId' ";						   
	$Profile              = mysqli_query($con,$displayProfile);
	
 }
?>
<title>My Profile |<?php echo SITENAME;?></title>
<div class="page-inner">
<div class="page-breadcrumb">
  <ol class="breadcrumb container">
    <li><a href="dashboard.php">Home</a></li>
    <li class="active">My Profile</li>
  </ol>
</div>
 
<div class="page-title">
  <div class="container">
    <h3>My Profile</h3>
  </div>
</div>
<div id="main-wrapper" class="container">
<div class="row">
  <div class="col-md-12">
   <?php if(isset($_GET['errmsg'])) { ?>
  <div class="alert alert-danger" role="alert">
	  <strong>Sorry !</strong> <a href="#" class="alert-link"><?php echo unserialize(base64_decode($_GET['errmsg']));?></a>
  </div>
  <?php } 
  if(isset($_GET['infomsg'])) { ?>
  <div class="alert alert-info" role="alert">
	  <strong>Note : </strong> <a href="#" class="alert-link"><?php echo unserialize(base64_decode($_GET['infomsg']));?></a>
  </div>
  <?php } 
  if(isset($_GET['sucmsg'])) {?>
  <div class="alert alert-success" role="alert">
	  <strong>Success !</strong> <a href="#" class="alert-link"><?php echo unserialize(base64_decode($_GET['sucmsg']));?></a>
  </div>
  <?php } ?>
    <div class="panel panel-white">
      <div class="panel-heading clearfix">
        <h4 class="panel-title">Profile Details</h4>
      </div>
      <div class="panel-body">
        <?php
		function getAgentDetails($con,$agentID)
		{
			$query	=	mysqli_query($con,"SELECT * FROM user WHERE id = $agentID");

			$result 	= 	mysqli_fetch_assoc($query);

			return $result;
		}
		
		  $getuserprofiledetails	 =  getAgentDetails($con,$userId);
		  $agent_id				   = $getuserprofiledetails['id'];
		  $compnamyName            = $getuserprofiledetails['comp_name'];
		  $firstName               = $getuserprofiledetails['fname'];
		  $lastName                = $getuserprofiledetails['lname'];
		  $mobileNo                = $getuserprofiledetails['mobile'];
		  $email                   = $getuserprofiledetails['email'];
		  $country                 = $getuserprofiledetails['country'];
		 
		  $state                   = $getuserprofiledetails['state'];
		  $stateName	=	mysqli_fetch_assoc(mysqli_query($con,"SELECT state from state_list where id=$state"));
		  $city                    = $getuserprofiledetails['city'];
		  $area                    = $getuserprofiledetails['address1'];
		  $address                 = $getuserprofiledetails['address2'];
		  $pan                     = $getuserprofiledetails['pan'];
		  $caf_form				=	$getuserprofiledetails['caf_form'];
		  $kyc_form				=	$getuserprofiledetails['kyc_form'];
		  ?>
        <form class="form-horizontal" action="profile_update.php" method="post" enctype="multipart/form-data">
          <div class="form-group">
            <label for="input-Default" class="col-sm-2 control-label">Company Name : </label>
            <div class="col-sm-10">
              <input type="text" name="compnamyName" class="form-control" value="<?php echo $compnamyName; ?> " id="compnamyName" readonly="readonly">
              <input type="hidden" name="agent_id" value="<?php echo $agent_id;?>" />
            </div>
          </div>
          <div class="form-group">
            <label for="input-help-block" class="col-sm-2 control-label">First Name : </label>
            <div class="col-sm-10">
              <input type="text" readonly class="form-control" value="<?php echo $firstName ; ?>" id="input-help-block">
            </div>
          </div>
          <div class="form-group">
            <label for="input-placeholder" class="col-sm-2 control-label">Last Name : </label>
            <div class="col-sm-10">
              <input type="text" readonly class="form-control" value="<?php echo  $lastName ; ?> " id="input-placeholder" >
            </div>
          </div>
          <div class="form-group">
            <label for="input-rounded" class="col-sm-2 control-label">Mobile Number : </label>
            <div class="col-sm-10">
              <input type="text" readonly class="form-control" value="<?php echo  $mobileNo  ; ?>" id="input-readonly" >
            </div>
          </div>
          <div class="form-group">
            <label for="input-disabled" class="col-sm-2 control-label">Email :</label>
            <div class="col-sm-10">
              <input type="text" readonly class="form-control" value="<?php echo $email ; ?> " id="input-disabled" >
            </div>
          </div>
          <div class="form-group">
            <label for="input-readonly" class="col-sm-2 control-label">Country : </label>
            <div class="col-sm-10">
              <input type="text" readonly class="form-control" value="India" id="input-readonly" >
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label">State : </label>
            <div class="col-sm-10">
              <input type="text" readonly class="form-control" value="<?php echo $stateName['state'] ; ?> " id="input-readonly" >
            </div>
          </div>
          <div class="form-group">
            <label for="input-password" class="col-sm-2 control-label">City : </label>
            <div class="col-sm-10">
              <input type="text" readonly class="form-control" value="<?php echo $city ; ?> " id="input-readonly" >
            </div>
          </div>
          <div class="form-group">
            <label for="input-password" class="col-sm-2 control-label">Area : </label>
            <div class="col-sm-10">
              <input type="text" readonly class="form-control" value="<?php echo $area ; ?> " id="input-readonly" >
            </div>
          </div>
          <div class="form-group">
            <label for="input-password" class="col-sm-2 control-label">Address : </label>
            <div class="col-sm-10">
              <input type="text" readonly class="form-control"  value="<?php echo $address ; ?> "id="input-readonly" >
            </div>
          </div>
          <!--div class="form-group">
            <label for="input-password" class="col-sm-2 control-label">PAN : </label>
            <div class="col-sm-10">
              <input type="text" readonly class="form-control"  value="<?php echo $pan ; ?> "id="input-readonly" >
            </div>
          </div>
           <div class="form-group">
                <label class="col-sm-2 control-label">Upload PAN CARD <span class="star">*</span>: </label>
                <div class="col-sm-5">
                  <input id="uploadFile1" name="caf-form" placeholder="Choose File" readonly="readonly"/>
                  <div class="fileUpload file_btn"> <span>Upload</span>
                   <input id="uploadBtn1" type="file" name="caf-form" class="upload">&nbsp;
                  </div>
                </div>
                <div class="col-md-3" style="border:#999">                            
                                <img src="<?php echo $caf_form;?>" alt="No Image" height="100" width="200" border="2">
                        </div>
              </div>
               <div class="form-group">
                <label class="col-sm-2 control-label">Upload ID PROOF<span class="star">*</span>: </label>
                <div class="col-sm-5">
                  <input id="uploadFile2" name="kyc-form" placeholder="Choose File" readonly="readonly" />
                  <div class="fileUpload file_btn"> <span>Upload</span>
                    <input id="uploadBtn2" type="file" name="kyc-form" class="upload">
                  </div>
                </div>
                 <div class="col-md-3" style="border:#999">                            
                                <img src="<?php echo $kyc_form;?>" alt="No Image" height="100" width="200" border="2">
                        </div>
              </div>
              <em><span style="color:#F00">Note
              Supported File Formats for both Address Proof and Id Proof are jpg, jpeg, gif, pdf</span> <span style="color:#F00">(Maximum 1 MB allowed for file upload )</span></em>
              <hr>
               <div class="form-group col-md-12" align="right">
                  <button type="submit" class="btn btn-primary btn-addon m-b-sm"><i class="fa fa-plus"></i>UPDATE</button>
                  </div-->
        </form>
      </div>
    </div>
  </div>
  </div>
  </div>
  <script>

jQuery(document).ready(function()
{
	document.getElementById("uploadBtn1").onchange = function () 
	{
		document.getElementById("uploadFile1").value = this.value;
	};		
	document.getElementById("uploadBtn2").onchange = function () 
	{
		document.getElementById("uploadFile2").value = this.value;
	};		

});

</script> 
  <?php include('footer.php');?>
