<?php
/* * ---------------------------------------------------------------------------------
 * @MAHESHFULSUNDAR
 * @author           : Mahesh Fulsundar
 * @owner            : Mahesh Fulsundar
 * @version          : MAHESHFULSUNDAR 1.0 12-06-2018
 * @Notes            : All copyrights are reserved!
 * @rights           : All rights are reserved to the owner of the file creator.
  No one can do change to this file without permission of the owner.
 * @fileName         :
 * @dependancies     :
  ------------------------------------------------------------------------------------- */
include('cconfig.php');
$inputs = $_POST;
 
if(!empty($inputs))
{
	//==============UPLOAD FOR id_proof=============================//
	if($_FILES['caf-form'] && $_FILES["caf-form"]["name"]!='')
	{	
		$name 				= $_FILES["caf-form"]["name"];
		$extension	 		= end((explode(".", $name)));
		$filetype	 		= 	$_FILES['caf-form']['type'];
		$size 				= 	$_FILES['caf-form']['size'];
		$destinationPath 	= 	'uploads/';
		
		
		$idProofFileName	=	time();
		$idProofFileName	=	'caf-'.$userId	.'.'.$extension;
		$idUrl				= 	'uploads/'.$idProofFileName;
		
		 				
		//if image file is > 1Mb
		if($size > 5024000)
		{
			// show error
			$errmsg	=	base64_encode(serialize("File size is greater than 1Mb!"));
			header("Location:profile.php?errmsg=$errmsg");
			exit;	
		}
		if(($extension=='JPG' || $extension=='JPEG' || 
			$extension=='jpg' || $extension=='jpeg' || 
			$extension=='png' || $extension=='PNG' || 
			$extension=='pdf' || $extension=='PDF') )
		{
			try 
			{
				
				move_uploaded_file($_FILES["caf-form"]["tmp_name"],$idUrl);  
   
			} 
			catch(Exception $e) 
			{ 
				$errmsg	=	base64_encode(serialize("Error occured while uploading file please try again..!"));
				header("Location:profile.php?errmsg=$errmsg");
				exit;			
			}
		}
		else
		{
			$errmsg	=	base64_encode(serialize("Invalid File Format.. Only JPG/JPEG/PNG upto 5 mb is allowed"));
			header("Location:profile.php?errmsg=$errmsg");
			exit;
		}
	}
	
	//=============================UPLOAD FOR	kyc-form========================================//
	if($_FILES['kyc-form'] && $_FILES["kyc-form"]["name"]!='')
	{
		$name 					= $_FILES["kyc-form"]["name"];
		$extension	 			= end((explode(".", $name)));
		$filetype	 			= 	$_FILES['kyc-form']['type'];		
		$size 					= 	$_FILES['kyc-form']['size']; 
		$destinationPath 		= 	'uploads/';
		$addressProofFileName	=	time();
		$addressProofFileName	=	'kyc-'.$userId.'.'.$extension;
		$addressUrl				= 	'uploads/'.$addressProofFileName;
			
		//if image file is > 1Mb
		if($size > 5024000)
		{
			// show error
			$errmsg	=	base64_encode(serialize("File size is greater than 1Mb!"));
			header("Location:profile.php?errmsg=$errmsg");
			exit;
		}
		
		if(($extension=='JPG' || $extension=='JPEG' || 
			$extension=='jpg' || $extension=='jpeg' || 
			$extension=='png' || $extension=='PNG' || 
			$extension=='pdf' || $extension=='PDF') )
		{
			try
			{
				move_uploaded_file($_FILES["kyc-form"]["tmp_name"],$addressUrl);  
			} 
			catch(Exception $e) 
			{
				$errmsg	=	base64_encode(serialize("Error occured while uploading file please try again..!"));
				header("Location:profile.php?errmsg=$errmsg");
				exit;
			}
		}
		else
		{
			$errmsg	=	base64_encode(serialize("Invalid File Format.. Only JPG/JPEG/PNG upto 5 mb is allowed"));
			header("Location:profile.php?errmsg=$errmsg");
			exit;
		}
	}
	 
	$inputs['cafUrl'] 		=  $idUrl;
	$inputs['kycFileUrl']	=  $addressUrl; 
	if($idUrl!='' && $addressUrl!='')
	{
		mysqli_query($con,"UPDATE user SET caf_form='".$inputs['cafUrl']."', kyc_form='".$inputs['kycFileUrl']."',comp_name='".$inputs['companyName']."',fname='".$inputs['firstName']."',lname='".$inputs['lastName']."',mobile='".$inputs['mobileNo']."',email='".$inputs['email']."',address1='".$inputs['area']."',address2='".$inputs['landmark']."',state='".$inputs['state']."',city='".$inputs['city']."',pincode='".$inputs['pincode']."' WHERE id=".$inputs['agent_id']);
	}
	elseif($addressUrl!='')
	{
		mysqli_query($con,"UPDATE user SET kyc_form='".$inputs['kycFileUrl']."',comp_name='".$inputs['companyName']."',fname='".$inputs['firstName']."',lname='".$inputs['lastName']."',mobile='".$inputs['mobileNo']."',email='".$inputs['email']."',address1='".$inputs['area']."',address2='".$inputs['landmark']."',state='".$inputs['state']."',city='".$inputs['city']."',pincode='".$inputs['pincode']."' WHERE id=".$inputs['agent_id']);
	}
	elseif($idUrl!='')
	{
		mysqli_query($con,"UPDATE user SET caf_form='".$inputs['cafUrl']."', comp_name='".$inputs['companyName']."',fname='".$inputs['firstName']."',lname='".$inputs['lastName']."',mobile='".$inputs['mobileNo']."',email='".$inputs['email']."',address1='".$inputs['area']."',address2='".$inputs['landmark']."',state='".$inputs['state']."',city='".$inputs['city']."',pincode='".$inputs['pincode']."' WHERE id=".$inputs['agent_id']);
	}
	else
	{
		mysqli_query($con,"UPDATE user SET comp_name='".$inputs['companyName']."',fname='".$inputs['firstName']."',lname='".$inputs['lastName']."',mobile='".$inputs['mobileNo']."',email='".$inputs['email']."',address1='".$inputs['area']."',address2='".$inputs['landmark']."',state='".$inputs['state']."',city='".$inputs['city']."',pincode='".$inputs['pincode']."' WHERE id=".$inputs['agent_id']);
	}
	$sucmsg	=	base64_encode(serialize("Profile updated successfully!"));
	
	header("Location:profile.php?sucmsg=$sucmsg");
	exit;
}
else
{
	$errmsg	=	base64_encode(serialize("Error updating the profile!"));
	header("Location:profile.php?errmsg=$errmsg");
	exit;
}
?>