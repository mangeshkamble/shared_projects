<?php 
/* * ---------------------------------------------------------------------------------
 * @MAHESHFULSUNDAR
 * @author           : Mahesh Fulsundar
 * @owner            : Mahesh Fulsundar
 * @version          : MAHESHFULSUNDAR 1.0 12-06-2018
 * @Notes            : All copyrights are reserved!
 * @rights           : All rights are reserved to the owner of the file creator.
  No one can do change to this file without permission of the owner.
 * @fileName         :
 * @dependancies     :
  ------------------------------------------------------------------------------------- */
include('cconfig.php');
session_start();

if(isset($_GET['reviewId']))
{
	$reviewId	=	unserialize(base64_decode($_GET['reviewId']));		
	$status		=	unserialize(base64_decode($_GET['stat']));
	
	if($status	=='Enabled')
	{
		$updateReview		=	mysqli_query($con,"UPDATE reviews SET status='Disabled' WHERE review_id='$reviewId'"); 
	}
	else
	{
		$updateReview		=	mysqli_query($con,"UPDATE reviews SET status='Enabled' WHERE review_id='$reviewId'"); 
	}
 
	$sucmsg	=	base64_encode(serialize("Review status changed successfully !"));
	header("Location:reviews_report.php?sucmsg=$sucmsg");
	exit;
}
else
{
	$errmsg	=	base64_encode(serialize("Oop's somthing went wrong please try again!"));
	header("Location:reviews_report.php?errmsg=$errmsg");
	exit; 
} 
?>