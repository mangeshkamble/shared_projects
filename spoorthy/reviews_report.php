<?php 
/* * ---------------------------------------------------------------------------------
 * @MAHESHFULSUNDAR
 * @author           : Mahesh Fulsundar
 * @owner            : Mahesh Fulsundar
 * @version          : MAHESHFULSUNDAR 1.0 12-06-2018
 * @Notes            : All copyrights are reserved!
 * @rights           : All rights are reserved to the owner of the file creator.
  No one can do change to this file without permission of the owner.
 * @fileName         :
 * @dependancies     :
  ------------------------------------------------------------------------------------- */
include('cconfig.php');
include('header.php');
include('nav.php');
?>
<title>Products Reviews |<?php echo SITENAME;?></title>
<div class="page-inner">
  <div class="page-breadcrumb">
    <ol class="breadcrumb container">
      <li><a href="dashboard.php">Home</a></li>
      <li class="active">Products Reviews</li>
    </ol>
  </div>
  <div class="page-title">
    <div class="container">
      <h3>Products Reviews</h3>
    </div>
  </div>
  <?php if(isset($_GET['errmsg'])) { ?>
  <div class="alert alert-danger" role="alert"> <strong>Sorry !</strong> <a href="#" class="alert-link"><?php echo unserialize(base64_decode($_GET['errmsg']));?></a> </div>
  <?php } 
  if(isset($_GET['infomsg'])) { ?>
  <div class="alert alert-info" role="alert"> <strong>Note : </strong> <a href="#" class="alert-link"><?php echo unserialize(base64_decode($_GET['infomsg']));?></a> </div>
  <?php } 
  if(isset($_GET['sucmsg'])) {?>
  <div class="alert alert-success" role="alert"> <strong>Success !</strong> <a href="#" class="alert-link"><?php echo unserialize(base64_decode($_GET['sucmsg']));?></a> </div>
  <?php } ?>
  <div class="panel panel-white">
    <div class="panel-body">
      <div class="row m-b-lg table-responsive">
        <?php
                $counter	=	1;
               
                    $query = "SELECT r.review_id,p.product_name,r.author,r.title,
                                r.text,r.rating,r.status,r.date_added
                                 FROM reviews r
                                INNER JOIN products p on p.product_id=r.product_id"; 
                    
                    if($_SESSION["user_type"]==2){
                        $userId = $_SESSION["UserId"];
                        $query.= " WHERE p.created_by=$userId";
                    }
                
                $review_details	=	mysqli_query($con,$query);
                
                $reviewsCount	=	mysqli_num_rows($review_details);
                if($reviewsCount>0)
                { 
                  ?>
        <table id="example" class="display table" style="width: 100%;" role="grid" aria-describedby="example_info">
          <thead>
            <tr>
              <th>Sr</th>
              <th>Product</th>
              <th>Customer Name</th>
              <th>Title</th>
              <th>Description</th>
              <th>Rating</th>
              <th>Status</th>
              <th>Action</th>
            </tr>
          </thead>
          <tfoot>
            <tr>
              <th>Sr</th>
              <th>Product</th>
              <th>Customer Name</th>
              <th>Title</th>
              <th>Description</th>
              <th>Rating</th>
              <th>Status</th>
              <th>Action</th>
            </tr>
          </tfoot>
          <tbody>
            <?php
			while($reviewsList = mysqli_fetch_assoc($review_details))
			{ 
				$reviewsID		=	$reviewsList['review_id'];
                                $product_name           =       $reviewsList['product_name'];
				$title			=	$reviewsList['title'];
				$author			=	$reviewsList['author'];
				$comments		=	$reviewsList['text'];
				$status			=	$reviewsList['status'];
				$rating			=	$reviewsList['rating'];
				$date			=	date('d M Y',strtotime($reviewsList['date_added']));
				?>
            <tr>
              <td><?php echo $counter++;?></td>
              <td><?php echo $product_name;?></td>
              <td><?php echo $author;?></td>
              <td><?php echo $title;?></td>
              <td><?php echo substr($comments,0,60);?></td>
              <td><?php for($i=0;$i<5;$i++)
				{ 
					if($i<$rating)
					{ ?>
                <span class="fa fa-star"></span>
                <?php 
				  }
				  else
				  { ?>
                <span class="fa fa-star-o"></span>
                <?php 
				  }
			  } ?></td>
              <td><?php  if($status=='Enabled')
					{ ?>
                <a href="reviews_activate.php?reviewId=<?php echo base64_encode(serialize($reviewsID));?>&stat=<?php echo base64_encode(serialize($status));?>" title="Enabled" class="btn btn-success"> <?php echo $status;?> </a>
                <?php
					}
					else
					{?>
                <a href="reviews_activate.php?reviewId=<?php echo base64_encode(serialize($reviewsID));?>&stat=<?php echo base64_encode(serialize($status));?>" title="Disabled" class="btn btn-danger"> <?php echo $status;?> </a>
                <?php
					} ?></td>
              <td><a href="reviews_view.php?reviewId=<?php echo base64_encode(serialize($reviewsID));?>" title="View"><span class="fa fa-eye"></span></a>&nbsp; </td>
            </tr>
            <?php 
					}
                	?>
          </tbody>
        </table>
        <?php
                } else {
                    $error = "No Reviews yet."; 
                }
                ?>
      </div>
    </div>
  </div>
</div>
<?php include("footer.php");?>