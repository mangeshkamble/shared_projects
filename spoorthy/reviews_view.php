<?php 
/* * ---------------------------------------------------------------------------------
 * @MAHESHFULSUNDAR
 * @author           : Mahesh Fulsundar
 * @owner            : Mahesh Fulsundar
 * @version          : MAHESHFULSUNDAR 1.0 12-06-2018
 * @Notes            : All copyrights are reserved!
 * @rights           : All rights are reserved to the owner of the file creator.
  No one can do change to this file without permission of the owner.
 * @fileName         :
 * @dependancies     :
  ------------------------------------------------------------------------------------- */
include('cconfig.php');
include('header.php');
include('nav.php'); 
?>
<title>Product Review |<?php echo SITENAME;?></title>
<div class="page-inner">
  <div class="page-breadcrumb">
    <ol class="breadcrumb container">
      <li><a href="dashboard.php">Home</a></li>
      <li class="active">Product Review</li>
    </ol>
  </div>
  <div class="page-title">
    <div class="container">
      <h3>Product Review</h3>
    </div>
  </div>
  <div id="main-wrapper" class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="panel panel-white">
          <div class="panel-body">
            <?php 
		$reviewId		=	unserialize(base64_decode($_GET['reviewId']));
		$getreview		=	mysqli_query($con,"SELECT * FROM reviews WHERE review_id='$reviewId'");
		while($reviews 	=	mysqli_fetch_assoc($getreview))
		{ 
		
			$customer_id=	$reviews['customer_id'];
			$title		=	$reviews['title'];
			$author		=	$reviews['author'];
			$comments	=	$reviews['text'];
			$rating		=	$reviews['rating'];
			$status		=	$reviews['status'];
			$date_added	=	date('d M Y H:i A',strtotime($reviews['date_added']));
			$status		=	$reviews['status'];
			
			?>
            <form class="form-horizontal" id="category" name="category" method="post" enctype="multipart/form-data">
              <div class="form-group">
                <label for="input-Default" class="col-sm-2 control-label">Title : </label>
                <div class="col-sm-5">
                  <input type="text" name="title" id="title" value=" <?php echo $title;?>" disabled="disabled" class="form-control">
                </div>
              </div>
              <div class="form-group">
                <label for="input-Default" class="col-sm-2 control-label">Author Name : </label>
                <div class="col-sm-5">
                  <input type="text" name="title" id="title" value=" <?php echo $author;?>" disabled="disabled" class="form-control">
                </div>
              </div>
              <div class="form-group">
                <label for="input-Default" class="col-sm-2 control-label">Rating : </label>
                <div class="col-sm-10">
                  <label style="text-align:left;">
                    <?php for($i=0;$i<5;$i++)
				  { 
					if($i<$rating)
					{ ?>
                    <span class="fa fa-star"></span>
                    <?php 
					}
					else
					{ ?>
                    <span class="fa fa-star-o"></span>
                    <?php 
					}
				  } ?>
                  </label>
                </div>
              </div>
              <div class="form-group">
                <label for="input-Default" class="col-sm-2 control-label">Status : </label>
                <div class="col-sm-5">
                  <input type="text" name="title" id="title" value="<?php echo $status;?>" disabled="disabled" class="form-control">
                </div>
              </div>
              <div class="form-group">
                <label for="input-Default" class="col-sm-2 control-label">Date Added : </label>
                <div class="col-sm-5">
                  <input type="text" name="title" id="title" value=" <?php echo $date_added;?>" disabled="disabled" class="form-control">
                </div>
              </div>
              <div class="form-group">
                <label for="input-Default" class="col-sm-2 control-label">Description : </label>
                <div class="col-sm-5">
                  <textarea name="title" id="title" class="form-control" disabled="disabled"><?php echo $comments;?></textarea>
                </div>
              </div>
              <?php 
		}?>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
  <?php include("footer.php");?>