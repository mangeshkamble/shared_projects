
<div class="panel panel-white">
    <div class="panel-body">
        <div class="row m-b-lg table-responsive">
            <?php
            $getproducts = "SELECT * FROM orders order by order_id desc";

            $getTransaction = mysqli_query($con, $getproducts);
            ?>
            <table id="example" class="display table" style="width: 100%;" role="grid" aria-describedby="example_info">
                <thead>
                    <tr>
                        <th>Sr</th>
                        <th>Invoice No</th> 
                        <th>Customer</th>
                        <th>Shipping</th> 
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th>Sr</th>
                        <th>Invoice No</th> 
                        <th>Customer</th>
                        <th>Shipping</th> 
                    </tr>
                </tfoot>
                <tbody>
                    <?php
                    $i = 0;
                    while ($productList = mysqli_fetch_assoc($getTransaction)) {
                        $i++;
                        $order_id = $productList['order_id'];
                        $invoice_no = $productList['invoice_no'];
                        $cust_name = $productList['cust_name'];
                        $shipping_code = $productList['shipping_code'];
                        $bill_amount = $productList['bill_amount'];
                        $create_date = $productList['create_date'];
                        $order_status = $productList['order_status_id'];
                        $order_status = getStatus($con, $order_status);
                        ?>
                        <tr>
                            <td><?php echo $i; ?></td>
                            <td><?php echo $invoice_no; ?></td>
                            <td><?php echo $cust_name; ?></td>
                            <td><?php echo $shipping_code; ?></td>
                        </tr>
    <?php
}
?>
                </tbody>
            </table>
        </div>
    </div>
</div>