<?php 
/* * ---------------------------------------------------------------------------------
 * @MAHESHFULSUNDAR
 * @author           : Mahesh Fulsundar
 * @owner            : Mahesh Fulsundar
 * @version          : MAHESHFULSUNDAR 1.0 12-06-2018
 * @Notes            : All copyrights are reserved!
 * @rights           : All rights are reserved to the owner of the file creator.
  No one can do change to this file without permission of the owner.
 * @fileName         :
 * @dependancies     :
  ------------------------------------------------------------------------------------- */
include('cconfig.php'); 

//=========if form is posted ===========================// 
// code to update into database
	if(isset($_POST['submit']))
	{ 
		$category	=	$_POST['category'];
		$subcategory1	=	$_POST['subcategory1'];		
		$subcategory2	=	$_POST['subcategory2'];		
		$status		= 	$_POST['status']; 
                $cat_image = '';
    
                if($_FILES['sub_category_image1'] && $_FILES["sub_category_image1"]["name"]!=='')
                {	
                    $name 				= 	$_FILES["sub_category_image1"]["name"];
                    $extension	 		=	 end((explode(".", $name)));
                    $filetype	 		= 	$_FILES['sub_category_image1']['type'];
                    $size 				= 	$_FILES['sub_category_image1']['size'];
                    $destinationPath 	= 	'../upload/products/';

                    $idProofFileName	=	time();
                    $idProofFileName	=	'1-'.$idProofFileName.'.'.$extension;
                    $image1				= 	'../upload/products/'.$idProofFileName;

                    move_uploaded_file($_FILES["sub_category_image1"]["tmp_name"],$image1);  
                    $cat_image				= 	'upload/products/'.$idProofFileName;
                }
		
		$query 	=	mysqli_query($con,"INSERT INTO subcategory(category_id,subcategory_eng,subcategory_mar,subcategory_status,image) VALUES  ('".$category."','".$subcategory1."','".$subcategory2."','".$status."','".$cat_image."')");
		$sucmsg	=	base64_encode(serialize("Sub Category added sucessfully!"));
		header("Location:subcategory.php?sucmsg=$sucmsg");
		exit;
	}

include('header.php');
include('nav.php'); 
?>
<title>Sub Category Add |<?php echo SITENAME;?></title>
<div class="page-inner">
  <div class="page-breadcrumb">
    <ol class="breadcrumb container">
      <li><a href="dashboard.php">Home</a></li>
      <li class="active">Sub Category Add</li>
    </ol>
  </div>
  <div class="page-title">
    <div class="container">
      <h3>Sub Category Add</h3>
    </div>
  </div>
  <div id="main-wrapper" class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="panel panel-white">
          <div class="panel-body">
            <form class="form-horizontal" id="category" name="category" method="post" enctype="multipart/form-data">
            <div class="form-group">
                <label for="input-Default" class="col-sm-2 control-label">Category : </label>
                <div class="col-sm-5">
                  <select name="category" id="category" class="form-control">
                  	<option value="">Select</option>
                    <?php $catQuery	=	mysqli_query($con,"SELECT * FROM category WHERE deleteFlag!='Yes' AND category_status!='Inactive'"); 
						while($catResult	=	mysqli_fetch_assoc($catQuery))
						{ ?>
                    <option value="<?php echo $catResult['category_id'];?>"><?php echo $catResult['category_eng'];?></option>
                    <?php } ?>
                  </select> 
                </div>
              </div>
            <div class="form-group">
                <label for="input-Default" class="col-sm-2 control-label">Sub Category Name (English) : </label>
                <div class="col-sm-5">
                  <input type="text" name="subcategory1" id="subcategory1" class="form-control" placeholder="Subcategory Name">
                </div>
              </div>
			  <div class="form-group">
                <label for="input-Default" class="col-sm-2 control-label">Sub Category Name (Marathi): </label>
                <div class="col-sm-5">
                  <input type="text" name="subcategory2" id="subcategory2" class="form-control" placeholder="Subcategory Name">
                </div>
              </div>
                <div class="form-group">
                    <label for="input-Default" class="col-sm-2 control-label">Upload Image : </label>
                    <div class="col-sm-4">
                        <input type="file" name="sub_category_image1" id="sub_category_image1" class="text-center event-name gui-input br-light bg-light" placeholder="Upload Image">
                    </div>
                </div>
              <div class="form-group">
                <label for="input-Default" class="col-sm-2 control-label">Status : </label>
                <div class="col-sm-5">
                  <select name="status" id="status" class="form-control">
                  	<option value="Active">Active</option>
                    <option value="Inactive">Inactive</option>
                  </select> 
                </div>
              </div>
               
              <div class="form-group">
                <div class="col-sm-offset-10 col-sm-10">
                  <input  class="btn btn-sm btn-success" type="submit" name="submit" value="Submit">
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
  
  <?php include('footer.php');?>
<script>
$(document).ready(function(e) {
 
$("#date").datepicker({
        //minDate: ,
		maxDate:0,
		});
		//validation for topuplimit check
			$('#category').validate({
				rules:{
					 category:
					 {
						 required:true,
					 },
					 subcategory:
					 {
						 required:true,
					 },					 
					},				
				});
});
</script>