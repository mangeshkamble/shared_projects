<?php 
/* * ---------------------------------------------------------------------------------
 * @MAHESHFULSUNDAR
 * @author           : Mahesh Fulsundar
 * @owner            : Mahesh Fulsundar
 * @version          : MAHESHFULSUNDAR 1.0 12-06-2018
 * @Notes            : All copyrights are reserved!
 * @rights           : All rights are reserved to the owner of the file creator.
  No one can do change to this file without permission of the owner.
 * @fileName         :
 * @dependancies     :
  ------------------------------------------------------------------------------------- */
include('cconfig.php'); 

//=========if form is posted ===========================// 
// code to update into database
	if(isset($_POST['submit']))
	{ 
		$category_id=	$_POST['category_id'];
		$category1	=	$_POST['category1'];
		$category2	=	$_POST['category2'];
		$status		= 	$_POST['status']; 
                $cat_image = '';
                $update_image = '';

                if($_FILES['edit_sub_category_image1'] && $_FILES["edit_sub_category_image1"]["name"]!=='')
                {	
                    $name 				= 	$_FILES["edit_sub_category_image1"]["name"];
                    $extension	 		=	 end((explode(".", $name)));
                    $filetype	 		= 	$_FILES['edit_sub_category_image1']['type'];
                    $size 				= 	$_FILES['edit_sub_category_image1']['size'];
                    $destinationPath 	= 	'../upload/products/';

                    $idProofFileName	=	time();
                    $idProofFileName	=	'1-'.$idProofFileName.'.'.$extension;
                    $image1				= 	'../upload/products/'.$idProofFileName;

                    move_uploaded_file($_FILES["edit_sub_category_image1"]["tmp_name"],$image1);  
                    $cat_image				= 	'upload/products/'.$idProofFileName;
                }

                if($cat_image!=''){
                    $update_image = " ,image='".$cat_image."'";
                }
 
		$query 	=	mysqli_query($con,"UPDATE subcategory SET subcategory_eng='".$category1."',subcategory_mar='".$category2."',subcategory_status ='".$status."' $update_image WHERE subcat_id=".$category_id);
		$sucmsg	=	base64_encode(serialize("Sub Category updated sucessfully!"));
		header("Location:subcategory.php?sucmsg=$sucmsg");
		exit;		
	}

$myQ	=	mysqli_query($con,"Select * FROM subcategory sc INNER JOIN category c ON c.category_id= sc.category_id WHERE subcat_id=".unserialize(base64_decode($_GET['id'])));
$result	=	mysqli_fetch_assoc($myQ);
include('header.php');
include('nav.php'); 
?>
<title>Sub Category Edit |<?php echo SITENAME;?></title>
<div class="page-inner">
  <div class="page-breadcrumb">
    <ol class="breadcrumb container">
      <li><a href="dashboard.php">Home</a></li>
      <li class="active">Sub Category Edit</li>
    </ol>
  </div>
  <div class="page-title">
    <div class="container">
      <h3>Sub Category Edit</h3>
    </div>
  </div>
  <div id="main-wrapper" class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="panel panel-white">
          <div class="panel-body">
            <form class="form-horizontal" id="category" name="category" method="post" enctype="multipart/form-data">
            <div class="form-group">
                <label for="input-Default" class="col-sm-2 control-label">Category Name : </label>
                <div class="col-sm-5">
                  <input type="text" name="category" id="category" class="form-control" readonly="readonly" value="<?php echo $result['category_eng'];?>">
                  <input type="hidden" name="category_id" id="category_id" value="<?php echo unserialize(base64_decode($_GET['id']));?>" />
                </div>
              </div>
              <div class="form-group">
                <label for="input-Default" class="col-sm-2 control-label">Sub Category Name : </label>
                <div class="col-sm-5">
                  <input type="text" name="category1" id="category1" class="form-control" value="<?php echo $result['subcategory_eng'];?>">
                  <input type="hidden" name="category_id" id="category_id" value="<?php echo unserialize(base64_decode($_GET['id']));?>" />
                </div>
              </div>
			  <div class="form-group">
                <label for="input-Default" class="col-sm-2 control-label">Sub Category Name : </label>
                <div class="col-sm-5">
                  <input type="text" name="category2" id="category2" class="form-control" value="<?php echo $result['subcategory_mar'];?>">
                   
                </div>
              </div>
                <div class="form-group">
                    <label for="input-Default" class="col-sm-2 control-label">Upload Image : </label>
                    <div class="col-sm-4">
                        <input value="<?php echo $result['image']; ?>" type="file" name="edit_sub_category_image1" id="edit_sub_category_image1" class="text-center event-name gui-input br-light bg-light" placeholder="Upload Image">
                        <span><?php echo end((explode("/", $result['image']))); ?></span>
                    </div>
                </div>
              <div class="form-group">
                <label for="input-Default" class="col-sm-2 control-label">Status : </label>
                <div class="col-sm-5">
                  <select name="status" id="status" class="form-control">
                  	<option value="Active" <?php if($result['category_status']=='Active') echo 'Selected';?>>Active</option>
                    <option value="Inactive" <?php if($result['category_status']=='Inactive') echo 'Selected';?>>Inactive</option>
                  </select> 
                </div>
              </div>
               
              <div class="form-group">
                <div class="col-sm-offset-10 col-sm-10">
                  <input  class="btn btn-sm btn-success" type="submit" name="submit" value="Submit">
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
  
  <?php include('footer.php');?>
<script>
$(document).ready(function(e) {
 
$("#date").datepicker({
        //minDate: ,
		maxDate:0,
		});
		//validation for topuplimit check
			$('#category').validate({
				rules:{
					 category:
					 {
						 required:true,
					 },
					 
					},				
				});
});
</script>