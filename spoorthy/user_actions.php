<?php 
/* * ---------------------------------------------------------------------------------
 * @MAHESHFULSUNDAR
 * @author           : Mahesh Fulsundar
 * @owner            : Mahesh Fulsundar
 * @version          : MAHESHFULSUNDAR 1.0 12-06-2018
 * @Notes            : All copyrights are reserved!
 * @rights           : All rights are reserved to the owner of the file creator.
  No one can do change to this file without permission of the owner.
 * @fileName         :
 * @dependancies     :
  ------------------------------------------------------------------------------------- */
include('cconfig.php'); 
// get all
$inputs = $_POST;

if(!empty($inputs) && $inputs['action']=='pactive')
{
	if(!empty($inputs['id']) )
	{
		if($inputs['status']=='Active')
		{
			$queryStatus = mysqli_query($con,"UPDATE products SET status='Inactive' WHERE product_id=".$inputs['id']);
		}
		else if($inputs['status']=="Inactive")
		{
			$queryStatus = mysqli_query($con,"UPDATE products SET status='Active' WHERE product_id=".$inputs['id']);
		}
		
		if($queryStatus)
		{
			echo 'success';exit();
		}
		else
		{
			echo 'error';exit();
		}
	}
	else
	{
		echo 'error';exit();	
	}
}elseif(!empty($inputs) && $inputs['action']=='useractive')
{
	if(!empty($inputs['id']) )
	{
		if($inputs['status']=='Active')
		{
			$queryStatus = mysqli_query($con,"UPDATE user SET status='Inactive' WHERE id=".$inputs['id']);
		}
		else if($inputs['status']=="Inactive")
		{
			$queryStatus = mysqli_query($con,"UPDATE user SET status='Active' WHERE id=".$inputs['id']);
		}
		
		if($queryStatus)
		{
			echo 'success';exit();
		}
		else
		{
			echo 'error';exit();
		}
	}
	else
	{
		echo 'error';exit();
	}
}
elseif(!empty($inputs) && $inputs['action']=='pdelete')
{
	if(!empty($inputs['id']) )
	{
		$queryStatus = mysqli_query($con,"UPDATE products SET deleteFlag='Yes', status='Inactive' WHERE product_id=".$inputs['id']);
		if($queryStatus)
		{
			echo 'success';exit();
		}
		else
		{
			echo 'error';exit();
		}
	}
	else
	{
		echo 'error';exit();	
	}
}elseif(!empty($inputs) && $inputs['action']=='userdelete')
{
	if(!empty($inputs['id']) )
	{
		$queryStatus = mysqli_query($con,"UPDATE user SET deleteFlag='Yes', status='Inactive' WHERE id=".$inputs['id']);
		if($queryStatus)
		{
			echo 'success';exit();
		}
		else
		{
			echo 'error';exit();
		}
	}
	else
	{
		echo 'error';exit();
	}
}  
elseif(!empty($inputs) && $inputs['action']=='active')
{
	if(!empty($inputs['id']))
	{
		if($inputs['status']=='Active')
		{
			$queryStatus = mysqli_query($con,"UPDATE category SET category_status='Inactive' WHERE category_id=".$inputs['id']);
			$queryStatus1 = mysqli_query($con,"UPDATE subcategory SET subcategory_status='Inactive' WHERE category_id=".$inputs['id']);
		}
		else if($inputs['status']=="Inactive")
		{
			$queryStatus = mysqli_query($con,"UPDATE category SET category_status='Active' WHERE category_id=".$inputs['id']);
			$queryStatus1 = mysqli_query($con,"UPDATE subcategory SET subcategory_status='Active' WHERE category_id=".$inputs['id']);
		}
		
		if($queryStatus)
		{
			echo 'success';exit();
		}
		else
		{
			echo 'error';exit();
		}
	}
	else
	{
		echo 'error';exit();
	}
}
elseif(!empty($inputs) && $inputs['action']=='delete')
{
	if(!empty($inputs['id']) )
	{
		$queryStatus = mysqli_query($con,"UPDATE category SET deleteFlag='Yes', category_status='Inactive' WHERE category_id=".$inputs['id']);
		$queryStatus1 = mysqli_query($con,"UPDATE subcategory SET deleteFlag='Yes',subcategory_status='Inactive' WHERE category_id=".$inputs['id']);
		if($queryStatus)
		{
			echo 'success';exit();
		}
		else
		{
			echo 'error';exit();
		}
	}
	else
	{
		echo 'error';exit();	
	}
}  
elseif(!empty($inputs) && $inputs['action']=='subactive')
{
	if(!empty($inputs['id']))
	{
		if($inputs['status']=='Active')
		{
			$queryStatus = mysqli_query($con,"UPDATE subcategory SET subcategory_status='Inactive' WHERE subcat_id=".$inputs['id']);
		}
		else if($inputs['status']=="Inactive")
		{
			$queryStatus = mysqli_query($con,"UPDATE subcategory SET subcategory_status='Active' WHERE subcat_id=".$inputs['id']);
		}
		
		if($queryStatus)
		{
			echo 'success';exit();
		}
		else
		{
			echo 'error';exit();
		}
	}
	else
	{
		echo 'error';exit();	
	}
}
elseif(!empty($inputs) && $inputs['action']=='subdelete')
{
	if(!empty($inputs['id']) )
	{
		$queryStatus = mysqli_query($con,"UPDATE subcategory SET deleteFlag='Yes', subcategory_status='Inactive' WHERE subcat_id=".$inputs['id']);
		if($queryStatus)
		{
			echo 'success';exit();
		}
		else
		{
			echo 'error';exit();
		}
	}
	else
	{
		echo 'error';exit();
	}
}  
 
elseif(!empty($inputs) && $inputs['action']=='priceactive')
{
	if(!empty($inputs['id']))
	{
		if($inputs['status']=='Active')
		{
			$queryStatus = mysqli_query($con,"UPDATE products_price SET price_status='Inactive' WHERE price_id=".$inputs['id']);
		}
		else if($inputs['status']=="Inactive")
		{
			$queryStatus = mysqli_query($con,"UPDATE products_price SET price_status='Active' WHERE price_id=".$inputs['id']);
		}
		
		if($queryStatus)
		{
			echo 'success';exit();
		}
		else
		{
			echo 'error';exit();
		}
	}
	else
	{
		echo 'error';exit();
	}
}elseif(!empty($inputs) && $inputs['action']=='resetpassword')
{
	if(!empty($inputs['id']) )
	{
		$NewPass ='123456789';// Common::randPass(8); 
	 
		//Update New Password in DB
		$updateQuery	=	mysqli_query($con,"UPDATE user SET password = '".hash('sha256',$NewPass)."',plain_text='".$NewPass."' WHERE id='".$inputs['id']."'");
		
		$query	=	mysqli_query($con,"SELECT * FROM user where id=".$inputs['id']);
		$logins	=	mysqli_fetch_assoc($query);
		
		$arrSMS	=	array();
		$arrSMS['msg']	=	TXNCONSTANT.": Dear ".$logins['fname'].' '.$logins['lname'].", your password is reset successfully. New Password is - ".$NewPass;
		$arrSMS['to']	=	$logins['mobile'];
            
        // Send sms to user
		send_sms($arrSMS['to'],$arrSMS['msg'],$con);
		
		//--------------Sending Email--------------------------------------    
		$arrstrRequest['userid']  		 = $logins['user_id'];  
		$arrstrRequest['password']		 = $NewPass;
		$arrstrRequest['name']     		 = $logins['fname'].' '.$logins['lname'];
		
		$arrstrRequest['emailid']  		 = $logins['email'];
		$arrstrRequest['SITEURL'] 		 = SITEURL;
		$arrstrRequest['GLOBAL_URL']	 = GLOBAL_URL;
		$arrstrRequest['SITENAME'] 		 = SITENAME;
		$arrstrRequest['NO_REPLY_EMAIL'] = NO_REPLY_EMAIL;
		
		//send_email($arrstrRequest['emailid'], $subject, $message, $fromEmail)
		//sendEmailRequest('resetPassword',$arrstrRequest);
		//---------------------------------------------------------------------
		if($updateQuery)
		{
			echo 'success';exit();
		}
		else
		{
			echo 'error';exit();
		}
	}
	else
	{
		echo 'error';exit();
	}
}
elseif(!empty($inputs) && $inputs['action']=='dealStatusChange')
{
	if(!empty($inputs['id']) )
	{
                $status = $inputs['status'];
		$queryStatus = mysqli_query($con,"UPDATE deal_of_day SET status=$status WHERE id=".$inputs['id']);
		if($queryStatus)
		{
			echo 'success';exit();
		}
		else
		{
			echo 'error';exit();
		}
	}
	else
	{
		echo 'error';exit();
	}
}  
else
{
	echo 'error';exit();	
}
?>