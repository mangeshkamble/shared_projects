<?php 
/* * ---------------------------------------------------------------------------------
 * @MAHESHFULSUNDAR
 * @author           : Mahesh Fulsundar
 * @owner            : Mahesh Fulsundar
 * @version          : MAHESHFULSUNDAR 1.0 12-06-2018
 * @Notes            : All copyrights are reserved!
 * @rights           : All rights are reserved to the owner of the file creator.
  No one can do change to this file without permission of the owner.
 * @fileName         :
 * @dependancies     :
  ------------------------------------------------------------------------------------- */
include('cconfig.php');
include('header.php');
include('nav.php'); 
 
?>
<title>Add User |<?php echo SITENAME;?></title>
<div class="page-inner">
<div class="page-breadcrumb">
  <ol class="breadcrumb container">
    <li><a href="dashboard.php">Home</a></li>
    <li class="active">Add User</li>
  </ol>
</div>
 
<div class="page-title">
  <div class="container">
    <h3>Add User</h3>
  </div>
</div>
<div id="main-wrapper" class="container">
<div class="row">
  <div class="col-md-12">
   <?php if(isset($_GET['errmsg'])) { ?>
  <div class="alert alert-danger" role="alert">
	  <strong>Sorry !</strong> <a href="#" class="alert-link"><?php echo unserialize(base64_decode($_GET['errmsg']));?></a>
  </div>
  <?php } 
  if(isset($_GET['infomsg'])) { ?>
  <div class="alert alert-info" role="alert">
	  <strong>Note : </strong> <a href="#" class="alert-link"><?php echo unserialize(base64_decode($_GET['infomsg']));?></a>
  </div>
  <?php } 
  if(isset($_GET['sucmsg'])) {?>
  <div class="alert alert-success" role="alert">
	  <strong>Success !</strong> <a href="#" class="alert-link"><?php echo unserialize(base64_decode($_GET['sucmsg']));?></a>
  </div>
  <?php } ?>
    <div class="panel panel-white">
      <div class="panel-heading clearfix">
        <h4 class="panel-title">Fill User Details</h4>
      </div>
      <div class="panel-body">
          
          <form class="form-horizontal" action="user_add_fun.php" method="post" enctype="multipart/form-data">
          <div class="form-group">
            <label for="input-Default" class="col-sm-2 control-label">Company Name : </label>
            <div class="col-sm-10">
              <input type="text" name="compnamyName" class="form-control" value="" id="compnamyName">
            </div>
          </div>
          <div class="form-group">
            <label for="input-help-block" class="col-sm-2 control-label">First Name : </label>
            <div class="col-sm-10">
              <input type="text"  class="form-control" value="" id="fname" name="fname">
            </div>
          </div>
          <div class="form-group">
            <label for="input-placeholder" class="col-sm-2 control-label">Last Name : </label>
            <div class="col-sm-10">
              <input type="text"  class="form-control" value="" id="lname" name="lname" >
            </div>
          </div>
          <div class="form-group">
            <label for="input-readonly" class="col-sm-2 control-label">UserName : </label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="userName" name="userName" >
            </div>
          </div>
          <div class="form-group">
            <label for="input-readonly" class="col-sm-2 control-label">Password : </label>
            <div class="col-sm-10">
              <input type="password" class="form-control" id="password" name="password" >
            </div>
          </div>
          <div class="form-group">
            <label for="input-readonly" class="col-sm-2 control-label">Confirm password : </label>
            <div class="col-sm-10">
              <input type="password" class="form-control" id="repassword" name="repassword" >
            </div>
          </div>
          <div class="form-group">
            <label for="input-rounded" class="col-sm-2 control-label">Mobile Number : </label>
            <div class="col-sm-10">
              <input type="text" class="form-control" value="" id="mobile" name="mobile">
            </div>
          </div>
          <div class="form-group">
            <label for="input-disabled" class="col-sm-2 control-label">Email :</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" value="" id="email" name="email" >
            </div>
          </div>
          <div class="form-group">
            <label for="input-readonly" class="col-sm-2 control-label">Country : </label>
            <div class="col-sm-10">
              <input type="text" class="form-control" value="India" id="country" name="country" >
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label">State : </label>
            <div class="col-sm-10">
              <input type="text" class="form-control" value="" id="state" name="state">
            </div>
          </div>
          <div class="form-group">
            <label for="input-password" class="col-sm-2 control-label">City : </label>
            <div class="col-sm-10">
              <input type="text" class="form-control" value="" id="city" name="city">
            </div>
          </div>
          <div class="form-group">
            <label for="input-password" class="col-sm-2 control-label">Area : </label>
            <div class="col-sm-10">
              <input type="text" class="form-control" value="" id="area" name="area">
            </div>
          </div>
          <div class="form-group">
            <label for="input-password" class="col-sm-2 control-label">Address : </label>
            <div class="col-sm-10">
              <input type="text" class="form-control"  value="" id="address" name="address">
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label">Role : </label>
            <div class="col-sm-10">
              <select class="form-control" value="" id="role" name="role">
              <option value="0">-- Choose Role --</option>
              <?php
                $res = mysqli_query($con,"select * from user_type");
                if(mysqli_num_rows($res)>0){
                  while($row=mysqli_fetch_array($res)){
                  $code = $row["code"];    
                  $type = $row["type"];
                  ?> 
                  <option value="<?php echo $code; ?>"><?php echo $type;?></option>
              <?php  
                  }
                }
              ?>
            </select>
            </div>
          </div>
          <!--div class="form-group">
            <label for="input-password" class="col-sm-2 control-label">PAN : </label>
            <div class="col-sm-10">
              <input type="text" class="form-control"  value="" id="pan" name="pan">
            </div>
          </div>
           <div class="form-group">
                <label class="col-sm-2 control-label">Upload PAN CARD <span class="star">*</span>: </label>
                <div class="col-sm-5">
                  <input id="uploadFile1" name="caf-form" placeholder="Choose File"/>
                  <div class="fileUpload file_btn"> <span>Upload</span>
                   <input id="uploadBtn1" type="file" name="caf-form" class="upload">&nbsp;
                  </div>
                </div>
                <div class="col-md-3" style="border:#999">                            
                                <img src="" alt="Select image" height="100" width="200" border="2">
                        </div>
              </div>
               <div class="form-group">
                <label class="col-sm-2 control-label">Upload ID PROOF<span class="star">*</span>: </label>
                <div class="col-sm-5">
                  <input id="uploadFile2" name="kyc-form" placeholder="Choose File" />
                  <div class="fileUpload file_btn"> <span>Upload</span>
                    <input id="uploadBtn2" type="file" name="kyc-form" class="upload">
                  </div>
                </div>
                 <div class="col-md-3" style="border:#999">                            
                                <img src="" alt="Upload Image" height="100" width="200" border="2">
                        </div>
              </div>
              <em><span style="color:#F00">Note
              Supported File Formats for both Address Proof and Id Proof are jpg, jpeg, gif, pdf</span> <span style="color:#F00">(Maximum 1 MB allowed for file upload )</span></em>
              <hr-->
               <div class="form-group col-md-12" align="right">
                  <button type="submit" class="btn btn-primary btn-addon m-b-sm"><i class="fa fa-user-plus"></i>Add User</button>
                  </div>
        </form>
      </div>
    </div>
  </div>
  </div>
  </div>
  <script>

jQuery(document).ready(function()
{
	document.getElementById("uploadBtn1").onchange = function () 
	{
		document.getElementById("uploadFile1").value = this.value;
	};		
	document.getElementById("uploadBtn2").onchange = function () 
	{
		document.getElementById("uploadFile2").value = this.value;
	};		

});

</script> 
  <?php include('footer.php');?>