<?php
/* * ---------------------------------------------------------------------------------
 * @MAHESHFULSUNDAR
 * @author           : Mahesh Fulsundar
 * @owner            : Mahesh Fulsundar
 * @version          : MAHESHFULSUNDAR 1.0 12-06-2018
 * @Notes            : All copyrights are reserved!
 * @rights           : All rights are reserved to the owner of the file creator.
  No one can do change to this file without permission of the owner.
 * @fileName         :
 * @dependancies     :
  ------------------------------------------------------------------------------------- */
include('cconfig.php');
$inputs = $_POST;
if(!empty($inputs))
{

	if($inputs["password"]==$inputs["repassword"] && $inputs["repassword"]!='' && $inputs["password"]!=''){
		if(isset($inputs["userName"]) && isset($inputs["compnamyName"]) && isset($inputs['fname']) && $inputs["role"]!=0){
			$res = mysqli_query($con,"select id from user where user_id='".$inputs["userName"]."'");
			if(mysqli_num_rows($res)>0){
				$errmsg	=	base64_encode(serialize("User name is already taken!!"));
				header("Location:user_add.php?errmsg=$errmsg");
				exit;		
			}else{
				$user_id = $inputs["userName"];
				$cpassword = hash('sha256',$inputs['password']);
				$password = $inputs['password'];
				$compnamyName = $inputs["compnamyName"];
				$fname = $inputs["fname"];
				$lname = $inputs["lname"];
				$mobile = empty($inputs["mobile"])?0:$inputs["mobile"];
				$email = $inputs["email"];
				$address = $inputs["address"];
				$country = $inputs["country"];
				$state = $inputs["state"];
				$city = $inputs["city"];
				$pincode = $inputs["area"];
				$status = "Active";
				$userType = $inputs["role"];
				$added_by = $_SESSION["UserId"];
				$deleteFlag = "No";
				$qry="INSERT INTO user(user_id,comp_name,fname,lname,password,plain_text,mobile,email,address1,country,state,
									city,pincode,status,userType,added_by,deleteFlag)VALUES('$user_id','$compnamyName','$fname','$lname','$cpassword','$password',$mobile,'$email','$address','$country','$state','$city','$pincode','$status','$userType','$added_by','$deleteFlag')";
				mysqli_query($con,$qry);
				
				$sucmsg	=	base64_encode(serialize("User added successfully!"));
				header("Location:user_add.php?sucmsg=$sucmsg");
				exit;	
			}
		}else{
			$errmsg	=	base64_encode(serialize("Company Name , User Name , Role and First Name is mandatory!!"));
			header("Location:user_add.php?errmsg=$errmsg");
			exit;	
		}		
	}else{
		$errmsg	=	base64_encode(serialize("Passwords Invalid!!"));
		header("Location:user_add.php?errmsg=$errmsg");
		exit;
	}
	exit;
}
?>