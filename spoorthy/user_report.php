<?php
/* * ---------------------------------------------------------------------------------
 * @MAHESHFULSUNDAR
 * @author           : Mahesh Fulsundar
 * @owner            : Mahesh Fulsundar
 * @version          : MAHESHFULSUNDAR 1.0 12-06-2018
 * @Notes            : All copyrights are reserved!
 * @rights           : All rights are reserved to the owner of the file creator.
  No one can do change to this file without permission of the owner.
 * @fileName         :
 * @dependancies     :
  ------------------------------------------------------------------------------------- */
include('cconfig.php');
include('header.php');
include('nav.php');
?>
<title>User Management |<?php echo SITENAME; ?></title>
<div class="page-inner">
    <div class="page-breadcrumb">
        <ol class="breadcrumb container">
            <li><a href="dashboard.php">Home</a></li>
            <li class="active">User Management</li>
        </ol>
    </div>
    <div class="page-title">
        <div class="container">
            <h3>User Management</h3>
            <?php
            /* $sql="SELECT  sum(balance)  FROM  user where id NOT IN(1,2)";
              $result=mysqli_query($con,$sql);
              ?>
              <?php
              while($rows=mysqli_fetch_array($result)){
              ?>
              <h2>Total Balance : <?php  echo  $rows['sum(balance)'];  ?></h2>
              <?php
              //  close  while  loop
              } */
            ?>

        </div>
    </div>
    <?php if (isset($_GET['errmsg'])) { ?>
        <div class="alert alert-danger" role="alert"> <strong>Sorry !</strong> <a href="#" class="alert-link"><?php echo unserialize(base64_decode($_GET['errmsg'])); ?></a> </div>
    <?php }
    if (isset($_GET['infomsg'])) {
        ?>
        <div class="alert alert-info" role="alert"> <strong>Note : </strong> <a href="#" class="alert-link"><?php echo unserialize(base64_decode($_GET['infomsg'])); ?></a> </div>
    <?php }
    if (isset($_GET['sucmsg'])) {
        ?>
        <div class="alert alert-success" role="alert"> <strong>Success !</strong> <a href="#" class="alert-link"><?php echo unserialize(base64_decode($_GET['sucmsg'])); ?></a> </div>
<?php } ?>
    <div id="main-wrapper" class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-white">
                    <div class="panel-heading clearfix">
                        <h4 class="panel-title">Search Users</h4>
                    </div>
                    <div class="panel-body" >
                        <form name="searchform" id="searchform" method="post">
                            <div class="form-group">
                                <label class="col-sm-2 control-label">User Type :</label>
                                <div class="col-sm-3">
                                    <select name="userType" id="userType" class="form-control" >
                                        <option value="">All</option>
                                        <?php
                                        $typeQuery = mysqli_query($con, "SELECT * FROM user_type WHERE id !=1");
                                        while ($types = mysqli_fetch_assoc($typeQuery)) {
                                            ?>
                                            <option value="<?php echo $types['id']; ?>" <?php if (@$_POST['userType'] == $types['id']) echo 'Selected'; ?>><?php echo $types['type']; ?></option>
<?php } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Status :</label>
                                <div class="col-sm-3">
                                    <select name="status" id="status" class="form-control" >
                                        <option value="">All</option>
                                        <option value="Active" <?php if (@$_POST['status'] == 'Active') echo 'Selected'; ?>>Active</option>
                                        <option value="Inactive" <?php if (@$_POST['status'] == 'Inactive') echo 'Selected'; ?>>Inactive</option>
                                    </select>
                                </div>
                            </div>
                            <button type="submit" class="btn btn-primary btn-rounded">Search</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="panel panel-white">
        <?php
        $actives = mysqli_query($con, "SELECT count(id) as actives FROM user where status='Active' AND deleteFlag !='Yes'");
        $resultActive = mysqli_fetch_assoc($actives);
        $inactives = mysqli_query($con, "SELECT count(id) as inactives FROM user where status!='Active' AND deleteFlag !='Yes'");
        $resultinActive = mysqli_fetch_assoc($inactives);
        ?>
        <div class="panel-heading clearfix">User List : (Active : <?php echo $resultActive['actives']; ?> / Inactive : <?php echo $resultinActive['inactives']; ?>) </div>
        <div class="panel-body">
            <div class="row m-b-lg table-responsive">
                <?php
                $getusers = "SELECT * FROM user where id !=1 AND deleteFlag !='Yes'";
                if (isset($_POST['userType']) && $_POST['userType'] != '') {
                    $getusers .= " AND userType=" . $_POST['userType'];
                }
                if (isset($_POST['status']) && $_POST['status'] != '') {
                    $getusers .= " AND status='" . $_POST['status'] . "'";
                }

                $getusers .= " order by id desc";
                //$getusers	= "SELECT * FROM user where id!=1 AND deleteFlag !='Yes' order by id desc";						  
                $resultUsers = mysqli_query($con, $getusers);
                $allNumbers = array();
                $allEmails = array();
                if (mysqli_num_rows($resultUsers) > 0) {
                    ?>
                    <div class="panel-heading clearfix" align="right"> <a href="export_excel.php?fileName=user_report&query=<?php echo "SELECT user_id,comp_name,title, fname, lname,mobile, email, address1, address2,city,pincode,pan, balance,status FROM user WHERE id!=1 AND deleteFlag !='Yes'"; ?>">
                            <a href="user_add.php"><button type="button" class="btn btn-primary btn-addon m-b-sm"><i class="fa fa-user-plus"></i> Add User </button></a>
                            <button type="button" class="btn btn-warning btn-addon m-b-sm"><i class="fa fa-download"></i>EXCEL</button>
                        </a></div><br />
                    <table id="example" class="display table" aria-describedby="example_info" role="grid">
                        <thead>
                            <tr>
                                <th>Sr.No</th>
                                <th>Parent</th>
                                <th>UserID</th>
                                <th>Agent Code</th>
                                <th>User Type</th>
                                <th>Name</th>
                                <th>Firm Name</th>
                                <th>Mobile</th>
                                <th>Email</th>
                                <th>City</th> 
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th>Sr.No</th>
                                <th>Parent</th>
                                <th>UserID</th>
                                <th>Agent Code</th>
                                <th>User Type</th>
                                <th>Name</th>
                                <th>Firm Name</th>
                                <th>Mobile</th>
                                <th>Email</th>
                                <th>City</th> 
                                <th>Actions</th>
                        </tfoot>
                        <tbody>
                            <?php
                            $i = 0;

                            while ($userList = mysqli_fetch_assoc($resultUsers)) {
                                $i++;
                                $id = $userList['id'];
                                $userID = $userList['user_id'];
                                echo $parent_id = $userList['assignTo'];
                                $parentDetails = getAgentDetailsbyID($con, $parent_id);
                                $parentName = $parentDetails['comp_name'];
                                $userType = getUserTypeName($con, $id);
                                $name = $userList['fname'] . ' ' . $userList['lname'];
                                $firmName = $userList['comp_name'];
                                $mobieNo = $userList['mobile'];
                                $email = $userList['email'];
                                $balance = $userList['balance'];
                                $city = $userList['city'];
                                $status = $userList['status'];
                                $type = $userList['userType'];

                                $allNumbers[] = $userList['mobile'];
                                $allEmails[] = $userList['email'];
                                ?>
                                <tr>
                                    <td><?php echo $i; ?></td>
                                    <td><?php echo $parentName; ?></td>
                                    <td><?php echo $id; ?></td>
                                    <td><?php echo $userID; ?></td>
                                    <td><?php echo $userType; ?></td>
                                    <td><?php echo strtoupper($name); ?></td>
                                    <td><?php echo strtoupper($firmName); ?></td>
                                    <td><?php echo $mobieNo; ?></td>
                                    <td><?php echo $email; ?></td>
                                    <td><?php echo $city; ?></td> 
                                    <td><?php if ($status == 'Active') {
                            ?>
                                            <a onclick="active(<?php echo $id; ?>, 'Active')" title="Active"><span class="fa fa-check-square"></span></a>
                                        <?php
                                        } else {
                                            ?>
                                            <a onclick="active(<?php echo $id; ?>, 'Inactive')" title="Inactive"><span class="fa fa-close"></span></a>
                                        <?php } ?>
                                        | <!--<a href="user_edit.php?id=<?php //echo base64_encode(serialize($id));?>" title="Edit"><span class="fa fa-pencil"></span></a> |--> <a onclick="resetpassword(<?php echo $id; ?>);" title="Reset Password"><span class="fa fa-key"></span></a> | <a title="Delete" onclick="if (confirm('Do you really want to delete this record?'))
                                                {
                                                    deleteid(<?php echo $id; ?>);
                                                } else {
                                                    return  false;
                                                }"><span class="fa fa-trash"></span></a> | <a href="#myModal"  title="Send SMS" data-toggle="modal" data-target="#myModal" onclick="javascript:copyMobileNo(<?php echo $mobieNo; ?>)"><span class="fa fa-envelope"></span></a> | <a href="#myModal"  title="Send Email" data-toggle="modal" data-target="#myModalEmail" onclick="javascript:copyemail('<?php echo $email; ?>')"><span class="fa fa-envelope-o"></span></a></td>
                                </tr>
                    <?php } ?>
                        </tbody>
                    </table>
                <?php
                } else {
                    echo "No records Found!";
                }
                $allNumbers = implode(',', $allNumbers);
                $allEmails = implode(',', $allEmails);
                ?>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">SEND SMS</h4>
            </div>
            <form class="form-horizontal" method="post" name="sendsms" id="sendsms" action="user_actions.php">
                <input type="hidden" name="action" value="sendsms" />
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Mobile : </label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control" id="mobile" name="sms[to]" placeholder="mobile No">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputPassword3" class="col-sm-2 control-label"> SMS : </label>
                    <div class="col-sm-8">
                        <textarea  name="sms[msg]" class="form-control" id="smstext" placeholder="smstext"></textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputPassword3" class="col-sm-2 control-label"> &nbsp; </label>
                    <div class="col-sm-8">
                        <label>
                            <input id="admin-sms-all" type="checkbox" name="send-all" allnums="<?php echo $allNumbers; ?>">
                            Check to send all</label>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-success">Send SMS</button>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="modal fade" id="myModalEmail" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">SEND EMAIL</h4>
            </div>
            <form class="form-horizontal" method="post" name="sendemail" id="sendemail" action="user_actions.php">
                <input type="hidden" name="action" value="sendemail" />
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Email : </label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control" id="email" name="email[to]" placeholder="email id">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputPassword3" class="col-sm-2 control-label"> Message : </label>
                    <div class="col-sm-8">
                        <textarea  name="email[msg]" class="form-control" id="message" placeholder="Message"></textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputPassword3" class="col-sm-2 control-label"> &nbsp; </label>
                    <div class="col-sm-8">
                        <label>
                            <input id="admin-email-all" type="checkbox" name="send-all" allemails="<?php echo $allEmails; ?>">
                            Check to send all</label>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-success">Send Email</button>
                </div>
            </form>
        </div>
    </div>
</div>
<?php include('footer.php'); ?>
<script>

    function active(id, status)
    {
        $.ajax({
            url: 'user_actions.php',
            data: 'status=' + status + '&id=' + id + '&action=useractive',
            type: "post",
            success: function (data) {
                if ('success' == data)
                {
                    window.location.href = 'user_report.php?sucmsg=<?php echo base64_encode(serialize("Status updated successfully!")); ?>';
                } else
                {
                    window.location.href = 'user_report.php?errmsg=<?php echo base64_encode(serialize("Error updating the status!")); ?>';
                }
            }
        });

    }

    function deleteid(id)
    {

        $.ajax({
            url: 'user_actions.php',
            data: 'id=' + id + '&action=userdelete',
            type: "post",
            success: function (data) {
                if ('success' == data)
                {
                    window.location.href = 'user_report.php?sucmsg=<?php echo base64_encode(serialize("User deleted successfully!")); ?>';
                } else
                {
                    window.location.href = 'user_report.php?errmsg=<?php echo base64_encode(serialize("Error deleting the user!")); ?>';
                }
            }
        });


    }
    function resetpassword(id)
    {

        $.ajax({
            url: 'user_actions.php',
            data: 'id=' + id + '&action=resetpassword',
            type: "post",
            success: function (data) {

                if ('success' == data)
                {
                    window.location.href = 'user_report.php?sucmsg=<?php echo base64_encode(serialize("New password created and sent to users mailid !")); ?>';
                } else
                {
                    window.location.href = 'user_report.php?errmsg=<?php echo base64_encode(serialize("Error to reset password!")); ?>';
                }
            }
        });


    }
    var copyMobileNo = function (num) {
        $('input[name="sms[to]"]').val(num);
    }
    var copyemail = function (num) {
        $('input[name="email[to]"]').val(num);
    }

    jQuery('#admin-sms-all').click(function () {

        if (jQuery('#admin-sms-all:checked'))
        {
            var temp = jQuery('input[name="sms[to]"]').val();
            jQuery('input[name="sms[to]"]').val(jQuery(this).attr('allnums'));
            jQuery(this).attr('allnums', temp);
        } else
        {
            var temp = jQuery('input[name="sms[to]"]').val();
            jQuery('input[name="sms[to]"]').val(jQuery(this).attr('allnums'));
            jQuery(this).attr('allnums', temp);
        }
    });
    jQuery("#frm-sms").validate({
        rules: {
            'sms[to]': "required",
            'sms[msg]': "required",

        }
    });

    jQuery('#admin-email-all').click(function () {

        if (jQuery('#admin-email-all:checked'))
        {
            var temp = jQuery('input[name="email[to]"]').val();
            jQuery('input[name="email[to]"]').val(jQuery(this).attr('allemails'));
            jQuery(this).attr('allemails', temp);
        } else
        {
            var temp = jQuery('input[name="email[to]"]').val();
            jQuery('input[name="email[to]"]').val(jQuery(this).attr('allemails'));
            jQuery(this).attr('allemails', temp);
        }
    });

</script>